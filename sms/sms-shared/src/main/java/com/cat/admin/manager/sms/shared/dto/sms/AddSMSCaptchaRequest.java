package com.cat.admin.manager.sms.shared.dto.sms;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@ApiModel("添加登录验证码短信的请求")
@Data
public class AddSMSCaptchaRequest {

    /**
     * 参数code
     */
    public static final String PARAM_CODE_NAME = "code";

    /**
     * 参数失效时间
     */
    public static final String PARAM_EFICTIVE_TIME_NAME = "efictiveTime";

    /**
     * 请求方式
     */
    public static final String METHOD = "POST";

    @ApiModelProperty(value = "手机号", required = true)
    @NotBlank
    private String phone;

    @ApiModelProperty(value = "验证码", notes = "仅支持4到6位", required = true)
    @Pattern(regexp = "^.{4,6}$")
    private String code;

    @ApiModelProperty(value = "验证码有效时长，单位：毫秒", required = true)
    @NotNull
    private Long efictiveTime;

}
