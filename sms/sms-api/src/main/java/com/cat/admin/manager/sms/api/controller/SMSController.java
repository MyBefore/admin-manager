package com.cat.admin.manager.sms.api.controller;

import com.aliyuncs.exceptions.ClientException;
import com.cat.admin.manager.sms.core.service.sms.SMSService;
import com.cat.admin.manager.sms.shared.dto.sms.AddSMSCaptchaRequest;
import com.cat.commons.result.core.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@Api("短信服务接口")
@RequestMapping("sms")
@Validated
public class SMSController {

    @Autowired
    private SMSService smsService;

    @ApiOperation("添加短信验证码接口")
    @PostMapping("captchas")
    public Result addCaptcha(@NotNull @Valid @RequestBody AddSMSCaptchaRequest request) throws ClientException {
        return smsService.addCaptcha(request);
    }

}
