package com.cat.admin.manager.sms.core.service.sms;

import com.aliyuncs.exceptions.ClientException;
import com.cat.admin.manager.sms.shared.dto.sms.AddSMSCaptchaRequest;
import com.cat.commons.result.core.Result;

public interface SMSService {

    /**
     * 添加短信验证码接口
     * 暂不支持短信回执的处理
     * @param request
     * @return
     */
    Result addCaptcha(AddSMSCaptchaRequest request) throws ClientException;

}
