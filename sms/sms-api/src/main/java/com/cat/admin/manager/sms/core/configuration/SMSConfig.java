package com.cat.admin.manager.sms.core.configuration;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SMSConfig {

    @Bean
    @ConfigurationProperties("sms.alisms")
    public Alisms alisms() {
        return new Alisms();
    }

    /**
     * 阿里的短信云平台配置
     * @return
     */
    @Bean
    public IAcsClient iAcsClient(Alisms alisms) throws ClientException {
        //初始化ascClient,暂时不支持多region（请勿修改）
        IClientProfile profile = DefaultProfile.getProfile(alisms.getRegionId(), alisms.getAccessKeyId(), alisms.getAccessKeySecret());
        DefaultProfile.addEndpoint(alisms.getEndpointName(), alisms.getRegionId(), alisms.getProduct(), alisms.getDomain());
        IAcsClient acsClient = new DefaultAcsClient(profile);
        return acsClient;
    }

    /**
     * 阿里短信云平台配置
     */
    @Data
    public static class Alisms {

        /**
         * 区域id
         */
        private String regionId;

        /**
         * 签名名称
         */
        private String signName;

        /**
         * 访问的key
         */
        private String accessKeyId;

        /**
         * 访问的安全密钥
         */
        private String accessKeySecret;

        /**
         * 服务的端点名称
         */
        private String endpointName;

        /**
         * 服务的产品名称
         */
        private String product;

        /**
         * 服务的域
         */
        private String domain;

        /**
         * 短信模板配置
         */
        private SMSTemplates templates;

        /**
         * 短信模板
         */
        @Data
        public static class SMSTemplates {
            /**
             * 短信模板code
             */
            private String captchaTemplateCode;
        }

    }

}
