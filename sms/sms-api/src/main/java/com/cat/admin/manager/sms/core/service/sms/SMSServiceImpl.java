package com.cat.admin.manager.sms.core.service.sms;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.cat.admin.manager.sms.core.configuration.SMSConfig;
import com.cat.admin.manager.sms.core.consts.TimeConsts;
import com.cat.admin.manager.sms.core.enums.SMSResponse;
import com.cat.admin.manager.sms.shared.dto.sms.AddSMSCaptchaRequest;
import com.cat.commons.result.core.Result;
import com.cat.commons.result.core.Results;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class SMSServiceImpl implements SMSService {

    @Autowired
    private IAcsClient iAcsClient;

    @Autowired
    private SMSConfig.Alisms alisms;

    @Override
    public Result addCaptcha(AddSMSCaptchaRequest request) throws ClientException {
        SendSmsResponse acsResponse = iAcsClient.getAcsResponse(transform(request));
        if (SMSResponse.isSuccess(acsResponse.getCode())) {
            return Results.success();
        } else {
            return Results.failed(acsResponse.getMessage(), HttpStatus.OK);
        }
    }

    private SendSmsRequest transform(AddSMSCaptchaRequest request) {
        SendSmsRequest sendSmsRequest = new SendSmsRequest();
        sendSmsRequest.setMethod(MethodType.valueOf(AddSMSCaptchaRequest.METHOD));
        sendSmsRequest.setPhoneNumbers(request.getPhone());
        sendSmsRequest.setSignName(alisms.getSignName());
        sendSmsRequest.setTemplateCode(alisms.getTemplates().getCaptchaTemplateCode());
        JSONObject params = new JSONObject();
        params.put(AddSMSCaptchaRequest.PARAM_CODE_NAME, request.getCode());
        params.put(AddSMSCaptchaRequest.PARAM_EFICTIVE_TIME_NAME, transformEfictiveTime(request.getEfictiveTime()));
        sendSmsRequest.setTemplateParam(JSONObject.toJSONString(params));
        return sendSmsRequest;
    }

    /**
     * 转换有效时间以字符串描述
     * @param efictiveTime
     * @return
     */
    private String transformEfictiveTime(long efictiveTime) {
        StringBuffer buff = new StringBuffer();
        List<Node> nodes = createNodes(efictiveTime);
        for (Node node : nodes) {
            buff
                    .append(node.getCount())
                    .append(node.getName());
        }
        return buff.toString();
    }

    /**
     * 创建时间描述节点
     * @param efictiveTime
     * @return
     */
    private List<Node> createNodes(long efictiveTime) {
        List<Node> nodes = new ArrayList<>();
        if (efictiveTime > 0) {
            Node node = createNode(efictiveTime, TimeConsts.DAY_BY_MILLISECOND, TimeConsts.DAY_NAME);
            nodes.add(node);
            node = createNode(node.getSurplusTime(), TimeConsts.HOUR_BY_MILLISECOND, TimeConsts.HOUR_NAME);
            nodes.add(node);
            node = createNode(node.getSurplusTime(), TimeConsts.MINUTE_BY_MILLISECOND, TimeConsts.MINUTE_NAME);
            nodes.add(node);
            node = createNode(node.getSurplusTime(), TimeConsts.SECOND_BY_MILLISECOND, TimeConsts.SECOND_NAME);
            nodes.add(node);
        }
        trim(nodes);
        return nodes;
    }

    /**
     * 创建节点
     * @param efictiveTime
     * @param ms 此节点个单位所占时间
     * @param nm 此节点单位名称
     * @return
     */
    private Node createNode(long efictiveTime, long ms, String nm) {

        if (efictiveTime == 0){
            return new Node(0, nm, 0);
        }

        long c = efictiveTime / ms;
        efictiveTime = efictiveTime % ms;

        return new Node(c, nm, efictiveTime);
    }

    /**
     * 去除首尾为0的描述节点
     * @param nodes
     */
    private void trim(List<Node> nodes) {
        Set<Node> removes = new HashSet<>();
        // 去首
        for (int i = 0; i < nodes.size(); i++) {
            Node node = nodes.get(i);
            if (node.count <= 0) {
                removes.add(node);
            } else {
                break;
            }
        }
        // 去尾
        for (int i = nodes.size() - 1; i >= 0; i--) {
            Node node = nodes.get(i);
            if (node.count <= 0) {
                removes.add(node);
            } else {
                break;
            }
        }
        nodes.removeAll(removes);
    }

    @Data
    @AllArgsConstructor
    class Node {

        private long count;

        private String name;

        private long surplusTime;
    }
}
