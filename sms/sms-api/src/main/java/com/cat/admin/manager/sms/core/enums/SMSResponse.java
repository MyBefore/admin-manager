package com.cat.admin.manager.sms.core.enums;

/**
 * 阿里云短信平台的返回码
 */
public enum SMSResponse {
    OK("OK", "请求成功"),
    ISP_RAM_PERMISSION_DENY("isp.RAM_PERMISSION_DENY", "RAM权限DENY"),
    ISV_OUT_OF_SERVICE("isv.OUT_OF_SERVICE", "业务停机"),
    ISV_PRODUCT_UN_SUBSCRIPT("isv.PRODUCT_UN_SUBSCRIPT", "未开通云通信产品的阿里云客户"),
    ISV_PRODUCT_UNSUBSCRIBE("isv.PRODUCT_UNSUBSCRIBE", "产品未开通"),
    ISV_ACCOUNT_NOT_EXISTS("isv.ACCOUNT_NOT_EXISTS", "账户不存在"),
    ISV_ACCOUNT_ABNORMAL("isv.ACCOUNT_ABNORMAL", "账户异常"),
    ISV_SMS_TEMPLATE_ILLEGAL("isv.SMS_TEMPLATE_ILLEGAL", "短信模板不合法"),
    ISV_SMS_SIGNATURE_ILLEGAL("isv.SMS_SIGNATURE_ILLEGAL", "短信签名不合法"),
    ISV_INVALID_PARAMETERS("isv.INVALID_PARAMETERS", "参数异常"),
    ISP_SYSTEM_ERROR("isp.SYSTEM_ERROR", "系统错误"),
    ISV_MOBILE_NUMBER_ILLEGAL("isv.MOBILE_NUMBER_ILLEGAL", "非法手机号"),
    ISV_MOBILE_COUNT_OVER_LIMIT("isv.MOBILE_COUNT_OVER_LIMIT", "手机号码数量超过限制"),
    ISV_TEMPLATE_MISSING_PARAMETERS("isv.TEMPLATE_MISSING_PARAMETERS", "模板缺少变量"),
    ISV_BUSINESS_LIMIT_CONTROL("isv.BUSINESS_LIMIT_CONTROL", "业务限流"),
    ISV_INVALID_JSON_PARAM("isv.INVALID_JSON_PARAM", "JSON参数不合法，只接受字符串值"),
    ISV_BLACK_KEY_CONTROL_LIMIT("isv.BLACK_KEY_CONTROL_LIMIT", "黑名单管控"),
    ISV_PARAM_LENGTH_LIMIT("isv.PARAM_LENGTH_LIMIT", "参数超出长度限制"),
    ISV_PARAM_NOT_SUPPORT_URL("isv.PARAM_NOT_SUPPORT_URL", "不支持URL"),
    ISV_AMOUNT_NOT_ENOUGH("isv.AMOUNT_NOT_ENOUGH", "账户余额不足");

    private String code;

    private String message;

    SMSResponse(String code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * 通过code 查询返回对象 不存在返回null
     * @param code
     * @return
     */
    public static SMSResponse valueByCode(String code) {
        for (SMSResponse smsResponse : values()) {
            if (smsResponse.code.equals(code)) {
                return smsResponse;
            }
        }
        return null;
    }

    /**
     * 判断code是否是成功的code
     * @param code
     * @return
     */
    public static boolean isSuccess(String code) {
        return OK.code.equals(code);
    }
}
