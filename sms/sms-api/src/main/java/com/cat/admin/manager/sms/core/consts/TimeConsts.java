package com.cat.admin.manager.sms.core.consts;

public class TimeConsts {

    /**
     * 秒 以毫秒显示
     */
    public static final long SECOND_BY_MILLISECOND = 1000;

    /**
     * 分 以毫秒显示
     */
    public static final long MINUTE_BY_MILLISECOND = 60 * SECOND_BY_MILLISECOND;

    /**
     * 小时 以毫秒显示
     */
    public static final long HOUR_BY_MILLISECOND = 60 * MINUTE_BY_MILLISECOND;

    /**
     * 天 以毫秒显示
     */
    public static final long DAY_BY_MILLISECOND = 24 * HOUR_BY_MILLISECOND;

    /**
     * 天
     */
    public static final String DAY_NAME = "天";

    /**
     * 时
     */
    public static final String HOUR_NAME = "小时";

    /**
     * 分
     */
    public static final String MINUTE_NAME = "分";

    /**
     * 秒
     */
    public static final String SECOND_NAME = "秒";
}
