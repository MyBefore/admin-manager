-- 卡片样式表
DROP TABLE IF EXISTS `card_style`;
CREATE TABLE `card_style` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT '样式名称',
  `style` text NOT NULL COMMENT '样式',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
