-- 员工表
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `worker_id` varchar(255) NOT NULL COMMENT '工作id',
  `icon` varchar(255) NOT NULL COMMENT '头像',
  `name` varchar(255) NOT NULL COMMENT '名称',
  `phone` varchar(255) NOT NULL COMMENT '手机号',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `full_time` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否是全职',
  `weixin` varchar(255) DEFAULT NULL COMMENT '微信号',
  `card` varchar(255) NOT NULL COMMENT '卡片的图片id',
  `card_style` varchar(255) NOT NULL COMMENT '卡片样式id',
  `user_id` varchar(255) NOT NULL COMMENT '用户id',
  `union_id` varchar(255) DEFAULT NULL COMMENT '用户微信全局id',
  `open_id` varchar(255) DEFAULT NULL COMMENT '用户微信公众号唯一id',
  `leave` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否离职',
  PRIMARY KEY (`worker_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='员工表';
