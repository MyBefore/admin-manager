-- 员工卡片样式表
DROP TABLE IF EXISTS `employee_card_style`;
CREATE TABLE `employee_card_style` (
  `id` varchar(255) NOT NULL COMMENT '主键',
  `params` text COMMENT '生成卡片所需的参数',
  `card_style_id` varchar(255) NOT NULL COMMENT '卡片样式的id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
