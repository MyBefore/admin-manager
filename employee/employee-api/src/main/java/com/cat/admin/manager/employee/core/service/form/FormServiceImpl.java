package com.cat.admin.manager.employee.core.service.form;

import com.cat.admin.manager.employee.core.intergration.FormClient;
import com.cat.admin.manager.file.manager.shared.dto.form.DownloadFormDTO;
import com.cat.commons.result.core.GenericResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class FormServiceImpl implements FormService {

    @Autowired
    private FormClient formClient;

    @Override
    public List<DownloadFormDTO> generateDownloadForms(List<String> ids) {
        GenericResult<List<DownloadFormDTO>> dfr = formClient.generateDownloadForms(ids);
        if (dfr == null || !dfr.getResult()) {
            return Collections.EMPTY_LIST;
        }
        return dfr.getData();
    }

}
