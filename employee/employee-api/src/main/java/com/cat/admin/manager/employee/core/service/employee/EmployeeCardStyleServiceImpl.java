package com.cat.admin.manager.employee.core.service.employee;

import com.cat.admin.manager.employee.core.consts.MessageConsts;
import com.cat.admin.manager.employee.core.dto.employee.card.style.AddEmployeeCardStyleRequest;
import com.cat.admin.manager.employee.core.dto.employee.card.style.EmployeeCardStyleDTO;
import com.cat.admin.manager.employee.data.mapper.EmployeeCardStyleMapper;
import com.cat.admin.manager.employee.domain.entity.EmployeeCardStyle;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.Results;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class EmployeeCardStyleServiceImpl implements EmployeeCardStyleService {

    @Autowired
    private EmployeeCardStyleMapper employeeCardStyleMapper;

    @Override
    public GenericResult<EmployeeCardStyleDTO> add(AddEmployeeCardStyleRequest request) {

        EmployeeCardStyle ecs = transform(request);

        employeeCardStyleMapper.insert(ecs);

        return Results.successGeneric(transform(ecs));
    }

    @Override
    public GenericResult<EmployeeCardStyleDTO> get(String id) {

        EmployeeCardStyle ecs = employeeCardStyleMapper.selectById(id);

        if (ecs == null) {
            return Results.failedGeneric(MessageConsts.CARD_STYLE_NOT_EXISTS, HttpStatus.OK);
        }

        return Results.successGeneric(transform(ecs));
    }

    private EmployeeCardStyle transform(AddEmployeeCardStyleRequest request) {
        EmployeeCardStyle employeeCardStyle = new EmployeeCardStyle();
        BeanUtils.copyProperties(request, employeeCardStyle);
        return employeeCardStyle;
    }

    private EmployeeCardStyleDTO transform(EmployeeCardStyle employeeCardStyle) {
        EmployeeCardStyleDTO dto = new EmployeeCardStyleDTO();
        BeanUtils.copyProperties(employeeCardStyle, dto);
        return dto;
    }
}
