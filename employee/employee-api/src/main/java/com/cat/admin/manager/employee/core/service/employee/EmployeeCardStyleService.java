package com.cat.admin.manager.employee.core.service.employee;

import com.cat.admin.manager.employee.core.dto.employee.card.style.AddEmployeeCardStyleRequest;
import com.cat.admin.manager.employee.core.dto.employee.card.style.EmployeeCardStyleDTO;
import com.cat.commons.result.core.GenericResult;

public interface EmployeeCardStyleService {

    /**
     * 添加员工卡片样式
     * @param request
     * @return
     */
    GenericResult<EmployeeCardStyleDTO> add(AddEmployeeCardStyleRequest request);

    /**
     * 添加员工卡片样式
     * @param id
     * @return
     */
    GenericResult<EmployeeCardStyleDTO> get(String id);

}
