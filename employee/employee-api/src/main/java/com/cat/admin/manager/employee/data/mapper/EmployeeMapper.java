package com.cat.admin.manager.employee.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cat.admin.manager.employee.domain.entity.Employee;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface EmployeeMapper extends BaseMapper<Employee> {
}
