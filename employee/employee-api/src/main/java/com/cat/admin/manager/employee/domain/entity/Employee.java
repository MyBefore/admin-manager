package com.cat.admin.manager.employee.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("employee")
@Data
public class Employee {

    public static final String COLUMN_NAME = "`name`";
    public static final String COLUMN_PHONE = "phone";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_WEIXIN = "weixin";

    @TableId(value = "worker_id", type = IdType.ID_WORKER_STR)
    private String workerId;

    @TableField("icon")
    private String icon;

    @TableField("`name`")
    private String name;

    @TableField("phone")
    private String phone;

    @TableField("email")
    private String email;

    @TableField("full_time")
    private Boolean fullTime;

    @TableField("weixin")
    private String weixin;

    @TableField("card")
    private String card;

    @TableField("card_style")
    private String cardStyle;

    @TableField("user_id")
    private String userId;

    @TableField("union_id")
    private String unionId;

    @TableField("open_id")
    private String openId;

    @TableField("`leave`")
    private Boolean leave;
}
