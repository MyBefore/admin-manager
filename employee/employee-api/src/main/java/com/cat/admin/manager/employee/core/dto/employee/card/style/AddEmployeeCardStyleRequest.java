package com.cat.admin.manager.employee.core.dto.employee.card.style;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@ApiModel("添加员工卡片样式请求")
@Data
public class AddEmployeeCardStyleRequest {

    /**
     * 创建卡片的参数
     */
    @ApiModelProperty(value = "创建卡片的参数")
    private String params;

    /**
     * 卡片样式的id
     */
    @ApiModelProperty(value = "卡片样式id", required = true)
    @NotBlank
    private String cardStyleId;
}
