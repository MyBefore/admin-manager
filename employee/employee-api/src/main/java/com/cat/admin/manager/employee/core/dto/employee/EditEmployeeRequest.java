package com.cat.admin.manager.employee.core.dto.employee;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@ApiModel("编辑员工")
@Data
public class EditEmployeeRequest extends AddEmployeeRequest {

    @ApiModelProperty(value = "以前的员工密码", required = true)
    @NotEmpty
    private String previousPassword;

}
