package com.cat.admin.manager.employee.core.dto.employee;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@ApiModel("添加员工")
@Data
public class AddEmployeeRequest {

    @ApiModelProperty(value = "员工名称", required = true)
    @NotEmpty
    private String name;

    @ApiModelProperty(value = "头像id", required = true)
    @NotEmpty
    private String icon;

    @ApiModelProperty(value = "员工手机号", required = true)
    @NotEmpty
    private String phone;

    @ApiModelProperty(value = "员工密码", required = true)
    @NotEmpty
    private String password;

    @ApiModelProperty(value = "员工邮箱")
    private String email;

    @ApiModelProperty(value = "是否全职", required = true)
    @NotNull
    private Boolean fullTime;

    @ApiModelProperty(value = "卡片", required = true)
    @NotEmpty
    private String card;

    @ApiModelProperty(value = "卡片样式", required = true)
    @NotEmpty
    private String cardStyle;

    @ApiModelProperty(value = "是否离职", required = true)
    @NotNull
    private Boolean leave;
}
