package com.cat.admin.manager.employee.core.dto.card.style;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("编辑卡片样式")
public class EditCardStyleRequest extends AddCardStyleRequest {
}
