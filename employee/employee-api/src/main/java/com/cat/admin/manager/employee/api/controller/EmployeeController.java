package com.cat.admin.manager.employee.api.controller;

import com.cat.admin.manager.employee.core.dto.employee.AddEmployeeRequest;
import com.cat.admin.manager.employee.core.dto.employee.EditEmployeeRequest;
import com.cat.admin.manager.employee.core.dto.employee.EmployeeDTO;
import com.cat.admin.manager.employee.core.dto.employee.card.style.AddEmployeeCardStyleRequest;
import com.cat.admin.manager.employee.core.dto.employee.card.style.EmployeeCardStyleDTO;
import com.cat.admin.manager.employee.core.service.employee.EmployeeCardStyleService;
import com.cat.admin.manager.employee.core.service.employee.EmployeeService;
import com.cat.admin.manager.logger.client.core.annotation.Log;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.PagedResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Api(value = "员工接口", description = "员工管理的接口")
@Validated
@RestController
@RequestMapping("employees")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private EmployeeCardStyleService employeeCardStyleService;

    @Log(function = "添加员工")
    @ApiOperation("添加员工")
    @PostMapping
    public GenericResult<EmployeeDTO> add(@Valid @RequestBody AddEmployeeRequest request) {
        return employeeService.add(request);
    }

    @ApiOperation("批量查询员工")
    @GetMapping(params={"workerIds"})
    public GenericResult<List<EmployeeDTO>> get(@ApiParam(value = "员工id集合", required = true) @NotNull @RequestParam("workerIds") List<String> workerIds) {
        return employeeService.get(workerIds);
    }

    @ApiOperation("分页查询员工")
    @GetMapping(params={"pageIndex", "pageSize"})
    public PagedResult<EmployeeDTO> get(
            @ApiParam(value = "页码", required = true) @NotNull @RequestParam("pageIndex") Integer pageIndex,
            @ApiParam(value = "条数", required = true) @NotNull @RequestParam("pageSize") Integer pageSize,
            @ApiParam(value = "关键字") @RequestParam(value = "keywords", required = false, defaultValue = "") String keywords
    ) {
        return employeeService.get(pageIndex, pageSize, keywords);
    }

    @ApiOperation("编辑员工")
    @PutMapping("{workerId}")
    public GenericResult<EmployeeDTO> edit(
            @ApiParam(value = "员工id", required = true) @NotEmpty @PathVariable("workerId") String workerId,
            @Valid @RequestBody EditEmployeeRequest request
    ) {
        return employeeService.edit(workerId, request);
    }

    @ApiOperation("员工离职")
    @PutMapping("leave/{workerId}")
    public GenericResult<EmployeeDTO> leave(@ApiParam(value = "员工id", required = true) @NotEmpty @PathVariable("workerId") String workerId) {
        return employeeService.leave(workerId);
    }

    @ApiOperation("创建模板员工卡片样式")
    @PostMapping("template/cardStyles")
    public GenericResult<EmployeeCardStyleDTO> add(@Valid @RequestBody AddEmployeeCardStyleRequest request) {
        return employeeCardStyleService.add(request);
    }

}
