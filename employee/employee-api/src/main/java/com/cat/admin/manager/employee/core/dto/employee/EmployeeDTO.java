package com.cat.admin.manager.employee.core.dto.employee;

import com.cat.admin.manager.employee.core.service.form.FormService;
import com.cat.admin.manager.file.manager.shared.dto.form.DownloadFormDTO;
import com.cat.util.join.LeftOuter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@ApiModel("员工")
@Data
public class EmployeeDTO {

    @ApiModelProperty(value = "员工id", required = true)
    @NotEmpty
    private String workerId;

    @ApiModelProperty(value = "员工名称", required = true)
    @NotEmpty
    private String name;

    @ApiModelProperty(value = "头像id", required = true)
    @NotEmpty
    private String icon;

    @ApiModelProperty("头像的下载表单")
    @LeftOuter(leftProp = "icon", right = FormService.class, rightMethod = "generateDownloadForms")
    private DownloadFormDTO iconForm;

    @ApiModelProperty(value = "员工手机号", required = true)
    @NotEmpty
    private String phone;

    @ApiModelProperty(value = "员工邮箱")
    private String email;

    @ApiModelProperty(value = "是否全职", required = true)
    @NotNull
    private Boolean fullTime;

    @ApiModelProperty(value = "微信号")
    private String weixin;

    @ApiModelProperty(value = "卡片", required = true)
    @NotEmpty
    private String card;

    @ApiModelProperty("卡片的下载表单")
    @LeftOuter(leftProp = "card", right = FormService.class, rightMethod = "generateDownloadForms")
    private DownloadFormDTO cardForm;

    @ApiModelProperty(value = "卡片样式", required = true)
    @NotEmpty
    private String cardStyle;

    @ApiModelProperty(value = "微信用户全局id")
    private String unionId;

    @ApiModelProperty(value = "微信用户公众号唯一id")
    private String openId;

    @ApiModelProperty(value = "是否离职", required = true)
    @NotNull
    private Boolean leave;
}
