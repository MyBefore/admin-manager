package com.cat.admin.manager.employee.core.dto.card.style;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LayerContent {

    /**
     * 文字 对图片无效
     */
    private String words;

    /**
     * 文字颜色 对图片无效
     */
    private String wordColor;

    /**
     * 图片id 对文字无效
     */
    private String imageFileId;

    /**
     * 参数名称 如果是图片参数的值是文件id 如果是文字参数的值是文字内容
     */
    private String paramName;

}
