package com.cat.admin.manager.employee.core.dto.card;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@ApiModel("生成员工卡片的请求")
@Data
public class AddEmployeeCardRequest {

    @ApiModelProperty(value = "员工卡片样式的id", required = true)
    @NotBlank
    private String employeeCardStyleId;

}
