package com.cat.admin.manager.employee.core.dto.card.style;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 位置
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Position {

    private int x;

    private int y;

    public Position add(Position position) {
        return new Position(x + position.x, y + position.y);
    }
}
