package com.cat.admin.manager.employee.core.consts;

public class MessageConsts {

    public static final String EMPLOYEE_NOT_EXISTS = "员工不存在";
    public static final String CARD_STYLE_NOT_EXISTS = "卡片样式不存在";
    public static final String IDS_CANNOT_IS_EMPTY = "id集合不能为空";
    public static final String NO_DATA_WAS_DELETED = "没有删除任何数据";
    public static final String FILE_UPLOAD_FAILED = "文件保存失败";
    public static final String NOT_SUPPORT_LAYER_TYPE = "不支持的层类型";
    public static final String STYLE_SIZE_CANNOT_IS_NULL = "样式大小不能为空";
    public static final String CARD_GENERATE_FAILED = "卡片生成失败";
    public static final String NOT_SUPPORT_ORIGIN_POSITION = "不支持的原点";

}
