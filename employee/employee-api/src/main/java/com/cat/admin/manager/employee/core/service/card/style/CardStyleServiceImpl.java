package com.cat.admin.manager.employee.core.service.card.style;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cat.admin.manager.employee.core.consts.MessageConsts;
import com.cat.admin.manager.employee.core.dto.card.style.AddCardStyleRequest;
import com.cat.admin.manager.employee.core.dto.card.style.CardStyleDTO;
import com.cat.admin.manager.employee.core.dto.card.style.EditCardStyleRequest;
import com.cat.admin.manager.employee.core.dto.card.style.Style;
import com.cat.admin.manager.employee.data.mapper.CardStyleMapper;
import com.cat.admin.manager.employee.domain.entity.CardStyle;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.PagedResult;
import com.cat.commons.result.core.Result;
import com.cat.commons.result.core.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CardStyleServiceImpl implements CardStyleService {

    @Autowired
    private CardStyleMapper cardStyleMapper;

    @Override
    public GenericResult<CardStyleDTO> add(AddCardStyleRequest request) {

        CardStyle cs = transform(request);

        cardStyleMapper.insert(cs);

        return Results.successGeneric(transform(cs));
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    @Override
    public GenericResult<CardStyleDTO> edit(String id, EditCardStyleRequest request) {

        CardStyle cardStyle = cardStyleMapper.selectById(id);

        if (cardStyle == null) {
            return Results.failedGeneric(MessageConsts.CARD_STYLE_NOT_EXISTS, HttpStatus.OK);
        }

        cardStyleMapper.updateById(transform(cardStyle, request));

        return Results.successGeneric(transform(cardStyle));
    }

    @Override
    public Result delete(List<String> ids) {

        if (ids.isEmpty()) {
            return Results.failed(MessageConsts.IDS_CANNOT_IS_EMPTY, HttpStatus.OK);
        }

        int deletes = cardStyleMapper.deleteBatchIds(ids);

        if (deletes <= 0) {
            return Results.failed(MessageConsts.NO_DATA_WAS_DELETED, HttpStatus.OK);
        }

        return Results.success();
    }

    @Override
    public GenericResult<List<CardStyleDTO>> get(String keywords) {
        return Results.successGeneric(transforms(cardStyleMapper.selectList(new QueryWrapper<CardStyle>().like(CardStyle.COLUMN_NAME, keywords))));
    }

    @Override
    public GenericResult<CardStyleDTO> getById(String id) {

        CardStyle cs = cardStyleMapper.selectById(id);

        if (cs == null) {
            return Results.failedGeneric(MessageConsts.CARD_STYLE_NOT_EXISTS, HttpStatus.OK);
        }

        return Results.successGeneric(transform(cs));
    }

    @Override
    public PagedResult<CardStyleDTO> get(Integer pageIndex, Integer pageSize, String keywords) {
        IPage<CardStyle> page = cardStyleMapper.selectPage(new Page<>(pageIndex, pageSize), new QueryWrapper<CardStyle>().like(CardStyle.COLUMN_NAME, keywords));
        return Results.successPaged(transforms(page.getRecords()), page.getCurrent(), page.getSize(), page.getTotal());
    }

    private CardStyle transform(AddCardStyleRequest request) {
        CardStyle cardStyle = new CardStyle();
        cardStyle.setName(request.getName());
        cardStyle.setStyle(JSONObject.toJSONString(request.getStyle()));
        return cardStyle;
    }

    private CardStyleDTO transform(CardStyle cardStyle) {
        CardStyleDTO dto = new CardStyleDTO();
        dto.setId(cardStyle.getId());
        dto.setName(cardStyle.getName());
        dto.setStyle(JSONObject.parseObject(cardStyle.getStyle(), Style.class));
        return dto;
    }

    private CardStyle transform(CardStyle cardStyle, EditCardStyleRequest request) {
        cardStyle.setName(request.getName());
        cardStyle.setStyle(JSONObject.toJSONString(request.getStyle()));
        return cardStyle;
    }

    private List<CardStyleDTO> transforms(List<CardStyle> cardStyles) {
        return cardStyles.stream().map(cs -> transform(cs)).collect(Collectors.toList());
    }
}
