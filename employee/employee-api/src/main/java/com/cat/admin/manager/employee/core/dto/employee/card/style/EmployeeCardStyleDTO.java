package com.cat.admin.manager.employee.core.dto.employee.card.style;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel("员工卡片样式描述")
public class EmployeeCardStyleDTO {

    @ApiModelProperty(value = "id", required = true)
    @NotBlank
    private String id;

    @ApiModelProperty("生成员工卡片的参数")
    private String params;

    @ApiModelProperty("卡片样式的id")
    @NotBlank
    private String cardStyleId;
}
