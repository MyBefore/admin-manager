package com.cat.admin.manager.employee.core.dto.card.style;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ApiModel("添加卡片样式请求")
public class AddCardStyleRequest {


    @ApiModelProperty(value = "样式名称", required = true)
    @NotBlank
    private String name;

    @ApiModelProperty(value = "样式", required = true)
    @NotNull
    private Style style;

}
