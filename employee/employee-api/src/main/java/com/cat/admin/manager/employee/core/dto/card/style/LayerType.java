package com.cat.admin.manager.employee.core.dto.card.style;

/**
 * 层的类型
 */
public enum LayerType {

    /**
     * 图片类型
     */
    IMAGE,

    /**
     * 文字类型
     */
    WORD

}
