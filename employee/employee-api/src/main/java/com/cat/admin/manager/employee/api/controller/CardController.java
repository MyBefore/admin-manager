package com.cat.admin.manager.employee.api.controller;

import com.cat.admin.manager.employee.core.dto.card.AddEmployeeCardRequest;
import com.cat.admin.manager.employee.core.service.card.CardService;
import com.cat.admin.manager.file.manager.shared.dto.file.FileDTO;
import com.cat.admin.manager.file.worker.shared.dto.file.UploadFileDTO;
import com.cat.commons.result.core.GenericResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(value = "卡片接口", description = "卡片管理的接口")
@Validated
@RestController
@RequestMapping("cards")
public class CardController {

    @Autowired
    private CardService cardService;

    @ApiOperation("生成员工卡片的接口")
    @PostMapping
    public GenericResult<UploadFileDTO> add(@Valid @RequestBody AddEmployeeCardRequest request) {
        return cardService.add(request);
    }
}
