package com.cat.admin.manager.employee.core.service.card;

import com.cat.admin.manager.employee.core.dto.card.AddEmployeeCardRequest;
import com.cat.admin.manager.file.worker.shared.dto.file.UploadFileDTO;
import com.cat.commons.result.core.GenericResult;

/**
 * 卡片
 */
public interface CardService {
    /**
     * 生成卡片的逻辑
     * @param request
     * @return
     */
    GenericResult<UploadFileDTO> add(AddEmployeeCardRequest request);

}
