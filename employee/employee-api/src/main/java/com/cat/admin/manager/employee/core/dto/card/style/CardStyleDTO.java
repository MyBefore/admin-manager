package com.cat.admin.manager.employee.core.dto.card.style;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ApiModel("卡片样式描述")
public class CardStyleDTO {

    @ApiModelProperty(value = "样式id", required = true)
    @NotBlank
    private String id;

    @ApiModelProperty(value = "样式名称", required = true)
    @NotBlank
    private String name;

    @ApiModelProperty(value = "样式 通过此样式将文字和图片渲染", required = true)
    @NotNull
    private Style style;

}
