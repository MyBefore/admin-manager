package com.cat.admin.manager.employee.core.service.employee;

import com.cat.admin.manager.employee.core.dto.employee.AddEmployeeRequest;
import com.cat.admin.manager.employee.core.dto.employee.EditEmployeeRequest;
import com.cat.admin.manager.employee.core.dto.employee.EmployeeDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.PagedResult;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public interface EmployeeService {

    /**
     * 添加员工
     * @param request
     * @return
     */
    GenericResult<EmployeeDTO> add(AddEmployeeRequest request);

    /**
     * 批量查询员工
     * @param workerIds
     * @return
     */
    GenericResult<List<EmployeeDTO>> get(List<String> workerIds);

    /**
     * 分页查询员工
     * @param pageIndex
     * @param pageSize
     * @param keywords
     * @return
     */
    PagedResult<EmployeeDTO> get(Integer pageIndex, Integer pageSize, String keywords);
    /**
     * 编辑员工
     * @param workerId
     * @param request
     * @return
     */
    GenericResult<EmployeeDTO> edit(String workerId, EditEmployeeRequest request);

    /**
     * 员工离职
     * @param workerId
     * @return
     */
    GenericResult<EmployeeDTO> leave(String workerId);
}
