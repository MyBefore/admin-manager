package com.cat.admin.manager.employee.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 员工的卡片样式
 */
@Data
@TableName("employee_card_style")
public class EmployeeCardStyle {

    /**
     * id
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 创建卡片的参数
     */
    @TableField("params")
    private String params;

    /**
     * 卡片样式的id
     */
    @TableField("card_style_id")
    private String cardStyleId;
}
