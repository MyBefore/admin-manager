package com.cat.admin.manager.employee;

import com.cat.admin.manager.logger.client.EnableLogger;
import com.cat.admin.manager.tcc.client.core.EnableTcc;
import com.cat.commons.result.spring.annotation.EnableResult;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@EnableResult
@EnableTcc
@EnableLogger
public class EmployeeApplication {
    public static void main(String[] args) {
        SpringApplication.run(EmployeeApplication.class, args);
    }
}
