package com.cat.admin.manager.employee.core.dto.card.style;

/**
 * 原点 坐标 位置
 */
public enum OriginPosition {

    /**
     * 左上 默认值
     */
    LEFT_UPPER,

    /**
     * 右上
     */
    RIGHT_UPPER,

    /**
     * 左下
     */
    LEFT_LOWER,

    /**
     * 右下
     */
    RIGHT_LOWER

}
