package com.cat.admin.manager.employee.api.configuration;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.cat.admin.manager.employee.data.mapper")
public class MapperConfig {
}
