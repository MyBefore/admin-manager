package com.cat.admin.manager.employee.core.configuration;

import feign.Feign;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfig {

    @Bean
    public Feign.Builder builder() {
        return Feign.builder();
    }
    
}
