package com.cat.admin.manager.employee.core.dto.card.style;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 层
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Layer {

    /**
     * 层的类型
     */
    private LayerType type;

    /**
     *
     */
    private OriginPosition originPosition;

    /**
     * 层的内容
     */
    private LayerContent content;

    /**
     * 内容的大小
     */
    private Size size;

    /**
     * 内容的位置
     */
    private Position position;

    /**
     * 字体 对图片无效
     */
    private String font;

    /**
     * 子级层
     */
    private List<Layer> layers;
}
