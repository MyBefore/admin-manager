package com.cat.admin.manager.employee.api.controller;

import com.alibaba.fastjson.JSONObject;
import com.cat.admin.manager.employee.core.dto.card.style.*;
import com.cat.admin.manager.employee.core.service.card.style.CardStyleService;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.PagedResult;
import com.cat.commons.result.core.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Api(value = "卡片样式接口", description = "卡片样式管理的接口")
@Validated
@RestController
@RequestMapping("cardStyles")
public class CardStyleController {

    @Autowired
    private CardStyleService cardStyleService;

    @ApiOperation("添加卡片样式")
    @PostMapping
    public GenericResult<CardStyleDTO> add(@Valid @RequestBody AddCardStyleRequest request) {
        return cardStyleService.add(request);
    }

    @ApiOperation("编辑卡片样式")
    @PutMapping("{id}")
    public GenericResult<CardStyleDTO> edit(
            @ApiParam(value = "id",  required = true) @NotBlank @PathVariable("id") String id,
            @Valid @RequestBody EditCardStyleRequest request
    ) {
        return cardStyleService.edit(id, request);
    }

    @ApiOperation("删除卡片样式")
    @DeleteMapping
    public Result delete(@ApiParam(value = "ids",  required = true) @NotNull @RequestParam("ids") List<String> ids) {
        return cardStyleService.delete(ids);
    }

    @ApiOperation("获取卡片样式列表")
    public GenericResult<List<CardStyleDTO>> get(@ApiParam(value = "关键字") @RequestParam(value = "keywords", required = false, defaultValue = "") String keywords) {
        return cardStyleService.get(keywords);
    }

    @ApiOperation("分页查询卡片样式")
    @GetMapping(params={"pageIndex", "pageSize"})
    public PagedResult<CardStyleDTO> get(
            @ApiParam(value = "页码", required = true) @NotNull @RequestParam("pageIndex") Integer pageIndex,
            @ApiParam(value = "条数", required = true) @NotNull @RequestParam("pageSize") Integer pageSize,
            @ApiParam(value = "关键字") @RequestParam(value = "keywords", required = false, defaultValue = "") String keywords
    ) {
        return cardStyleService.get(pageIndex, pageSize, keywords);
    }

}
