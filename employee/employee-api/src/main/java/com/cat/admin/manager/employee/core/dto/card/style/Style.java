package com.cat.admin.manager.employee.core.dto.card.style;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Style {

    /**
     * 整个样式大小
     */
    private Size size;

    /**
     * 整个样式位置
     */
    private Position position;

    /**
     * 样式的层 按数组先后顺序覆盖
     */
    private List<Layer> layers;

}
