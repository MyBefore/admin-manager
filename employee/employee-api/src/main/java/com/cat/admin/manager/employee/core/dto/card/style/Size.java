package com.cat.admin.manager.employee.core.dto.card.style;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 大小
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Size {

    /**
     * 宽
     */
    private int width;

    /**
     * 高
     */
    private int height;

}
