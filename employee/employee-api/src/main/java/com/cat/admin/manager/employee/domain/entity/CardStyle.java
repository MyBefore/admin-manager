package com.cat.admin.manager.employee.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("card_style")
@Data
public class CardStyle {

    public static final String COLUMN_NAME = "name";

    /**
     * 样式id
     */
    @TableId(value = "id", type = IdType.UUID)
    private String id;

    /**
     * 卡片样式的名称
     */
    @TableField("name")
    private String name;

    /**
     * 样式 通过此样式将文字和图片渲染
     */
    @TableField("style")
    private String style;

}
