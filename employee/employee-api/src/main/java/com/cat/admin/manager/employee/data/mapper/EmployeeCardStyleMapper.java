package com.cat.admin.manager.employee.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cat.admin.manager.employee.domain.entity.EmployeeCardStyle;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface EmployeeCardStyleMapper extends BaseMapper<EmployeeCardStyle> {
}
