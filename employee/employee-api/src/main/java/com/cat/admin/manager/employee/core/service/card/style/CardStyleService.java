package com.cat.admin.manager.employee.core.service.card.style;

import com.cat.admin.manager.employee.core.dto.card.style.AddCardStyleRequest;
import com.cat.admin.manager.employee.core.dto.card.style.CardStyleDTO;
import com.cat.admin.manager.employee.core.dto.card.style.EditCardStyleRequest;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.PagedResult;
import com.cat.commons.result.core.Result;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public interface CardStyleService {

    /**
     * 添加卡片样式
     * @param request
     * @return
     */
    GenericResult<CardStyleDTO> add(AddCardStyleRequest request);

    /**
     * 编辑卡片样式
     * @param id
     * @param request
     * @return
     */
    GenericResult<CardStyleDTO> edit(String id, EditCardStyleRequest request);

    /**
     * 查询卡片样式
     * @param id
     * @return
     */
    GenericResult<CardStyleDTO> getById(String id);

    /**
     * 删除卡片样式
     * @param ids
     * @return
     */
    Result delete(List<String> ids);

    /**
     * 获取卡片样式列表
     * @param keywords
     * @return
     */
    GenericResult<List<CardStyleDTO>> get(String keywords);

    /**
     * 分页查询卡片样式
     * @param pageIndex
     * @param pageSize
     * @param keywords
     * @return
     */
    PagedResult<CardStyleDTO> get(Integer pageIndex, Integer pageSize, String keywords);

}
