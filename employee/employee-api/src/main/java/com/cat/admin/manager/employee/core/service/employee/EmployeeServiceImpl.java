package com.cat.admin.manager.employee.core.service.employee;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cat.admin.manager.employee.core.consts.MessageConsts;
import com.cat.admin.manager.employee.core.dto.employee.AddEmployeeRequest;
import com.cat.admin.manager.employee.core.dto.employee.EditEmployeeRequest;
import com.cat.admin.manager.employee.core.dto.employee.EmployeeDTO;
import com.cat.admin.manager.employee.core.intergration.UserClient;
import com.cat.admin.manager.employee.data.mapper.EmployeeMapper;
import com.cat.admin.manager.employee.domain.entity.Employee;
import com.cat.admin.manager.tcc.client.core.manager.TccLocal;
import com.cat.admin.manager.tcc.client.core.manager.annotation.Tcc;
import com.cat.admin.manager.user.shared.dto.user.AddUserRequest;
import com.cat.admin.manager.user.shared.dto.user.EditUserRequest;
import com.cat.admin.manager.user.shared.dto.user.UserDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.PagedResult;
import com.cat.commons.result.core.Results;
import com.cat.commons.result.core.exception.ResultException;
import com.cat.util.join.Join;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private UserClient userClient;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private TccLocal tccLocal;

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE, rollbackFor = Throwable.class)
    @Tcc(cancelMethod = "com.cat.admin.manager.employee.core.service.employee.EmployeeServiceImpl.cancelAdd")
    public GenericResult<EmployeeDTO> add(AddEmployeeRequest request) {

        // 创建用户
        GenericResult<UserDTO> ur = userClient.add(transform(request));
        if (!ur.getResult()) {
            throw new ResultException(ur.getMessage(), ur.getStatus());
        }
        // 创建员工
        Employee employee = transformx(request);
        employee.setUserId(ur.getData().getId());

        employeeMapper.insert(employee);

        // 设置事务取消参数
        tccLocal.setCancelParams(employee.getWorkerId());

        return Results.successGeneric(transform(employee));

    }

    /**
     * 回滚操作
     * @param workerId
     */
    public void cancelAdd(String workerId) {
        if (StringUtils.isNotBlank(workerId)) {
            employeeMapper.deleteById(workerId);
        }
    }

    @Override
    public GenericResult<List<EmployeeDTO>> get(List<String> workerIds) {
        List<Employee> employees = employeeMapper.selectBatchIds(workerIds);
        return Results.successGeneric(transforms(employees));
    }

    @Override
    public PagedResult<EmployeeDTO> get(Integer pageIndex, Integer pageSize, String keywords) {
        IPage<Employee> page = employeeMapper.selectPage(
                new Page<>(pageIndex, pageSize),
                new QueryWrapper<Employee>()
                .or().like(Employee.COLUMN_NAME, keywords)
                .or().like(Employee.COLUMN_PHONE, keywords)
                .or().like(Employee.COLUMN_EMAIL, keywords)
                .or().like(Employee.COLUMN_WEIXIN, keywords)

        );
        return Results.successPaged(transforms(page.getRecords()), page.getCurrent(), page.getSize(), page.getTotal());
    }

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ, rollbackFor = Throwable.class)
    public GenericResult<EmployeeDTO> edit(String workerId, EditEmployeeRequest request) {

        // 修改员工
        Employee employee = employeeMapper.selectById(workerId);
        if (employee == null) {
            return Results.failedGeneric(MessageConsts.EMPLOYEE_NOT_EXISTS, HttpStatus.BAD_REQUEST);
        }

        employee = transform(request, employee);

        employeeMapper.updateById(employee);

        // 修改用户
        GenericResult<UserDTO> ur = userClient.edit(employee.getUserId(), transform(request));

        if (!ur.getResult()) {
            // 回滚事务最终一致性
            throw new ResultException(ur.getMessage(), ur.getStatus());
        }

        return Results.successGeneric(transform(employee));
    }

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ, rollbackFor = Throwable.class)
    public GenericResult<EmployeeDTO> leave(String workerId) {

        Employee employee = employeeMapper.selectById(workerId);
        if (employee == null) {
            return Results.failedGeneric(MessageConsts.EMPLOYEE_NOT_EXISTS, HttpStatus.BAD_REQUEST);
        }
        employee.setLeave(true);
        employeeMapper.updateById(employee);

        return Results.successGeneric(transform(employee));
    }

    private AddUserRequest transform(AddEmployeeRequest request) {
        AddUserRequest addUserRequest = new AddUserRequest();
        BeanUtils.copyProperties(request, addUserRequest);
        addUserRequest.setUsername(request.getPhone());
        return addUserRequest;
    }

    private EditUserRequest transform(EditEmployeeRequest request) {
        EditUserRequest editUserRequest = new EditUserRequest();
        BeanUtils.copyProperties(request, editUserRequest);
        editUserRequest.setUsername(request.getPhone());
        return editUserRequest;
    }

    private Employee transformx(AddEmployeeRequest request) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(request, employee);
        return employee;
    }

    private Employee transform(AddEmployeeRequest request, Employee employee) {
        BeanUtils.copyProperties(request, employee);
        return employee;
    }

    private EmployeeDTO transform(Employee employee) {
        EmployeeDTO employeeDTO = new EmployeeDTO();
        BeanUtils.copyProperties(employee, employeeDTO);
        Join.leftOuter(Arrays.asList(employeeDTO), applicationContext);
        return employeeDTO;
    }

    private List<EmployeeDTO> transforms(List<Employee> employees) {
        List<EmployeeDTO> employeeDTOS = employees.stream().map(e -> {
            EmployeeDTO employeeDTO = new EmployeeDTO();
            BeanUtils.copyProperties(e, employeeDTO);
            return employeeDTO;
        }).collect(Collectors.toList());
        Join.leftOuter(employeeDTOS, applicationContext);
        return employeeDTOS;
    }
}
