package com.cat.admin.manager.employee.core.service.card;

import cn.hutool.cache.Cache;
import cn.hutool.cache.CacheUtil;
import cn.hutool.core.io.resource.BytesResource;
import cn.hutool.core.io.resource.Resource;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.http.Method;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.cat.admin.manager.employee.core.consts.MessageConsts;
import com.cat.admin.manager.employee.core.dto.card.AddEmployeeCardRequest;
import com.cat.admin.manager.employee.core.dto.card.style.*;
import com.cat.admin.manager.employee.core.dto.employee.card.style.EmployeeCardStyleDTO;
import com.cat.admin.manager.employee.core.intergration.FormClient;
import com.cat.admin.manager.employee.core.service.card.style.CardStyleService;
import com.cat.admin.manager.employee.core.service.employee.EmployeeCardStyleService;
import com.cat.admin.manager.file.manager.shared.dto.form.DownloadFormDTO;
import com.cat.admin.manager.file.manager.shared.dto.form.UploadFormDTO;
import com.cat.admin.manager.file.worker.shared.dto.file.UploadFileDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.Results;
import com.cat.commons.result.core.exception.ResultException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class CardServiceImpl implements CardService {

    public static final String PNG_FORMAT = "png";
    public static final String DEFAULT_PNG_IMAGE_CARD_NAME = "card.png";
    public static final  Position DEFAULT_POSITION = new Position(0, 0);
    public static final Font DEFAULT_FONT = new Font(null, Font.BOLD, 20);
    public static final Color DEFAULT_COLOR = Color.BLACK;
    public static final OriginPosition DEFAULT_ORIGIN_POSITION = OriginPosition.LEFT_UPPER;
    /**
     * 图片使用次数策略缓存 最大5张 0表示不设置超时时间
     */
    public static final Cache<String, Image> IMAGE_LFU_CACHE = CacheUtil.newLFUCache(5, 0);

    @Autowired
    private EmployeeCardStyleService employeeCardStyleService;

    @Autowired
    private CardStyleService cardStyleService;

    @Autowired
    private FormClient formClient;


    @Override
    public GenericResult<UploadFileDTO> add(AddEmployeeCardRequest request) {

        GenericResult<EmployeeCardStyleDTO> ecsr = employeeCardStyleService.get(request.getEmployeeCardStyleId());

        if (!ecsr.getResult()) {
            return Results.failedGeneric(ecsr.getMessage(), HttpStatus.OK);
        }

        // 生成图片
        BufferedImage image = generateImage(ecsr.getData());

        // 保存图片
        GenericResult<UploadFileDTO> fdr = saveImage(image);

        return fdr;
    }

    /**
     * 生成图片
     * @param dto
     * @return
     */
    private BufferedImage generateImage(EmployeeCardStyleDTO dto) {

        GenericResult<CardStyleDTO> csdr = cardStyleService.getById(dto.getCardStyleId());

        if (!csdr.getResult()) {
            throw new ResultException(csdr.getMessage(), csdr.getStatus());
        }

        CardStyleDTO data = csdr.getData();
        Style style = data.getStyle();

        // 创建图片
        Size size = style.getSize();
        if (size == null) {
            throw new ResultException(MessageConsts.STYLE_SIZE_CANNOT_IS_NULL, HttpStatus.OK);
        }
        BufferedImage image = new BufferedImage(size.getWidth(), size.getHeight(), BufferedImage.TYPE_INT_ARGB);
        // 获取画笔
        Graphics graphics = image.getGraphics();
        // 绘制层
        drawLayers(getPosition(style.getPosition()), graphics, style.getLayers(), JSONObject.parseObject(dto.getParams()));

        return image;
    }

    private Position getPosition(Position position) {
        return position == null ? DEFAULT_POSITION : position;
    }

    private Font getFont(String font, Size size) {

        Font f;

        if (font == null) {
            f = Font.decode(font);
        } else {
            f = DEFAULT_FONT;
        }

        if (size != null) {
            f = new Font(f.getName(), f.getStyle(), getFontSize(size));
        }

        return f;
    }

    private int getFontSize(Size size) {
        return (size.getWidth() + size.getWidth()) / 2;
    }

    private Color getColor(String color) {
        return color == null ? DEFAULT_COLOR : Color.decode(color);
    }

    private String getWords(LayerContent content, Map<String, Object> params) {
        if (StringUtils.isBlank(content.getWords())) {
            if (content.getParamName() == null || !params.containsKey(content.getParamName())) {
                return "";
            }
            Object o = params.get(content.getParamName());
            if (o == null) {
                return "";
            }
            return o.toString();
        }
        return content.getWords();
    }

    private Image getImage(LayerContent content, Map<String, Object> params) {

        Image image;

        String id = null;

        if (StringUtils.isBlank(content.getImageFileId()) || (image = getImageById(content.getImageFileId())) == null) {
            if (content.getParamName() == null && !params.containsKey(content.getParamName())) {
                image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
            }
            Object o = params.get(content.getParamName());
            if (o == null) {
                image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
            }
            if ((image = getImageById(o.toString())) == null) {
                image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
            } else {
                id = o.toString();
            }
        } else {
            id = content.getImageFileId();
        }

        // 设置缓存
        if (id != null) {
            IMAGE_LFU_CACHE.put(id, image);
        }

        return image;
    }

    private OriginPosition getOriginPosition(OriginPosition originPosition) {
        return originPosition == null ? DEFAULT_ORIGIN_POSITION : originPosition;
    }

    /**
     * 根据原点位置重新设置渲染的坐标
     * @param position
     * @param size
     * @param originPosition
     */
    private void resetOriginPosition(Position position, Size size, OriginPosition originPosition) {
        switch (originPosition) {
            case LEFT_UPPER:{}break;
            case RIGHT_UPPER:{
                position.setX(position.getX() - size.getWidth());
            }break;
            case LEFT_LOWER:{
                position.setY(position.getY() - size.getHeight());
            }break;
            case RIGHT_LOWER:{
                position.setX(position.getX() - size.getWidth());
                position.setY(position.getY() - size.getHeight());
            }break;
            default: throw new ResultException(MessageConsts.NOT_SUPPORT_ORIGIN_POSITION, HttpStatus.OK);
        }
    }

    private Image getImageById(String fileId) {

        Image image = IMAGE_LFU_CACHE.get(fileId);
        if (image != null) {
            return image;
        }

        GenericResult<List<DownloadFormDTO>> dfdsr = formClient.generateDownloadForms(Arrays.asList(fileId));

        if (!dfdsr.getResult()) {
            return null;
        }
        if (dfdsr.getData().isEmpty()) {
            throw new ResultException("File download failed.", HttpStatus.OK);
        }

        DownloadFormDTO dfd = dfdsr.getData().get(0);

        try (InputStream inputStream = HttpUtil.createRequest(Method.valueOf(StringUtils.upperCase(dfd.getMethod())), dfd.getUrl()).execute(false).bodyStream()) {
            image = ImageIO.read(inputStream);
        } catch (IOException e) {
            throw new ResultException("File download failed.", HttpStatus.OK);
        }

        return image;
    }

    private void drawLayers(Position basePosition, Graphics graphics, List<Layer> layers, Map<String, Object> params) {
        if (layers != null) {
            for (Layer layer : layers) {

                Position p = basePosition.add(getPosition(layer.getPosition()));
                Size s = layer.getSize();

                Image layerImage;

                switch (layer.getType()) {
                    case WORD: {
                        // 获取字体
                        Font font = getFont(layer.getFont(), s);
                        // 获取内容
                        LayerContent content = layer.getContent();
                        String words = getWords(content, params);
                        int length = words.length();
                        length = length == 0 ? 1 : length;
                        // 创建层图
                        layerImage = new BufferedImage(length * font.getSize(), font.getSize(), BufferedImage.TYPE_INT_ARGB);
                        // 绘制文字
                        Graphics lg = layerImage.getGraphics();
                        lg.setFont(font);
                        lg.setColor(getColor(content.getWordColor()));
                        // 以居中的方式绘制到画布
                        Graphics2D g2 = (Graphics2D)lg;
                        FontRenderContext context = g2.getFontRenderContext();
                        Rectangle2D bounds = font.getStringBounds(words, context);
                        double x = (layerImage.getWidth(null) - bounds.getWidth()) / 2;
                        double y = (layerImage.getHeight(null) - bounds.getHeight()) / 2;
                        double ascent = -bounds.getY();
                        double baseY = y + ascent;
                        lg.drawString(words, (int)x, (int)baseY);
                    }break;
                    case IMAGE: {
                        LayerContent content = layer.getContent();
                        layerImage = getImage(content, params);
                    }break;
                    default: throw new ResultException(MessageConsts.NOT_SUPPORT_LAYER_TYPE + "：" + layer.getType(), HttpStatus.OK);
                }

                if (layerImage != null) {
                    // 如果大小为空则使用原图大小 否则缩放图片
                    if (s == null || LayerType.WORD.equals(layer.getType())) {
                        s = new Size(layerImage.getWidth(null), layerImage.getHeight(null));
                    } else {
                        Image scaledInstance = layerImage.getScaledInstance(s.getWidth(), s.getHeight(), BufferedImage.SCALE_SMOOTH);
                        layerImage = new BufferedImage(scaledInstance.getWidth(null), scaledInstance.getHeight(null), BufferedImage.TYPE_INT_ARGB);
                        layerImage.getGraphics().drawImage(scaledInstance, 0, 0, null);
                    }
                    resetOriginPosition(p, s, getOriginPosition(layer.getOriginPosition()));
                    // 绘制下级图层
                    if (layer.getLayers() != null) {
                        drawLayers(new Position(p.getX(), p.getY()), layerImage.getGraphics(), layer.getLayers(), params);
                    }
                    graphics.drawImage(layerImage, p.getX(), p.getY(), s.getWidth(), s.getHeight(), null);
                }

            }
        }
    }

    /**
     * 保存图片到文件服务器
     * @param image
     * @return
     */
    private GenericResult<UploadFileDTO> saveImage(BufferedImage image) {

        // 生成上传表单
        GenericResult<List<UploadFormDTO>> ufdsr = formClient.generateUploadForms(1);

        if (!ufdsr.getResult()) {
            return Results.failedGeneric(ufdsr.getMessage(), HttpStatus.OK);
        }
        if (ufdsr.getData().isEmpty()) {
            throw new ResultException("File upload failed", HttpStatus.OK);
        }

        UploadFormDTO ufd = ufdsr.getData().get(0);

        try {
            HttpResponse response = HttpUtil.createRequest(
                    Method.valueOf(StringUtils.upperCase(ufd.getMethod()))
                    , ufd.getUrl()
            )
                    .header("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE, true)
                    .form(ufd.getParameterName(), imagePngToResource(image))
                    .execute(false);

            String body = response.body();

            GenericResult<UploadFileDTO> ufdr = JSONObject.parseObject(body, new TypeReference<GenericResult<UploadFileDTO>>() {});
            if (ufdr == null) {
                throw new IllegalStateException("File upload failed.");
            }
            return ufdr;
        } catch (Exception e) {
            return Results.failedGeneric(MessageConsts.FILE_UPLOAD_FAILED, HttpStatus.OK);
        }

    }

    /**
     * 将图片转换为Resource对象
     * @param image
     * @return
     * @throws IOException
     */
    private Resource imagePngToResource(BufferedImage image) throws IOException {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(image, PNG_FORMAT, baos);
            byte[] bytes = baos.toByteArray();
            if (bytes.length <= 0) {
                throw new ResultException(MessageConsts.CARD_GENERATE_FAILED, HttpStatus.OK);
            }
            return new BytesResource(bytes, DEFAULT_PNG_IMAGE_CARD_NAME);
        }
    }
}
