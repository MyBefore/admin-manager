package com.cat.admin.manager.employee.core.service.form;

import com.cat.admin.manager.file.manager.shared.dto.form.DownloadFormDTO;

import java.util.List;

public interface FormService {

    /**
     * 生成form的下载表单
     * @param ids
     * @return
     */
    List<DownloadFormDTO> generateDownloadForms(List<String> ids);

}
