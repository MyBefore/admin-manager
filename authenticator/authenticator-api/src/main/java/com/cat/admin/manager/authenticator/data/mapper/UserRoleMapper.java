package com.cat.admin.manager.authenticator.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cat.admin.manager.authenticator.domain.entity.Authority;
import com.cat.admin.manager.authenticator.domain.entity.UserRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserRoleMapper extends BaseMapper<UserRole> {

    /**
     * 批量插入用户角色
     * @param userRoles
     * @return
     */
    int insertBatch(@Param("userRoles") List<UserRole> userRoles);

    /**
     * 获取用户权限
     * @param id 用户id
     * @return
     */
    List<Authority> getAuthority(@Param("id") String id);

}
