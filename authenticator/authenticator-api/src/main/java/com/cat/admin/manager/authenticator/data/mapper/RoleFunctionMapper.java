package com.cat.admin.manager.authenticator.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cat.admin.manager.authenticator.domain.entity.RoleFunction;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface RoleFunctionMapper extends BaseMapper<RoleFunction> {

    /**
     * 批量插入角色功能
     * @param roleFunctions
     * @return
     */
    int insertBatch(@Param("roleFunctions") List<RoleFunction> roleFunctions);

}
