package com.cat.admin.manager.authenticator.core.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;

@Configuration
public class RedisConfig {

    /**
     * 配置oauth2的token存储的redis
     * @return
     */
    @Bean("oauth2Redis")
    @ConfigurationProperties("spring.redis.oauth2-redis")
    public RedisConnectionFactory oauth2Redis() {
        return new LettuceConnectionFactory();
    }

    /**
     * 配置验证码存储到redis
     * @return
     */
    @Primary
    @Bean("primary")
    @ConfigurationProperties("spring.redis.primary")
    public RedisConnectionFactory captchaRedis(){
        return new LettuceConnectionFactory();
    }

}
