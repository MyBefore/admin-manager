package com.cat.admin.manager.authenticator.core.intergration;

import com.cat.admin.manager.sms.shared.api.SMSApi;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

@FeignClient("sms")
@Component
public interface SMSClient extends SMSApi {
}
