package com.cat.admin.manager.authenticator.core.consts;

public class MessageConsts {

    public static final String ROLE_NOT_FOUND = "角色未找到";
    public static final String NOT_DELETE_ANY_DATA = "未能删除任何数据";
    public static final String NOT_SUPPORT_CAPTCHA = "不支持的验证码";
    public static final String CAPTCHA_NOT_EXISTS = "验证码不存在";
    public static final String USERNAME_CANNOT_IS_BLANK = "用户名不能为空";
    public static final String CAPTCHA_CANNOT_IS_BLANK = "验证码不能为空";
    public static final String CAPTCHA_VALIDATE_FAILED = "验证码验证失败";
    public static final String RANDOM_CANNOT_IS_BLANK = "随机码不能为空";
}
