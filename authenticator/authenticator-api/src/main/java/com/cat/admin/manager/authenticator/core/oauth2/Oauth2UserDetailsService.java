package com.cat.admin.manager.authenticator.core.oauth2;

import com.cat.admin.manager.authenticator.core.intergration.UserClient;
import com.cat.admin.manager.authenticator.core.service.user.UserService;
import com.cat.admin.manager.authenticator.shared.dto.user.Oauth2AuthorityUserDetails;
import com.cat.admin.manager.authenticator.shared.dto.user.UserAuthority;
import com.cat.admin.manager.user.shared.dto.user.ProtectedUserDTO;
import com.cat.commons.result.core.GenericResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

@AllArgsConstructor
@Data
public class Oauth2UserDetailsService implements UserDetailsService {

    private UserClient userClient;

    private UserService userService;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        GenericResult<ProtectedUserDTO> pudgr = userClient.get(username);

        if (!pudgr.getResult()) {
            throw new UsernameNotFoundException(pudgr.getMessage());
        }

        return transform(pudgr.getData());
    }

    private UserDetails transform(ProtectedUserDTO user) {

        UserAuthority authority = userService.getAuthority(user.getId()).getData();

        return new Oauth2AuthorityUserDetails(user.getUsername(), user.getPassword(), transforms(authority), authority);
    }

    private Set<GrantedAuthority> transforms(UserAuthority authority) {
        if (authority.getRoles() == null || authority.getRoles().isEmpty()) {
            return Collections.EMPTY_SET;
        }
        return authority.getRoles().stream().map(r -> new SimpleGrantedAuthority(r.getId())).collect(Collectors.toSet());
    }
}
