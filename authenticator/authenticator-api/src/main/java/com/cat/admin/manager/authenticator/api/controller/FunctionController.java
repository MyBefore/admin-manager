package com.cat.admin.manager.authenticator.api.controller;

import com.cat.admin.manager.authenticator.core.service.function.FunctionService;
import com.cat.admin.manager.authenticator.shared.dto.function.FunctionDTO;
import com.cat.commons.result.core.GenericResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;

@Api("功能管理接口")
@Validated
@RestController
@RequestMapping("functions")
public class FunctionController {

    @Autowired
    private FunctionService functionService;

    @ApiOperation("通过多个id批量查询功能")
    @GetMapping(params = {"ids"})
    public GenericResult<List<FunctionDTO>> get(
            @ApiParam(value = "功能的id集合", required = true) @RequestParam("ids") @NotNull List<String> ids
    ) {
        return functionService.get(ids);
    }

    @ApiOperation("通过关键字模糊查询功能呢列表")
    @GetMapping
    public GenericResult<List<FunctionDTO>> get(
            @ApiParam("功能名称关键字") @RequestParam(value = "keywords", required = false, defaultValue = "") String keywords
    ) {
        return functionService.get(keywords);
    }

}
