package com.cat.admin.manager.authenticator.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 角色
 */
@Data
@TableName("role")
public class Role {

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";

    @TableId(value = "id", type = IdType.UUID)
    private String id;

    @TableField("name")
    private String name;

}
