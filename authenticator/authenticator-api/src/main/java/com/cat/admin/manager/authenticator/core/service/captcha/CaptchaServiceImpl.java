package com.cat.admin.manager.authenticator.core.service.captcha;

import com.cat.admin.manager.authenticator.core.configuration.CaptchaConfig;
import com.cat.admin.manager.authenticator.core.consts.MessageConsts;
import com.cat.admin.manager.authenticator.core.dto.captcha.AddCaptchaRequest;
import com.cat.admin.manager.authenticator.core.intergration.SMSClient;
import com.cat.admin.manager.authenticator.core.intergration.UserClient;
import com.cat.admin.manager.sms.shared.dto.sms.AddSMSCaptchaRequest;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.Result;
import com.cat.commons.result.core.Results;
import com.cat.commons.result.core.exception.ResultException;
import com.cat.commons.time.DateTimeUtils;
import com.cat.ss.manager.captcha.MathImageCaptcha;
import com.octo.captcha.Captcha;
import com.octo.captcha.CaptchaFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Service
public class CaptchaServiceImpl implements CaptchaService {

    @Autowired
    private CaptchaFactory captchaFactory;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private UserClient userClient;

    @Autowired
    private SMSClient smsClient;

    @Autowired
    private CaptchaConfig.Config captcha;

    /**
     * 生成手机验证码的对象
     */
    private Random phoneRandom = new SecureRandom(String.valueOf(DateTimeUtils.getUTCCurrentTimeMillis()).getBytes());

    @Override
    public void addImage(AddCaptchaRequest request, HttpServletResponse response) throws IOException {

        Result exists = userClient.exists(request.getUsername());
        if (!exists.getResult()) {
            throw new ResultException(exists.getMessage(), exists.getStatus());
        }

        Captcha captcha = captchaFactory.getCaptcha();

        if (!(captcha instanceof MathImageCaptcha)) {
            throw new ResultException(MessageConsts.NOT_SUPPORT_CAPTCHA, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        MathImageCaptcha c = (MathImageCaptcha) captcha;
        // 存放验证码到redis
        setCaptcha(request.getUsername(), request.getRandom(), c.getResponse(), request.getEffectiveDuration());
        // 输出验证码
        response.setContentType(MediaType.IMAGE_PNG_VALUE);
        ImageIO.write(c.getImageChallenge(), "png", response.getOutputStream());
    }

    @Override
    public Result addPhone(AddCaptchaRequest request, HttpServletResponse response) {

        // 查询redis中是否存在
        ValueOperations<String, String> vos = stringRedisTemplate.opsForValue();
        String random = vos.get(generateKey(this.captcha.getRedis().getKeyPrefix(), request.getUsername(), request.getRandom()));

        // 校验空 产生必要的字符
        if (StringUtils.isBlank(random) || !this.captcha.getSms().getLengths().contains(random.length())){
            random = phoneRandom();
        }

        // 存放到redis
        setCaptcha(request.getUsername(), request.getRandom(), random, request.getEffectiveDuration());

        // 向用户发送短信
        AddSMSCaptchaRequest ascr = new AddSMSCaptchaRequest();
        ascr.setPhone(request.getUsername());
        ascr.setCode(random);
        ascr.setEfictiveTime(Long.valueOf(request.getEffectiveDuration()));

        // 发送并返回结果
        return smsClient.addCaptcha(ascr);
    }

    @Override
    public GenericResult<String> getCaptcha(String username, String random) {

        ValueOperations<String, String> vos = stringRedisTemplate.opsForValue();

        String code = vos.get(generateKey(this.captcha.getRedis().getKeyPrefix(), username, random));

        if (StringUtils.isBlank(code)) {
            return Results.failedGeneric(MessageConsts.CAPTCHA_NOT_EXISTS, HttpStatus.OK);
        }

        return Results.successGeneric(code);
    }

    @Override
    public Result addCaptcha(String username, String random, String captchaCode, long efictiveTime) {
        setCaptcha(username, random, captchaCode, efictiveTime);
        return Results.success();
    }

    @Override
    public GenericResult<BigInteger> getLoginFailedCount(String username) {
        ValueOperations<String, String> vos = stringRedisTemplate.opsForValue();

        String count = vos.get(generateKey(this.captcha.getLogin().getRedis().getErrorCountKeyPrefix(), username));

        try {
            BigInteger c = new BigInteger(count);
            return Results.successGeneric(c);
        } catch (Exception e) {
            return Results.successGeneric(BigInteger.ZERO);
        }
    }

    @Override
    public Result loginFailedCountIncr(String username, long efictiveTime) {
        ValueOperations<String, String> vos = stringRedisTemplate.opsForValue();
        String key = generateKey(this.captcha.getLogin().getRedis().getErrorCountKeyPrefix(), username);
        // 值自增1
        vos.increment(key, 1);
        // 设置失效时间
        stringRedisTemplate.expire(key, efictiveTime, TimeUnit.MILLISECONDS);
        return Results.success();
    }

    @Override
    public Result removeCaptcha(String username, String random) {
        // 移除验证码
        stringRedisTemplate.delete(generateKey(this.captcha.getRedis().getKeyPrefix(), username, random));
        return Results.success();
    }

    @Override
    public Result removeLoginFailedCount(String username) {
        // 移除登录失败次数
        stringRedisTemplate.delete(generateKey(this.captcha.getLogin().getRedis().getErrorCountKeyPrefix(), username));
        return Results.success();
    }

    private String phoneRandom() {
        StringBuffer buff = new StringBuffer();
        List<Integer> lengths = this.captcha.getSms().getLengths();
        int length = lengths.get(phoneRandom.nextInt(lengths.size()));
        for (int i = 0; i < length; i++) {
            buff.append(phoneRandom.nextInt(10));
        }
        return buff.toString();
    }

    /**
     * 生成存放到redis的key名称
     * @param prefix
     * @param username
     * @param random
     * @return
     */
    private String generateKey(String prefix, String username, String random) {
        return  prefix + "_" + username + "_" + random;
    }

    /**
     * 生成存放到redis的key名称
     * @param prefix
     * @param username
     * @return
     */
    private String generateKey(String prefix, String username) {
        return prefix + "_" + username;
    }

    /**
     * 将验证码存放到redis
     * @param username
     * @param random
     * @param captchaCode
     * @param efictiveTime
     */
    private void setCaptcha(String username, String random, String captchaCode, long efictiveTime) {
        ValueOperations<String, String> vos = stringRedisTemplate.opsForValue();
        // 存放到redis
        vos.set(
                generateKey(this.captcha.getRedis().getKeyPrefix(), username, random),
                captchaCode,
                efictiveTime,
                TimeUnit.MILLISECONDS
        );
    }
}
