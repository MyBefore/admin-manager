package com.cat.admin.manager.authenticator.core.service.user;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cat.admin.manager.authenticator.core.service.role.RoleService;
import com.cat.admin.manager.authenticator.data.mapper.UserRoleMapper;
import com.cat.admin.manager.authenticator.domain.entity.Authority;
import com.cat.admin.manager.authenticator.domain.entity.UserRole;
import com.cat.admin.manager.authenticator.shared.dto.function.FunctionDTO;
import com.cat.admin.manager.authenticator.shared.dto.role.RoleAuthority;
import com.cat.admin.manager.authenticator.shared.dto.role.RoleDTO;
import com.cat.admin.manager.authenticator.shared.dto.user.AddUserRoleRequest;
import com.cat.admin.manager.authenticator.shared.dto.user.UserAuthority;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Autowired
    private RoleService roleService;

    @Override
    public GenericResult<UserAuthority> getAuthority(String id) {
        return Results.successGeneric(transform(id, userRoleMapper.getAuthority(id)));
    }

    @Override
    public GenericResult<List<RoleDTO>> getRoles(String id) {
        return roleService.getByUserId(id);
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    @Override
    public GenericResult<List<RoleDTO>> editRoles(String id, AddUserRoleRequest request) {

        // 删除以前的关系
        userRoleMapper.delete(new QueryWrapper<UserRole>().eq(UserRole.COLUMN_USER_ID, id));

        if (request.getRoleIds().isEmpty()) {
            return Results.successGeneric(Collections.EMPTY_LIST);
        }

        userRoleMapper.insertBatch(request.getRoleIds().stream().distinct().map(rid -> new UserRole(null, id, rid)).collect(Collectors.toList()));

        return roleService.getByUserId(id);
    }

    private UserAuthority transform(String userId, List<Authority> authoritys) {
        UserAuthority ua = new UserAuthority();
        ua.setId(userId);
        ua.setRoles(new ArrayList<>());
        authoritys
                .stream()
                // 过滤用户
                .filter(a -> userId.equals(a.getUserId()))
                .forEach(a -> {
                    RoleAuthority ra = ua.getRoles().stream().filter(u -> a.getRoleId().equals(u.getId())).findFirst().orElse(null);
                    if (ra == null) {
                        ra = new RoleAuthority();
                        ra.setId(a.getRoleId());
                        ra.setName(a.getRoleName());
                        ra.setFunctions(new ArrayList<>());
                        FunctionDTO fDto = new FunctionDTO();
                        fDto.setId(a.getFunctionId());
                        fDto.setCode(a.getFunctionCode());
                        fDto.setName(a.getFunctionName());
                        ra.getFunctions().add(fDto);
                        ua.getRoles().add(ra);
                    } else {
                        FunctionDTO fDto = ra.getFunctions().stream().filter(f -> a.getFunctionId().equals(f.getId())).findFirst().orElse(null);
                        if (fDto == null) {
                            fDto = new FunctionDTO();
                            fDto.setId(a.getFunctionId());
                            fDto.setName(a.getFunctionName());
                            fDto.setCode(a.getFunctionCode());
                            ra.getFunctions().add(fDto);
                        }
                    }
                });
        return ua;
    }
}
