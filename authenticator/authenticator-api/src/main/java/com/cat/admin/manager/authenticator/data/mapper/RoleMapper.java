package com.cat.admin.manager.authenticator.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cat.admin.manager.authenticator.domain.entity.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface RoleMapper extends BaseMapper<Role> {

    List<Role> selectByUserId(@Param("userId") String userId);

}
