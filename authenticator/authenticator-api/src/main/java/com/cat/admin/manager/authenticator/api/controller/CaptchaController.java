package com.cat.admin.manager.authenticator.api.controller;

import com.cat.admin.manager.authenticator.core.dto.captcha.AddCaptchaRequest;
import com.cat.admin.manager.authenticator.core.service.captcha.CaptchaService;
import com.cat.commons.result.core.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;

@Api("验证码接口")
@Validated
@RestController
@RequestMapping("captchas")
public class CaptchaController {

    @Autowired
    private CaptchaService captchaService;

    @ApiOperation("添加图片验证码接口")
    @PostMapping("image")
    public void addImage(@NotNull @Valid @RequestBody AddCaptchaRequest request, HttpServletResponse response) throws IOException {
        captchaService.addImage(request, response);
    }

    @ApiOperation("添加手机验证码接口")
    @PostMapping("phone")
    public Result addPhone(@NotNull @Valid @RequestBody AddCaptchaRequest request, HttpServletResponse response) {
        return captchaService.addPhone(request, response);
    }

}
