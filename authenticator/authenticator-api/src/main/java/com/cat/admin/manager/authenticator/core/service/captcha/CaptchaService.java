package com.cat.admin.manager.authenticator.core.service.captcha;

import com.cat.admin.manager.authenticator.core.dto.captcha.AddCaptchaRequest;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.Result;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;

/**
 * 验证码的操作
 */
public interface CaptchaService {
    /**
     * 创建图片验证码
     * @param request 创建请求对象
     * @param response 返回对象
     */
    void addImage(AddCaptchaRequest request, HttpServletResponse response) throws IOException;

    /**
     * 创建手机验证码
     * @param request 创建请求对象
     * @param response 返回对象
     */
    Result addPhone(AddCaptchaRequest request, HttpServletResponse response);

    /**
     * 获取验证码
     * @param username
     * @param random
     * @return
     */
    GenericResult<String> getCaptcha(String username, String random);

    /**
     * 添加验证码
     * @param username 用户名
     * @param random 随机码
     * @param captchaCode 验证码
     * @param efictiveTime 失效时间
     * @return
     */
    Result addCaptcha(String username, String random, String captchaCode, long efictiveTime);

    /**
     * 获取登录失败次数
     * @param username
     * @return
     */
    GenericResult<BigInteger> getLoginFailedCount(String username);

    /**
     * 登录失败次数加1
     * @param username 用户名
     * @param efictiveTime 失效时间
     * @return
     */
    Result loginFailedCountIncr(String username, long efictiveTime);

    /**
     * 移除登录验证码
     * @param username
     * @param random
     * @return
     */
    Result removeCaptcha(String username, String random);

    /**
     * 移除登录失败次数
     * @param username 用户名
     * @return
     */
    Result removeLoginFailedCount(String username);
}
