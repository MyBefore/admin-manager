package com.cat.admin.manager.authenticator.core.service.role;

import com.cat.admin.manager.authenticator.shared.dto.function.FunctionDTO;
import com.cat.admin.manager.authenticator.shared.dto.role.AddRoleFunctionRequest;
import com.cat.admin.manager.authenticator.shared.dto.role.AddRoleRequest;
import com.cat.admin.manager.authenticator.shared.dto.role.EditRoleRequest;
import com.cat.admin.manager.authenticator.shared.dto.role.RoleDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.PagedResult;
import com.cat.commons.result.core.Result;

import java.util.List;

/**
 * 角色的操作
 */
public interface RoleService {

    /**
     * 添加角色
     * @param request
     * @return
     */
    GenericResult<RoleDTO> add(AddRoleRequest request);

    /**
     * 编辑角色
     * @param id
     * @param request
     * @return
     */
    GenericResult<RoleDTO> edit(String id, EditRoleRequest request);

    /**
     * 批量删除角色
     * @param ids
     * @return
     */
    Result delete(List<String> ids);

    /**
     * 根据关键字分页查询列表
     * @param pageIndex
     * @param pageSize
     * @param keywords
     * @return
     */
    PagedResult<RoleDTO> page(Integer pageIndex, Integer pageSize, String keywords);

    /**
     * 根据关键字获取列表
     * @param keywords
     * @return
     */
    GenericResult<List<RoleDTO>> get(String keywords);

    /**
     * 通过用户id查询
     * @return
     */
    GenericResult<List<RoleDTO>> getByUserId(String userId);

    /**
     * 根据id查询所有的functions
     * @param id
     * @return
     */
    GenericResult<List<FunctionDTO>> getFunctions(String id);

    /**
     * 根据id编辑所有的functions
     * @param id
     * @param request
     * @return
     */
    GenericResult<List<FunctionDTO>> editFunctions(String id, AddRoleFunctionRequest request);
}
