package com.cat.admin.manager.authenticator.api.controller;

import com.cat.admin.manager.authenticator.core.service.role.RoleService;
import com.cat.admin.manager.authenticator.shared.dto.function.FunctionDTO;
import com.cat.admin.manager.authenticator.shared.dto.role.AddRoleFunctionRequest;
import com.cat.admin.manager.authenticator.shared.dto.role.AddRoleRequest;
import com.cat.admin.manager.authenticator.shared.dto.role.EditRoleRequest;
import com.cat.admin.manager.authenticator.shared.dto.role.RoleDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.PagedResult;
import com.cat.commons.result.core.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Api("角色管理接口")
@Validated
@RestController
@RequestMapping("roles")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @ApiOperation("添加角色")
    @PostMapping
    public GenericResult<RoleDTO> add(@Valid @RequestBody @NotNull AddRoleRequest request) {
        return roleService.add(request);
    }

    @ApiOperation("编辑角色")
    @PutMapping("{id}")
    public GenericResult<RoleDTO> edit(
            @ApiParam(value = "id", required = true) @NotBlank @PathVariable("id") String id,
            @Valid @RequestBody EditRoleRequest request
    ) {
        return roleService.edit(id, request);
    }

    @ApiOperation("批量删除角色")
    @DeleteMapping(params = {"ids"})
    public Result delete(
            @ApiParam(value = "id集合", required = true) @NotNull @RequestParam("ids") List<String> ids
    ) {
        return roleService.delete(ids);
    }

    @ApiOperation("根据关键字分页查询列表")
    @GetMapping(params = {"pageIndex", "pageSize"})
    public PagedResult<RoleDTO> page(
            @ApiParam(value = "页码", required = true) @NotNull @RequestParam("pageIndex") Integer pageIndex,
            @ApiParam(value = "条数", required = true) @NotNull @RequestParam("pageSize") Integer pageSize,
            @ApiParam(value = "关键字") @RequestParam(value = "keywords", required = false, defaultValue = "") String keywords
    ) {
        return roleService.page(pageIndex, pageSize, keywords);
    }

    @ApiOperation("根据关键字获取列表")
    @GetMapping
    public GenericResult<List<RoleDTO>> get(@ApiParam(value = "关键字") @RequestParam(value = "keywords", required = false, defaultValue = "") String keywords) {
        return roleService.get(keywords);
    }

    @ApiOperation("根据id查询所有的functions")
    @GetMapping("{id}/functions")
    public GenericResult<List<FunctionDTO>> getFunctions(
            @ApiParam(value = "id", required = true) @NotBlank @PathVariable("id") String id
    ) {
        return roleService.getFunctions(id);
    }

    @ApiOperation("根据id编辑所有的functions")
    @PutMapping("{id}/functions")
    public GenericResult<List<FunctionDTO>> editFunctions(
            @ApiParam(value = "id", required = true) @NotBlank @PathVariable("id") String id,
            @RequestBody @Valid AddRoleFunctionRequest request
            ) {
        return roleService.editFunctions(id, request);
    }

}
