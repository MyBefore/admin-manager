package com.cat.admin.manager.authenticator.core.oauth2.filter;

import com.alibaba.fastjson.JSONObject;
import com.cat.admin.manager.authenticator.core.configuration.CaptchaConfig;
import com.cat.admin.manager.authenticator.core.consts.MessageConsts;
import com.cat.admin.manager.authenticator.core.service.captcha.CaptchaService;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.Results;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.common.util.OAuth2Utils;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.stream.Collectors;

public class CaptchaFilter extends GenericFilterBean {

    public static final String RANDOM = "random";

    public static final String GRANT_TYPE_PASSWORD = "password";

    public static final String USERNAME = "username";

    private RequestMatcher matcher;

    private CaptchaService captchaService;

    private CaptchaConfig.Config captcha;

    public CaptchaFilter(CaptchaConfig.Config captcha, CaptchaService captchaService) {
        matcher = new OrRequestMatcher(
                captcha.getLogin().getEndpoints()
                        .stream()
                        .map(m -> new AntPathRequestMatcher(m))
                        .collect(Collectors.toList())
        );
        this.captchaService = captchaService;
        this.captcha = captcha;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        if (!matcher.matches(req)) {
            // 匹配不成功
            chain.doFilter(request, response);
            return;
        }

        if (prepare(req, resp)) {
            chain.doFilter(request, response);
        }
        complete(req, resp);

    }

    private boolean prepare(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String grantType = req.getParameter(OAuth2Utils.GRANT_TYPE);
        // 不是密码类型直接返回true
        if (!GRANT_TYPE_PASSWORD.equals(grantType)) {
            return true;
        }
        // 根据用户名称和random获取是否存在验证码
        String username = req.getParameter(USERNAME);
        String random = req.getParameter(RANDOM);

        // 验证用户名参数
        if (StringUtils.isBlank(username)) {
            writeFailedResult(resp, MessageConsts.USERNAME_CANNOT_IS_BLANK, HttpStatus.BAD_REQUEST);
            return false;
        }

        // 验证随机码参数
        if (StringUtils.isBlank(random)) {
            if (needCaptcha(username)) {
                writeFailedResult(resp, MessageConsts.RANDOM_CANNOT_IS_BLANK, HttpStatus.BAD_REQUEST);
                return false;
            } else {
                return true;
            }
        }

        // 获取验证码
        GenericResult<String> sgr = captchaService.getCaptcha(username, random);
        // 验证码获取失败 验证错误次数
        if (!sgr.getResult() || StringUtils.isBlank(sgr.getData())) {
            if (needCaptcha(username)) {
                writeFailedResult(resp, MessageConsts.CAPTCHA_CANNOT_IS_BLANK, HttpStatus.BAD_REQUEST);
                return false;
            } else {
                return true;
            }
        }

        // 如果验证码不正确
        if (!sgr.getData().equalsIgnoreCase(req.getParameter(this.captcha.getLogin().getCaptchaParameterName()))) {
            writeFailedResult(resp, MessageConsts.CAPTCHA_VALIDATE_FAILED, HttpStatus.BAD_REQUEST);
            return false;
        }

        return true;
    }

    private void complete(HttpServletRequest req, HttpServletResponse resp) {

        String grantType = req.getParameter(OAuth2Utils.GRANT_TYPE);
        // 不是密码类型直接返回true
        if (!GRANT_TYPE_PASSWORD.equals(grantType)) {
            return;
        }
        // 根据用户名称和random获取是否存在验证码
        String username = req.getParameter(USERNAME);

        // 验证用户名参数
        if (StringUtils.isBlank(username)) {
            return;
        }

        String random = req.getParameter(RANDOM);
        if (StringUtils.isNotBlank(random)){
            // 清除验证码
            captchaService.removeCaptcha(username, random);
        }

        if (!isLoginSuccess(resp)) {
            if (isRecordLoginError(resp)) {
                // 记录错误次数加1，并刷新失效时间
                captchaService.loginFailedCountIncr(username, this.captcha.getLogin().getRedis().getErrorCountTimeout());
            }
            // 登录失败
            return;
        }

        // 登录成功 清除失败次数
        captchaService.removeLoginFailedCount(username);
    }

    /**
     * 根据验证码判断是否登录成功
     * @param resp
     * @return
     */
    private boolean isLoginSuccess(HttpServletResponse resp) {
        return HttpStatus.OK.value() == resp.getStatus();
    }

    /**
     * 判断是否记录登录失败
     * @param resp
     * @return
     */
    private boolean isRecordLoginError(HttpServletResponse resp) {
        return HttpStatus.BAD_REQUEST.value() == resp.getStatus();
    }

    /**
     * 判断是否需要验证码
     * @param username
     * @return
     */
    private boolean needCaptcha(String username) {
        GenericResult<BigInteger> bgr = captchaService.getLoginFailedCount(username);
        if (bgr.getResult()) {
            return BigInteger.valueOf(captcha.getLogin().getRedis().getErrorCountMinIgonre()).compareTo(bgr.getData()) < 0;
        }
        return false;
    }

    /**
     * 输出错误的结果
     * @param response
     * @param message
     * @param status
     */
    private void writeFailedResult(HttpServletResponse response, String message, HttpStatus status) throws IOException {
        response.setStatus(status.value());
        PrintWriter writer = response.getWriter();
        writer.print(JSONObject.toJSONString(Results.failedGeneric(message, status)));
        writer.flush();
        writer.close();
    }

}
