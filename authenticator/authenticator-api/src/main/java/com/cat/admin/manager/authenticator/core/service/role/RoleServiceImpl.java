package com.cat.admin.manager.authenticator.core.service.role;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cat.admin.manager.authenticator.core.consts.MessageConsts;
import com.cat.admin.manager.authenticator.core.service.function.FunctionService;
import com.cat.admin.manager.authenticator.data.mapper.RoleFunctionMapper;
import com.cat.admin.manager.authenticator.data.mapper.RoleMapper;
import com.cat.admin.manager.authenticator.domain.entity.Role;
import com.cat.admin.manager.authenticator.domain.entity.RoleFunction;
import com.cat.admin.manager.authenticator.shared.dto.function.FunctionDTO;
import com.cat.admin.manager.authenticator.shared.dto.role.AddRoleFunctionRequest;
import com.cat.admin.manager.authenticator.shared.dto.role.AddRoleRequest;
import com.cat.admin.manager.authenticator.shared.dto.role.EditRoleRequest;
import com.cat.admin.manager.authenticator.shared.dto.role.RoleDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.PagedResult;
import com.cat.commons.result.core.Result;
import com.cat.commons.result.core.Results;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private RoleFunctionMapper roleFunctionMapper;

    @Autowired
    private FunctionService functionService;

    @Override
    public GenericResult<RoleDTO> add(AddRoleRequest request) {
        Role r = transform(request);
        roleMapper.insert(r);
        return Results.successGeneric(transform(r));
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    @Override
    public GenericResult<RoleDTO> edit(String id, EditRoleRequest request) {

        Role role = roleMapper.selectById(id);

        if (role == null) {
            return Results.failedGeneric(MessageConsts.ROLE_NOT_FOUND, HttpStatus.OK);
        }

        roleMapper.updateById(transform(role, request));

        return Results.successGeneric(transform(role));
    }

    @Override
    public Result delete(List<String> ids) {

        int deletes = roleMapper.deleteBatchIds(ids);

        if (deletes <= 0) {
            return Results.failed(MessageConsts.NOT_DELETE_ANY_DATA, HttpStatus.OK);
        }

        return Results.success();
    }

    @Override
    public PagedResult<RoleDTO> page(Integer pageIndex, Integer pageSize, String keywords) {
        IPage<Role> page = roleMapper.selectPage(new Page<>(pageIndex, pageSize), new QueryWrapper<Role>().like(Role.COLUMN_NAME, keywords));
        return Results.successPaged(transforms(page.getRecords()), page.getCurrent(), page.getSize(), page.getTotal());
    }

    @Override
    public GenericResult<List<RoleDTO>> get(String keywords) {
        return Results.successGeneric(transforms(roleMapper.selectList(new QueryWrapper<Role>().like(Role.COLUMN_NAME, keywords))));
    }

    @Override
    public GenericResult<List<RoleDTO>> getByUserId(String userId) {
        return Results.successGeneric(transforms(roleMapper.selectByUserId(userId)));
    }

    @Override
    public GenericResult<List<FunctionDTO>> getFunctions(String id) {
        return functionService.getByRoleId(id);
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    @Override
    public GenericResult<List<FunctionDTO>> editFunctions(String id, AddRoleFunctionRequest request) {

        // 清空以前的关系
        roleFunctionMapper.delete(new QueryWrapper<RoleFunction>().eq(RoleFunction.COLUMN_ROLE_ID, id));

        // 判断是否是空
        if (request.getFunctionIds().isEmpty()) {
            return Results.successGeneric(Collections.EMPTY_LIST);
        }

        // 保存
        roleFunctionMapper.insertBatch(request.getFunctionIds().stream().distinct().map(fid -> new RoleFunction(null, id, fid)).collect(Collectors.toList()));

        return functionService.getByRoleId(id);
    }

    private Role transform(AddRoleRequest request) {
        Role r = new Role();
        BeanUtils.copyProperties(request, r);
        return r;
    }

    private RoleDTO transform(Role role) {
        RoleDTO dto = new RoleDTO();
        BeanUtils.copyProperties(role, dto);
        return dto;
    }

    private Role transform(Role role, EditRoleRequest request) {
        BeanUtils.copyProperties(request, role);
        return role;
    }

    private List<RoleDTO> transforms(List<Role> roles) {
        return roles.stream().map(this::transform).collect(Collectors.toList());
    }
}
