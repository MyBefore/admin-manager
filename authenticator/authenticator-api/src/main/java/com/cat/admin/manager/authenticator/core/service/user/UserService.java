package com.cat.admin.manager.authenticator.core.service.user;


import com.cat.admin.manager.authenticator.shared.dto.role.RoleDTO;
import com.cat.admin.manager.authenticator.shared.dto.user.AddUserRoleRequest;
import com.cat.admin.manager.authenticator.shared.dto.user.UserAuthority;
import com.cat.commons.result.core.GenericResult;

import java.util.List;

/**
 * 用户的操作
 */
public interface UserService {

    /**
     * 获取用户权限
     * @param id
     * @return
     */
    GenericResult<UserAuthority> getAuthority(String id);

    /**
     * 获取用户角色
     * @param id
     * @return
     */
    GenericResult<List<RoleDTO>> getRoles(String id);

    /**
     * 编辑用户角色
     * @param id
     * @param request
     * @return
     */
    GenericResult<List<RoleDTO>> editRoles(String id, AddUserRoleRequest request);
}
