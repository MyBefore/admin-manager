package com.cat.admin.manager.authenticator.domain.entity;

import lombok.Data;

@Data
public class Authority {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 角色id
     */
    private String roleId;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 功能id
     */
    private String functionId;

    /**
     * 功能编码
     */
    private String functionCode;

    /**
     * 功能名称
     */
    private String functionName;

}
