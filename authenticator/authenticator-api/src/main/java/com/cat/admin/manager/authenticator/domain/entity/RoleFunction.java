package com.cat.admin.manager.authenticator.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 角色功能关系表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("role_function")
public class RoleFunction {

    public static final String COLUMN_ROLE_ID = "role_id";

    @TableId(value = "id", type = IdType.UUID)
    private String id;

    @TableField("role_id")
    private String roleId;

    @TableField("function_id")
    private String functionId;
}
