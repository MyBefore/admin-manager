package com.cat.admin.manager.authenticator.api.controller;

import com.cat.admin.manager.authenticator.core.service.user.UserService;
import com.cat.admin.manager.authenticator.shared.dto.role.RoleDTO;
import com.cat.admin.manager.authenticator.shared.dto.user.AddUserRoleRequest;
import com.cat.admin.manager.authenticator.shared.dto.user.UserAuthority;
import com.cat.commons.result.core.GenericResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Api("用户管理接口")
@Validated
@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiModelProperty("获取用户权限")
    @GetMapping("{id}")
    public GenericResult<UserAuthority> getAuthority(
            @ApiParam(value = "id", required = true) @NotBlank @PathVariable("id") String id
    ) {
        return userService.getAuthority(id);
    }

    @ApiOperation("获取用户角色")
    @GetMapping("{id}/roles")
    public GenericResult<List<RoleDTO>> getRoles(
            @ApiParam(value = "id", required = true) @NotBlank @PathVariable("id") String id
    ) {
        return userService.getRoles(id);
    }

    @ApiOperation("编辑用户角色")
    @PutMapping("{id}/roles")
    public GenericResult<List<RoleDTO>> editRoles(
            @ApiParam(value = "id", required = true) @NotBlank @PathVariable("id") String id,
            @Valid @RequestBody AddUserRoleRequest request
            ) {
        return userService.editRoles(id, request);
    }

}
