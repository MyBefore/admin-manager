package com.cat.admin.manager.authenticator.core.service.function;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cat.admin.manager.authenticator.data.mapper.FunctionMapper;
import com.cat.admin.manager.authenticator.domain.entity.Function;
import com.cat.admin.manager.authenticator.shared.dto.function.FunctionDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.Results;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FunctionServiceImpl implements FunctionService {

    @Autowired
    private FunctionMapper functionMapper;

    @Override
    public GenericResult<List<FunctionDTO>> get(List<String> ids) {
        return Results.successGeneric(transforms(functionMapper.selectBatchIds(ids)));
    }

    @Override
    public GenericResult<List<FunctionDTO>> get(String keywords) {
        return Results.successGeneric(transforms(functionMapper.selectList(new QueryWrapper<Function>().like(Function.COLUMN_NAME, keywords))));
    }

    @Override
    public GenericResult<List<FunctionDTO>> getByRoleId(String roleId) {
        return Results.successGeneric(transforms(functionMapper.selectByRoleId(roleId)));
    }

    private List<FunctionDTO> transforms(List<Function> functions) {
        return functions.stream().map(this::transform).collect(Collectors.toList());
    }

    private FunctionDTO transform(Function function) {
        FunctionDTO dto = new FunctionDTO();
        BeanUtils.copyProperties(function, dto);
        return dto;
    }
}
