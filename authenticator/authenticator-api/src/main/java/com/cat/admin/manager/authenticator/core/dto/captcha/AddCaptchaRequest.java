package com.cat.admin.manager.authenticator.core.dto.captcha;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.*;

/**
 * 添加验证码请求
 */
@Data
@ApiModel("添加验证码请求")
public class AddCaptchaRequest {

    @ApiModelProperty(value = "用户名称", required = true)
    @NotBlank
    private String username;

    @ApiModelProperty(value = "随机字符串", required = true)
    @NotBlank
    private String random;

    @ApiModelProperty(value = "有效时长", notes = "取值范围：大于等于30000毫秒，小于等于300000毫秒", required = true)
    @NotNull
    @Max(value = 300000)
    @Min(value = 30000)
    private Long effectiveDuration;
}
