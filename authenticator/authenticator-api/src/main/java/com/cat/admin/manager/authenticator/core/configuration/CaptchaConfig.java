package com.cat.admin.manager.authenticator.core.configuration;

import com.cat.ss.manager.captcha.MathImageCaptchaFactory;
import com.cat.ss.manager.captcha.RandomWordToImage;
import com.octo.captcha.CaptchaFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class CaptchaConfig {

    @Bean
    public CaptchaFactory captchaFactory (){
        return new MathImageCaptchaFactory(new RandomWordToImage());
    }

    @Bean
    @ConfigurationProperties("captcha")
    public Config captcha() {
        return new Config();
    }

    @Data
    public static class Config {

        private RedisConfig redis = new RedisConfig();

        private SMSConfig sms = new SMSConfig();

        private LoginConfig login = new LoginConfig();

        @Data
        public static class RedisConfig {
            private String keyPrefix;
        }

        @Data
        public static class SMSConfig {
            private List<Integer> lengths = new ArrayList<>();
        }

        @Data
        public static class LoginConfig {
            private List<String> endpoints = new ArrayList<>();

            private RedisConfig redis = new RedisConfig();

            private String captchaParameterName;

            @Data
            public static class RedisConfig {
                private String errorCountKeyPrefix;
                private Long errorCountMinIgonre;
                private Long errorCountTimeout;
            }
        }
    }

}
