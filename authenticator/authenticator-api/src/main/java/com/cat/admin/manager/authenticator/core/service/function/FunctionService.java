package com.cat.admin.manager.authenticator.core.service.function;


import com.cat.admin.manager.authenticator.shared.dto.function.FunctionDTO;
import com.cat.commons.result.core.GenericResult;

import java.util.List;

/**
 * 功能的操作
 */
public interface FunctionService {

    /**
     * 通过id集合查询方法
     * @param ids
     * @return
     */
    GenericResult<List<FunctionDTO>> get(List<String> ids);

    /**
     * 通过关键字查询方法
     * @param keywords
     * @return
     */
    GenericResult<List<FunctionDTO>> get(String keywords);

    /**
     * 通过角色id获取功能
     * @param roleId
     * @return
     */
    GenericResult<List<FunctionDTO>> getByRoleId(String roleId);
}
