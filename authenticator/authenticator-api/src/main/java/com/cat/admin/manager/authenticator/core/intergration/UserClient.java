package com.cat.admin.manager.authenticator.core.intergration;

import com.cat.admin.manager.user.shared.api.UserApi;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;


@FeignClient("user")
@Component
public interface UserClient extends UserApi {
}