-- 角色和功能关系表
DROP TABLE IF EXISTS `role_function`;
CREATE TABLE `role_function` (
  `id` varchar(255) NOT NULL COMMENT '主键id',
  `role_id` varchar(255) NOT NULL COMMENT '角色id',
  `function_id` varchar(255) NOT NULL COMMENT '功能id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和功能的关系表';
