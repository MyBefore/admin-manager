/*
Navicat MySQL Data Transfer

Source Server         : localhost.3306.8.0
Source Server Version : 80012
Source Host           : localhost:3306
Source Database       : authenticator

Target Server Type    : MYSQL
Target Server Version : 80012
File Encoding         : 65001

Date: 2019-01-15 16:36:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for function
-- ----------------------------
DROP TABLE IF EXISTS `function`;
CREATE TABLE `function` (
  `id` varchar(255) NOT NULL COMMENT '功能的id',
  `code` varchar(255) NOT NULL COMMENT '功能的唯一code',
  `name` varchar(255) NOT NULL COMMENT '功能名称',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`) COMMENT 'code需要唯一性支持'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='功能表';
