package com.cat.admin.manager.authenticator.client.core.annotation;

import com.cat.admin.manager.authenticator.client.core.consts.MatchType;

import java.lang.annotation.*;

/**
 * 匹配
 */
@Target(ElementType.TYPE_USE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Match {

    /**
     * 匹配的function code
     * @return
     */
    String[] value() default {};

    /**
     * 匹配的模式
     * @return
     */
    MatchType type() default MatchType.AnyMatch;
}
