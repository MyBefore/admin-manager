package com.cat.admin.manager.authenticator.client.core.configuration;

import feign.Feign;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnMissingBean(Feign.Builder.class)
public class FeginBuilderConfig {

    @Bean
    public Feign.Builder builder() {
        return Feign.builder();
    }

}
