package com.cat.admin.manager.authenticator.client.core.consts;

/**
 * 匹配的方式
 */
public enum MatchType {
    /**
     * 任意匹配
     */
    AnyMatch,

    /**
     * 全匹配
     */
    AllMatch

}
