package com.cat.admin.manager.authenticator.client.core.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.cat.admin.manager.authenticator.shared.dto.user.Oauth2AuthorityUserDetails;
import com.cat.admin.manager.authenticator.shared.dto.user.UserAuthority;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Security {

    public static final String USER = "user";

    /**
     * 获取当前登录用户
     * @return
     */
    public static User getUser() {

        Oauth2AuthorityUserDetails user = null;

        try {
            HttpServletRequest request = getRequest();
            user = (Oauth2AuthorityUserDetails) request.getAttribute(USER);
            if (user == null) {
                user = deserialize(getUserString());
                if (user != null) {
                    request.setAttribute(USER, user);
                }
            }
        } catch (Exception e) {}

        return user;
    }

    /**
     * 设置当前登录用户
     * @param user
     */
    public static void setUser(User user) {
        getRequest().setAttribute(USER, user);
    }

    /**
     * 获取user字符串
     * @return
     */
    public static String getUserString() {
        HttpServletRequest request = getRequest();
        String user = request.getHeader(USER);
        if (StringUtils.isBlank(user)) {
            user = request.getParameter(USER);
            if (StringUtils.isBlank(user)) {
                Object u = request.getAttribute(USER);
                // 从attribute中序列化成user字符串
                if (u != null) {
                    user = serialize((User) u);
                }
            }
        }
        return user;
    }

    private static HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
    }

    /**
     * 序列化用户字符串对象
     * @param user
     * @return
     */
    public static String serialize(User user) {
        try {
            return JSONObject.toJSONString(user);
        } catch (Exception e) {}
        return null;
    }

    /**
     * 反序列化用户字符串对象
     * @param userString
     * @return
     */
    public static Oauth2AuthorityUserDetails deserialize(String userString) {

        try {
            JSONObject json = JSONObject.parseObject(userString);
            if (json == null) {
                return null;
            }

            UserAuthority userAuthority = null;
            JSONObject jua = json.getJSONObject("userAuthority");
            if (jua != null) {
                userAuthority = jua.toJavaObject(UserAuthority.class);
            }

            List<SimpleGrantedAuthority> authorities;
            JSONArray ja = json.getJSONArray("authorities");
            if (ja != null) {
                authorities = ja.toJavaObject(new TypeReference<ArrayList<SimpleGrantedAuthority>>(){});
            } else {
                authorities = Collections.EMPTY_LIST;
            }

            return new Oauth2AuthorityUserDetails(
                    json.getString("username"),
                    json.getString("password"),
                    json.getBoolean("enabled"),
                    json.getBoolean("accountNonExpired"),
                    json.getBoolean("credentialsNonExpired"),
                    json.getBoolean("accountNonLocked"),
                    authorities,
                    userAuthority
            );
        } catch (Exception e) {}
        return null;
    }

}
