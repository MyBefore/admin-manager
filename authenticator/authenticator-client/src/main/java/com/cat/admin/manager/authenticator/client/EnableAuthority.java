package com.cat.admin.manager.authenticator.client;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;


/**
 * 开启权限认证生效
 * @see com.cat.admin.manager.authenticator.client.core.annotation.Auth
 */
@Import(AuthorityAutoConfiguration.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnableAuthority {
}
