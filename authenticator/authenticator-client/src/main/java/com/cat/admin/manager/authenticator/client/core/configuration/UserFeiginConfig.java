package com.cat.admin.manager.authenticator.client.core.configuration;

import com.cat.admin.manager.authenticator.client.core.util.Security;
import feign.Feign;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserFeiginConfig implements InitializingBean {

    @Autowired
    private Feign.Builder builder;

    @Override
    public void afterPropertiesSet() {
        builder.requestInterceptor(new UserRequestInterceptor());
    }

    public static class UserRequestInterceptor implements RequestInterceptor {

        /**
         * 利用feign将当前user向下传递
         * @param template
         */
        @Override
        public void apply(RequestTemplate template) {
            String userString = Security.getUserString();
            if (StringUtils.isNotBlank(userString)) {
                template.header(Security.USER,  userString);
            }
        }
    }

}
