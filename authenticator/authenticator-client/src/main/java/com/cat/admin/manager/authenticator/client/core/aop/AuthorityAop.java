package com.cat.admin.manager.authenticator.client.core.aop;

import com.cat.admin.manager.authenticator.client.core.annotation.Auth;
import com.cat.admin.manager.authenticator.client.core.annotation.Match;
import com.cat.admin.manager.authenticator.client.core.consts.MatchType;
import com.cat.admin.manager.authenticator.client.core.consts.MessageConsts;
import com.cat.admin.manager.authenticator.shared.dto.function.FunctionDTO;
import com.cat.admin.manager.authenticator.shared.dto.role.RoleAuthority;
import com.cat.admin.manager.authenticator.shared.dto.user.UserAuthority;
import com.cat.commons.result.core.exception.ResultException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Stream;

@Aspect
@Component
public class AuthorityAop {

    @Order(Ordered.HIGHEST_PRECEDENCE)
    @Around("@annotation(com.cat.admin.manager.authenticator.client.core.annotation.Auth)")
    public Object auth(ProceedingJoinPoint joinPoint) throws Throwable {

        if (!auth(getAuth(joinPoint))) {
            throw new ResultException(MessageConsts.NOT_ACCESS_AUTHORITY, HttpStatus.FORBIDDEN);
        }

        return joinPoint.proceed(joinPoint.getArgs());
    }

    /**
     * 认证注解
     * @param auth
     * @return
     */
    private boolean auth(Auth auth) {

        Authentication authentication = getAuthentication();

        if (authentication == null || !authentication.isAuthenticated()) {
            return false;
        }

        Match[] matchs = auth.matchs();

        if (auth == null || matchs == null || matchs.length <= 0) {
            return false;
        }

        Object principal = authentication.getPrincipal();
        if (!(principal instanceof UserAuthority)) {
            throw new UnsupportedOperationException("Principal is unsupport: " + principal);
        }

        List<FunctionDTO> functions = getFunctions((UserAuthority) principal);
        if (functions == null || functions.isEmpty()) {
            return false;
        }

        return Arrays.stream(matchs).anyMatch(m -> match(m, functions));
    }

    /**
     * 匹配功能
     * @param match
     * @param functions
     * @return
     */
    private boolean match(Match match, List<FunctionDTO> functions){

        if (match.value() == null || match.value().length <= 0) {
            return false;
        }

        List<String> values = Arrays.asList(match.value());

        switch (match.type()) {
            case AllMatch:{
                return functions.stream().allMatch(f -> values.contains(f.getCode()));
            }
            case AnyMatch:{
                return functions.stream().anyMatch(f -> values.contains(f.getCode()));
            }
            default: throw new UnsupportedOperationException("Match type is unsupport.");
        }
    }

    /**
     * 通过用户授权信息获取所拥有的所有功能
     * @param authority
     * @return
     */
    private List<FunctionDTO> getFunctions(UserAuthority authority) {

        @NotNull List<RoleAuthority> roles = authority.getRoles();
        if (roles == null || roles.isEmpty()) {
            return Collections.EMPTY_LIST;
        }

        List<FunctionDTO> functions = new ArrayList<>();

        roles.forEach(r -> {
            @NotNull List<FunctionDTO> fs = r.getFunctions();
            if (fs != null) {
                functions.addAll(fs);
            }
        });

        return functions;
    }

    /**
     * 通过接入点获取认证注解
     * @param joinPoint
     * @return
     * @throws NoSuchMethodException
     */
    private static Auth getAuth(ProceedingJoinPoint joinPoint) throws NoSuchMethodException {
        return getAuth(getTargetMethod(joinPoint));
    }

    /**
     * 通过方法获取认证注解
     * @param method
     * @return
     */
    private static Auth getAuth(Method method) {
        return method.getAnnotation(Auth.class);
    }

    /**
     * 获取目标方法
     * @param joinPoint
     * @return
     * @throws NoSuchMethodException
     */
    private static Method getTargetMethod(ProceedingJoinPoint joinPoint) throws NoSuchMethodException {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        return joinPoint.getTarget().getClass().getMethod(signature.getName(), signature.getParameterTypes());
    }

    /**
     * 获取认证对象
     * @return
     */
    private Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }
}
