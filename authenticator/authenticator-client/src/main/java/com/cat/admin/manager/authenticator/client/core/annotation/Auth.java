package com.cat.admin.manager.authenticator.client.core.annotation;

import java.lang.annotation.*;

/**
 * 权限注解
 * @see com.cat.admin.manager.authenticator.client.core.aop.AuthorityAop
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Auth {
    /**
     * 满足任意的match
     * @return
     */
    Match[] matchs() default {};
}
