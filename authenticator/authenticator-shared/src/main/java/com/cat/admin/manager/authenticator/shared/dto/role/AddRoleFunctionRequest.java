package com.cat.admin.manager.authenticator.shared.dto.role;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@ApiModel("添加角色功能请求")
public class AddRoleFunctionRequest {

    @ApiModelProperty(value = "功能id集合", required = true)
    @NotNull
    private List<String> functionIds;

}
