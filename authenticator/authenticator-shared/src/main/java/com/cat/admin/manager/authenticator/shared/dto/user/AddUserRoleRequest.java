package com.cat.admin.manager.authenticator.shared.dto.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@ApiModel("添加用户角色请求")
public class AddUserRoleRequest {

    @ApiModelProperty(value = "角色的id集合", required = true)
    @NotNull
    private List<String> roleIds;

}
