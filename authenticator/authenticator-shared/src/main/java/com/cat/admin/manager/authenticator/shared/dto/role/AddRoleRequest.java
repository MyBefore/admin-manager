package com.cat.admin.manager.authenticator.shared.dto.role;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel("添加角色请求")
public class AddRoleRequest {

    @ApiModelProperty(value = "角色名称", required = true)
    @NotBlank
    private String name;

}
