package com.cat.admin.manager.authenticator.shared.dto.user;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;

@Data
public class Oauth2AuthorityUserDetails extends User {

    private UserAuthority userAuthority;

    public Oauth2AuthorityUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities, UserAuthority userAuthority) {
        super(username, password, authorities);
        this.userAuthority = userAuthority;
    }

    public Oauth2AuthorityUserDetails(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities, UserAuthority userAuthority) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.userAuthority = userAuthority;
    }
}
