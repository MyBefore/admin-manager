package com.cat.admin.manager.authenticator.shared.dto.function;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@ApiModel("功能描述")
public class FunctionDTO implements Serializable {

    @ApiModelProperty(value = "id", required = true)
    @NotBlank
    private String id;

    @ApiModelProperty(value = "功能编码", required = true)
    @NotBlank
    private String code;

    @ApiModelProperty(value = "功能名称", required = true)
    @NotBlank
    private String name;
}
