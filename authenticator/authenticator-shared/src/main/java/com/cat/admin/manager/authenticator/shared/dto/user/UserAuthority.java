package com.cat.admin.manager.authenticator.shared.dto.user;

import com.cat.admin.manager.authenticator.shared.dto.role.RoleAuthority;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("用户权限")
public class UserAuthority implements Serializable {

    @ApiModelProperty(value = "用户id", required = true)
    @NotBlank
    private String id;

    @ApiModelProperty(value = "角色集合", required = true)
    @NotNull
    private List<RoleAuthority> roles;
}
