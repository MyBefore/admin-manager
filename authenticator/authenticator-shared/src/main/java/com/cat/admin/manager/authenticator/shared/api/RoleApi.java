package com.cat.admin.manager.authenticator.shared.api;

import com.cat.admin.manager.authenticator.shared.dto.function.FunctionDTO;
import com.cat.admin.manager.authenticator.shared.dto.role.AddRoleFunctionRequest;
import com.cat.admin.manager.authenticator.shared.dto.role.AddRoleRequest;
import com.cat.admin.manager.authenticator.shared.dto.role.EditRoleRequest;
import com.cat.admin.manager.authenticator.shared.dto.role.RoleDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.PagedResult;
import com.cat.commons.result.core.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Api("角色管理接口")
@Validated
@RestController
@RequestMapping("roles")
public interface RoleApi {

    @ApiOperation("添加角色")
    @PostMapping
    GenericResult<RoleDTO> add(@Valid @RequestBody @NotNull AddRoleRequest request);

    @ApiOperation("编辑角色")
    @PutMapping("{id}")
    GenericResult<RoleDTO> edit(
            @ApiParam(value = "id", required = true) @NotBlank @PathVariable("id") String id,
            @Valid @RequestBody EditRoleRequest request
    );

    @ApiOperation("批量删除角色")
    @DeleteMapping(params = {"ids"})
    Result delete(
            @ApiParam(value = "id集合", required = true) @NotNull @RequestParam("ids") List<String> ids
    );

    @ApiOperation("根据关键字分页查询列表")
    @GetMapping(params = {"pageIndex", "pageSize"})
    PagedResult<RoleDTO> page(
            @ApiParam(value = "页码", required = true) @NotNull @RequestParam("pageIndex") Integer pageIndex,
            @ApiParam(value = "条数", required = true) @NotNull @RequestParam("pageSize") Integer pageSize,
            @ApiParam(value = "关键字") @RequestParam(value = "keywords", required = false, defaultValue = "") String keywords
    );

    @ApiOperation("根据关键字获取列表")
    @GetMapping
    GenericResult<List<RoleDTO>> get(@ApiParam(value = "关键字") @RequestParam(value = "keywords", required = false, defaultValue = "") String keywords);

    @ApiOperation("根据id查询所有的functions")
    @GetMapping("{id}/functions")
    GenericResult<List<FunctionDTO>> getFunctions(
            @ApiParam(value = "id", required = true) @NotBlank @PathVariable("id") String id
    );

    @ApiOperation("根据id编辑所有的functions")
    @PutMapping("{id}/functions")
    GenericResult<List<FunctionDTO>> editFunctions(
            @ApiParam(value = "id", required = true) @NotBlank @PathVariable("id") String id,
            @RequestBody @Valid AddRoleFunctionRequest request
            );

}
