package com.cat.admin.manager.authenticator.shared.dto.role;

import com.cat.admin.manager.authenticator.shared.dto.function.FunctionDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("角色权限")
public class RoleAuthority extends RoleDTO implements Serializable {

    @ApiModelProperty(value = "功能呢集合", required = true)
    @NotNull
    private List<FunctionDTO> functions;

}
