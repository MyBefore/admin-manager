package com.cat.admin.manager.authenticator.shared.dto.role;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel("角色描述")
public class RoleDTO extends EditRoleRequest {

    @ApiModelProperty(value = "角色id", required = true)
    @NotBlank
    private String id;

}
