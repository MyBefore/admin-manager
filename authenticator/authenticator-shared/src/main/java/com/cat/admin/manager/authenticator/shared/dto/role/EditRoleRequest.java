package com.cat.admin.manager.authenticator.shared.dto.role;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("编辑角色请求")
public class EditRoleRequest extends AddRoleRequest {
}
