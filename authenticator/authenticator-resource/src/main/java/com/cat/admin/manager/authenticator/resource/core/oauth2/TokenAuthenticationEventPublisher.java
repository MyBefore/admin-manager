package com.cat.admin.manager.authenticator.resource.core.oauth2;

import com.cat.admin.manager.authenticator.client.core.util.Security;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;

public class TokenAuthenticationEventPublisher implements AuthenticationEventPublisher {
    @Override
    public void publishAuthenticationSuccess(Authentication authentication) {
        // 认证成功设置用户
        Security.setUser((User) authentication.getPrincipal());
    }

    @Override
    public void publishAuthenticationFailure(AuthenticationException exception, Authentication authentication) {
        // 认证失败暂时不做任何操作
    }
}
