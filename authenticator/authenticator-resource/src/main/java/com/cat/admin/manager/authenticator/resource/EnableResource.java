package com.cat.admin.manager.authenticator.resource;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Import(ResourceAutoConfiguration.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnableResource {
}
