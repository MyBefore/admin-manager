package com.cat.admin.manager.authenticator.resource.core.oauth2.resource;

import com.cat.admin.manager.authenticator.resource.core.oauth2.TokenAuthenticationEventPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
public class Oauth2ResourceServerConfigurer extends ResourceServerConfigurerAdapter {

    /**
     * 资源服务的id
     */
    @Value("${authority.resource.id}")
    private String resourceId;

    @Autowired
    @Qualifier("oauth2Redis")
    private RedisConnectionFactory redisConnectionFactory;

    @Bean
    public TokenStore tokenStore() {
        return new Oauth2RedisTokenStore(redisConnectionFactory);
    }

    @Bean
    public AuthenticationEventPublisher authenticationEventPublisher() {
        return new TokenAuthenticationEventPublisher();
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        // 设置资源服务的id
        resources.resourceId(resourceId);
        // 设置资源服务器的token存储
        resources.tokenStore(tokenStore());
        // 设置认证事件发布器
        resources.eventPublisher(authenticationEventPublisher());
        super.configure(resources);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        super.configure(http);
    }
}
