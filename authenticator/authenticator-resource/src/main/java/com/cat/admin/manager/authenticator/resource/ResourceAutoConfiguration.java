package com.cat.admin.manager.authenticator.resource;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@Configuration
@ComponentScan
@EnableResourceServer
public class ResourceAutoConfiguration {
}
