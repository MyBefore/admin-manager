package com.cat.admin.manager.user.core.service.user;

import com.cat.admin.manager.user.shared.dto.user.AddUserRequest;
import com.cat.admin.manager.user.shared.dto.user.EditUserRequest;
import com.cat.admin.manager.user.shared.dto.user.ProtectedUserDTO;
import com.cat.admin.manager.user.shared.dto.user.UserDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.PagedResult;
import com.cat.commons.result.core.Result;

import java.util.List;

public interface UserService {


    /**
     * 添加用户
     * @param request
     * @return
     */
    GenericResult<UserDTO> add(AddUserRequest request);

    /**
     * 删除用户
     * @param id
     * @return
     */
    GenericResult<UserDTO> delete(String id);

    /**
     * 编辑用户
     * @param id
     * @param request
     * @return
     */
    GenericResult<UserDTO> edit(String id, EditUserRequest request);

    /**
     * 批量查询用户
     * @param ids
     * @return
     */
    GenericResult<List<UserDTO>> get(List<String> ids);

    /**
     * 过滤查询用户
     * @param username
     * @param password
     * @return
     */
    GenericResult<UserDTO> get(String username, String password);

    /**
     * 过滤查询用户
     * @param username
     * @return
     */
    GenericResult<ProtectedUserDTO> get(String username);

    /**
     * 验证用户名是否已存在
     * @param username
     * @return
     */
    Result exists(String username);

    /**
     * 分页查询用户
     * @param pageIndex
     * @param pageSize
     * @param keywords
     * @return
     */
    PagedResult<UserDTO> page(Integer pageIndex, Integer pageSize, String keywords);
}
