package com.cat.admin.manager.user.core.configuration;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.cat.admin.manager.user.data.mapper")
public class MapperConfig {
}
