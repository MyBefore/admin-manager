package com.cat.admin.manager.user.core.consts;

public class MessageConsts {

    public static final String DELETE_NOT_FOUND = "删除不存在的项";
    public static final String EDIT_NOT_FOUND = "编辑不存在的项";
    public static final String GET_NOT_FOUND = "查询不存在的项";
    public static final String INCORRECT_PASSWORD = "密码不正确";
    public static final String USERNAME_ALREADY_EXISTS = "用户名已存在";

}
