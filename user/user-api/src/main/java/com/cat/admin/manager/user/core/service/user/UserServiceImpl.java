package com.cat.admin.manager.user.core.service.user;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cat.admin.manager.tcc.client.core.manager.TccLocal;
import com.cat.admin.manager.tcc.client.core.manager.annotation.Join;
import com.cat.admin.manager.tcc.client.core.manager.annotation.Tcc;
import com.cat.admin.manager.user.core.consts.MessageConsts;
import com.cat.admin.manager.user.data.mapper.UserMapper;
import com.cat.admin.manager.user.domain.entity.User;
import com.cat.admin.manager.user.shared.dto.user.AddUserRequest;
import com.cat.admin.manager.user.shared.dto.user.EditUserRequest;
import com.cat.admin.manager.user.shared.dto.user.ProtectedUserDTO;
import com.cat.admin.manager.user.shared.dto.user.UserDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.PagedResult;
import com.cat.commons.result.core.Result;
import com.cat.commons.result.core.Results;
import com.cat.commons.security.core.PasswordUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private TccLocal tccLocal;

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE, rollbackFor = Throwable.class)
    @Tcc(
            cancelMethod = "com.cat.admin.manager.user.core.service.user.UserServiceImpl.cancelAdd",
            join = @Join
    )
    public GenericResult<UserDTO> add(AddUserRequest request) {

        if (existsUsername(request.getUsername())) {
            return Results.failedGeneric(MessageConsts.USERNAME_ALREADY_EXISTS, HttpStatus.OK);
        }

        User user = transform(request);
        userMapper.insert(user);

        // 设置取消参数
        tccLocal.setCancelParams(user.getId());

        return Results.successGeneric(transform(user));
    }

    public void cancelAdd(String userId) {
        if (StringUtils.isNotBlank(userId)) {
            userMapper.deleteById(userId);
        }
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE, rollbackFor = Throwable.class)
    public GenericResult<UserDTO> delete(String id) {
        User user = userMapper.selectById(id);
        if (user == null) {
            return Results.failedGeneric(MessageConsts.DELETE_NOT_FOUND, HttpStatus.OK);
        }
        userMapper.deleteById(id);
        return Results.successGeneric(transform(user));
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE, rollbackFor = Throwable.class)
    public GenericResult<UserDTO> edit(String id, EditUserRequest request) {
        User user = userMapper.selectById(id);
        if (user == null) {
            return Results.failedGeneric(MessageConsts.EDIT_NOT_FOUND, HttpStatus.OK);
        }
        if (!user.getPassword().equals(PasswordUtils.encode(request.getPreviousPassword()))) {
            return Results.failedGeneric(MessageConsts.INCORRECT_PASSWORD, HttpStatus.OK);
        }
        if (userMapper.selectCount(new QueryWrapper<User>().eq(User.COLUMN_USERNAME, request.getUsername()).notIn(User.COLUMN_ID, user.getId())) > 0) {
            return Results.failedGeneric(MessageConsts.USERNAME_ALREADY_EXISTS, HttpStatus.OK);
        }
        user = transform(request, user);
        userMapper.updateById(user);
        return Results.successGeneric(transform(user));
    }

    @Override
    public GenericResult<List<UserDTO>> get(List<String> ids) {
        List<User> users = userMapper.selectBatchIds(ids);
        return Results.successGeneric(transforms(users));
    }

    @Override
    public GenericResult<UserDTO> get(String username, String password) {
        List<User> users = userMapper.selectList(new QueryWrapper<User>().eq(User.COLUMN_USERNAME, username).eq(User.COLUMN_PASSWORD, PasswordUtils.encode(password)));
        if (users.isEmpty()) {
            return Results.failedGeneric(MessageConsts.GET_NOT_FOUND, HttpStatus.OK);
        }
        return Results.successGeneric(transform(users.get(0)));
    }

    @Override
    public GenericResult<ProtectedUserDTO> get(String username) {
        List<User> users = userMapper.selectList(new QueryWrapper<User>().eq(User.COLUMN_USERNAME, username));
        if (users.isEmpty()) {
            return Results.failedGeneric(MessageConsts.GET_NOT_FOUND, HttpStatus.OK);
        }
        return Results.successGeneric(transformP(users.get(0)));
    }

    @Override
    public Result exists(String username) {
        return existsUsername(username) ? Results.success() : Results.failed(MessageConsts.GET_NOT_FOUND, HttpStatus.OK);
    }

    @Override
    public PagedResult<UserDTO> page(Integer pageIndex, Integer pageSize, String keywords) {
        IPage<User> page = userMapper.selectPage(new Page<>(pageIndex, pageSize), new QueryWrapper<User>().like(User.COLUMN_USERNAME, "%" + keywords + "%"));
        return transform(page);
    }

    private User transform(AddUserRequest request) {
        User user = new User();
        BeanUtils.copyProperties(request, user);
        user.setPassword(PasswordUtils.encode(request.getPassword()));
        return user;
    }

    private User transform(EditUserRequest request, User user) {
        BeanUtils.copyProperties(request, user);
        user.setPassword(PasswordUtils.encode(request.getPassword()));
        return user;
    }

    private UserDTO transform(User user) {
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(user, userDTO);
        return userDTO;
    }

    private ProtectedUserDTO transformP(User user) {
        ProtectedUserDTO userDTO = new ProtectedUserDTO();
        BeanUtils.copyProperties(user, userDTO);
        return userDTO;
    }

    private List<UserDTO> transforms(List<User> users) {
        return users.stream().map(u -> transform(u)).collect(Collectors.toList());
    }

    private PagedResult<UserDTO> transform(IPage<User> page) {
        return Results.successPaged(transforms(page.getRecords()), page.getCurrent(), page.getSize(), page.getTotal());
    }

    private boolean existsUsername(String username) {
        Integer count = userMapper.selectCount(new QueryWrapper<User>().eq(User.COLUMN_USERNAME, username));
        return count > 0 ? true : false;
    }
}
