package com.cat.admin.manager.user.api.controller;

import com.cat.admin.manager.user.core.service.user.UserService;
import com.cat.admin.manager.user.shared.dto.user.AddUserRequest;
import com.cat.admin.manager.user.shared.dto.user.EditUserRequest;
import com.cat.admin.manager.user.shared.dto.user.ProtectedUserDTO;
import com.cat.admin.manager.user.shared.dto.user.UserDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.PagedResult;
import com.cat.commons.result.core.Result;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Api("用户接口")
@Validated
@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation("添加用户")
    @PostMapping
    public GenericResult<UserDTO> add(
            @Valid @RequestBody AddUserRequest request
    ) {
        return userService.add(request);
    }

    @ApiOperation("删除用户")
    @DeleteMapping("{id}")
    public GenericResult<UserDTO> delete(@ApiParam(value = "用户id", required = true) @NotEmpty @PathVariable("id") String id) {
        return userService.delete(id);
    }

    @ApiOperation("编辑用户")
    @PutMapping("{id}")
    public GenericResult<UserDTO> edit(
            @ApiParam(value = "用户id", required = true) @NotEmpty @PathVariable("id") String id,
            @Valid @RequestBody EditUserRequest request
    ) {
        return userService.edit(id, request);
    }

    @ApiOperation("批量查询用户")
    @GetMapping(params = {"ids"})
    public GenericResult<List<UserDTO>> get(@ApiParam(value = "用户id集合", required = true) @NotNull @RequestParam("ids") List<String> ids) {
        return userService.get(ids);
    }

    @ApiOperation("非安全的过滤查询用户，包括密码")
    @GetMapping(value = "protected", params = {"username"})
    public GenericResult<ProtectedUserDTO> get(
            @ApiParam(value = "用户名称", required = true) @NotEmpty @RequestParam("username") String username
    ) {
        return userService.get(username);
    }

    @ApiOperation("过滤查询用户")
    @GetMapping(params = {"username", "password"})
    public GenericResult<UserDTO> get(
            @ApiParam(value = "用户名称", required = true) @NotEmpty @RequestParam("username") String username,
            @ApiParam(value = "用户密码", required = true) @NotEmpty @RequestParam("password") String password
    ) {
        return userService.get(username, password);
    }

    @ApiOperation("验证用户名是否已存在")
    @GetMapping(params = {"username"})
    public Result exists(
            @ApiParam(value = "用户名称", required = true) @NotEmpty @RequestParam("username") String username
    ) {
        return userService.exists(username);
    }

    @ApiOperation("分页查询用户")
    @GetMapping(params = {"pageIndex", "pageSize"})
    public PagedResult<UserDTO> page(
            @ApiParam(value = "页码", required = true) @NotNull @RequestParam("pageIndex") Integer pageIndex,
            @ApiParam(value = "条数", required = true) @NotNull @RequestParam("pageSize") Integer pageSize,
            @ApiParam(value = "关键字") @RequestParam(value = "keywords", required = false, defaultValue = "") String keywords
    ) {
        return userService.page(pageIndex, pageSize, keywords);
    }

}
