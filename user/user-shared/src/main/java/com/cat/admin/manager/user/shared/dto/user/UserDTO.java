package com.cat.admin.manager.user.shared.dto.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@ApiModel("用户信息")
@Data
public class UserDTO {

    @ApiModelProperty(value = "id", required = true)
    @NotEmpty
    private String id;

    @ApiModelProperty(value = "名称", required = true)
    @NotEmpty
    private String username;

}
