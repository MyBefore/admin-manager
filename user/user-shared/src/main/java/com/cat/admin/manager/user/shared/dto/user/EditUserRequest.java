package com.cat.admin.manager.user.shared.dto.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@ApiModel("编辑用户")
@Data
public class EditUserRequest {

    @ApiModelProperty(value = "名称", required = true)
    @NotEmpty
    private String username;

    @ApiModelProperty(value = "密码", required = true)
    @NotEmpty
    private String password;

    @ApiModelProperty(value = "以前的密码", required = true)
    @NotEmpty
    private String previousPassword;
}
