package com.cat.admin.manager.user.shared.dto.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class ProtectedUserDTO extends UserDTO {

    @ApiModelProperty(value = "密码", required = true)
    @NotEmpty
    private String password;

}
