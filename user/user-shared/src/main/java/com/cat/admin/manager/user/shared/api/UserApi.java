package com.cat.admin.manager.user.shared.api;

import com.cat.admin.manager.user.shared.dto.user.AddUserRequest;
import com.cat.admin.manager.user.shared.dto.user.EditUserRequest;
import com.cat.admin.manager.user.shared.dto.user.ProtectedUserDTO;
import com.cat.admin.manager.user.shared.dto.user.UserDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.PagedResult;
import com.cat.commons.result.core.Result;
import com.sun.istack.internal.NotNull;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Api("用户接口")
@Validated
@RequestMapping("users")
public interface UserApi {

    @ApiOperation("添加用户")
    @PostMapping
    GenericResult<UserDTO> add(
            @Valid @RequestBody AddUserRequest request
    );

    @ApiOperation("删除用户")
    @DeleteMapping("{id}")
    GenericResult<UserDTO> delete(@ApiParam(value = "用户id", required = true) @NotEmpty @PathVariable("id") String id);

    @ApiOperation("编辑用户")
    @PutMapping("{id}")
    GenericResult<UserDTO> edit(
            @ApiParam(value = "用户id", required = true) @NotEmpty @PathVariable("id") String id,
            @Valid @RequestBody EditUserRequest request
    );

    @ApiOperation("批量查询用户")
    @GetMapping(params = {"ids"})
    GenericResult<List<UserDTO>> get(@ApiParam(value = "用户id集合", required = true) @NotNull @RequestParam("ids") List<String> ids);

    @ApiOperation("过滤查询用户")
    @GetMapping(value = "protected", params = {"username"})
    GenericResult<ProtectedUserDTO> get(
            @ApiParam(value = "用户名称", required = true) @NotEmpty @RequestParam("username") String username
    );

    @ApiOperation("过滤查询用户")
    @GetMapping(params = {"username", "password"})
    GenericResult<UserDTO> get(
            @ApiParam(value = "用户名称", required = true) @NotEmpty @RequestParam("username") String username,
            @ApiParam(value = "用户密码", required = true) @NotEmpty @RequestParam("password") String password
    );

    @ApiOperation("验证用户名是否已存在")
    @GetMapping(params = {"username"})
    Result exists(
            @ApiParam(value = "用户名称", required = true) @NotEmpty @RequestParam("username") String username
    );

    @ApiOperation("分页查询用户")
    @GetMapping(params = {"pageIndex", "pageSize"})
    PagedResult<UserDTO> page(
            @ApiParam(value = "页码", required = true) @NotNull @RequestParam("pageIndex") Integer pageIndex,
            @ApiParam(value = "条数", required = true) @NotNull @RequestParam("pageSize") Integer pageSize,
            @ApiParam(value = "关键字") @NotEmpty @RequestParam(value = "keywords", required = false, defaultValue = "") String keywords
    );
}
