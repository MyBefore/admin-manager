package com.cat.admin.manager.file.worker.shared.dto.file;

import com.sun.istack.internal.NotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.math.BigInteger;

@Data
@ApiModel("文件")
public class UploadFileDTO {

    @ApiModelProperty(value = "文件Id", required = true)
    @NotEmpty
    private String id;

    @ApiModelProperty(value = "文件名称", required = true)
    @NotEmpty
    private String name;

    @ApiModelProperty(value = "文件大小", required = true)
    @NotNull
    private BigInteger size;

    @ApiModelProperty(value = "上传文件的sha1值", required = true)
    @NotEmpty
    private String fileSha1Hash;
}
