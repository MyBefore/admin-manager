package com.cat.admin.manager.file.manager.shared.dto.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@ApiModel("表单")
public abstract class FormDTO {

    @ApiModelProperty(value = "访问的token", required = true)
    @NotEmpty
    private String accessToken;

    @ApiModelProperty(value = "文件的url", required = true)
    @NotEmpty
    private String url;

    @ApiModelProperty(value = "链接有效时长失效", required = true)
    @NotNull
    private long effective;

    @ApiModelProperty(value = "链接的请求方式", required = true)
    @NotEmpty
    private String method;
}
