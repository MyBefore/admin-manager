package com.cat.admin.manager.file.manager.shared.api;

import com.cat.admin.manager.file.manager.shared.dto.file.PhysicsFileDTO;
import com.cat.admin.manager.file.manager.shared.dto.form.DownloadFormDTO;
import com.cat.admin.manager.file.manager.shared.dto.form.UploadFormDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Api("文件表单接口")
@RestController
@RequestMapping("forms")
@Validated
public interface FormApi {

    @ApiOperation("生成文件上传表单")
    @GetMapping("{count}")
    GenericResult<List<UploadFormDTO>> generateUploadForms(@ApiParam(value = "生成的表单个数", required = true) @Min(1) @Max(10) @NotNull @PathVariable("count") Integer count);

    @ApiOperation("生成文件下载表单")
    @GetMapping(params = {"ids"})
    GenericResult<List<DownloadFormDTO>> generateDownloadForms(@ApiParam(value = "文件的id集合", required = true) @NotNull @RequestParam("ids") List<String> ids);

    @ApiOperation("检查表单的有效性")
    @GetMapping(value = "checkForm",params = {"worker", "accessToken", "method"})
    GenericResult<PhysicsFileDTO> checkForm(
            @ApiParam(value = "文件的工作机", required = true) @NotEmpty @RequestParam("worker") String worker,
            @ApiParam(value = "表单的accessToken", required = true) @NotEmpty @RequestParam("accessToken") String accessToken,
            @ApiParam(value = "表单的请求方式", required = true) @NotEmpty @RequestParam("method") String method
    );

    @ApiOperation("移除表单")
    @GetMapping(value = "removeForm",params = {"worker", "accessToken", "method"})
    Result removeForm(
            @ApiParam(value = "文件的工作机", required = true) @NotEmpty @RequestParam("worker") String worker,
            @ApiParam(value = "表单的accessToken", required = true) @NotEmpty @RequestParam("accessToken") String accessToken,
            @ApiParam(value = "表单的请求方式", required = true) @NotEmpty @RequestParam("method") String method
    );
}
