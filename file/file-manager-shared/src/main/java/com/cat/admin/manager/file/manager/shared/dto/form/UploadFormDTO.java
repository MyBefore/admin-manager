package com.cat.admin.manager.file.manager.shared.dto.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 文件上传的form
 */
@Data
@ApiModel("上传表单")
public class UploadFormDTO extends FormDTO {

    @ApiModelProperty(value = "文件上传的参数名称", required = true)
    @NotEmpty
    private String parameterName = "file";
}
