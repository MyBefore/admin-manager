package com.cat.admin.manager.file.manager.shared.dto.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;

/**
 * 文件下载的form
 */
@Data
@ApiModel("下载表单")
public class DownloadFormDTO extends FormDTO {

    @ApiModelProperty(value = "文件的id", required = true)
    @NotEmpty
    private String id;

    @ApiModelProperty(value = "文件的大小", required = true)
    @NotNull
    private BigInteger fileSize;
}
