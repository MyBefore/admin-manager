package com.cat.admin.manager.file.manager.shared.dto.file;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@ApiModel("物理文件")
public class PhysicsFileDTO {

    public PhysicsFileDTO() {
    }

    public PhysicsFileDTO(@NotEmpty String worker, @NotEmpty String physicsPath, @NotEmpty String sourceWorkerAddress) {
        this.worker = worker;
        this.physicsPath = physicsPath;
        this.sourceWorkerAddress = sourceWorkerAddress;
    }

    public PhysicsFileDTO(@NotEmpty String id, @NotEmpty String worker, @NotEmpty String contentType, @NotEmpty String physicsPath, @NotEmpty String sourceWorkerAddress, String sourceWorkerDownloadPrefix, String fileSha1Hash) {
        this.id = id;
        this.worker = worker;
        this.contentType = contentType;
        this.physicsPath = physicsPath;
        this.sourceWorkerAddress = sourceWorkerAddress;
        this.sourceWorkerDownloadPrefix = sourceWorkerDownloadPrefix;
        this.fileSha1Hash = fileSha1Hash;
    }

    @ApiModelProperty(value = "文件Id", required = true)
    @NotEmpty
    private String id;

    @ApiModelProperty(value = "工作机的服务名称", required = true)
    @NotEmpty
    private String worker;

    @ApiModelProperty(value = "文件的内容类型", required = true)
    @NotEmpty
    private String contentType;

    @ApiModelProperty(value = "文件的物理路径", required = true)
    @NotEmpty
    private String physicsPath;

    @ApiModelProperty(value = "上传的源主机地址", required = true)
    @NotEmpty
    private String sourceWorkerAddress;

    @ApiModelProperty(value = "上传的源主机下载地址前缀", required = true)
    private String sourceWorkerDownloadPrefix;

    @ApiModelProperty(value = "是否已被使用", required = true)
    @NotEmpty
    private boolean used = false;

    @ApiModelProperty(value = "上传文件的sha1值", required = true)
    @NotEmpty
    private String fileSha1Hash;
}
