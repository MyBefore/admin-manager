package com.cat.admin.manager.file.manager.shared.dto.file;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;

@Data
@ApiModel("添加文件的参数")
public class AddFileRequest {

    @ApiModelProperty(value = "文件名称", required = true)
    @NotEmpty
    private String name;

    @ApiModelProperty(value = "文件大小", required = true)
    @NotNull
    private BigInteger size;

    @ApiModelProperty(value = "工作机的服务名称", required = true)
    @NotEmpty
    private String worker;

    @ApiModelProperty(value = "文件的内容类型", required = true)
    @NotEmpty
    private String contentType;

    @ApiModelProperty(value = "文件的物理路径", required = true)
    @NotEmpty
    private String physicsPath;

    @ApiModelProperty(value = "上传的源主机地址", required = true)
    @NotEmpty
    private String sourceWorkerAddress;

    @ApiModelProperty(value = "上传的源主机下载地址前缀", required = true)
    private String sourceWorkerDownloadPrefix;

    @ApiModelProperty(value = "上传文件的sha1值", required = true)
    @NotEmpty
    private String fileSha1Hash;
}
