package com.cat.admin.manager.file.manager.core.service.form;

import com.cat.admin.manager.file.manager.shared.dto.file.PhysicsFileDTO;
import com.cat.admin.manager.file.manager.shared.dto.form.DownloadFormDTO;
import com.cat.admin.manager.file.manager.shared.dto.form.UploadFormDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.Result;

import java.net.MalformedURLException;
import java.util.List;

/**
 * 表单服务
 */
public interface FormService {

    /**
     * 生成多个上传表单
     * @param count
     * @return
     */
    GenericResult<List<UploadFormDTO>> generateUploadForms(Integer count) throws MalformedURLException;

    /**
     * 生成多个下载表单
     * @param ids
     * @return
     */
    GenericResult<List<DownloadFormDTO>> generateDownloadForms(List<String> ids) throws MalformedURLException;

    /**
     * 校验表单的有效性
     * @param worker
     * @param accessToken
     * @param method
     * @return
     */
    GenericResult<PhysicsFileDTO> checkForm(String worker, String accessToken, String method);

    /**
     * 移除表单
     * @param worker
     * @param accessToken
     * @param method
     * @return
     */
    Result removeForm(String worker, String accessToken, String method);
}
