package com.cat.admin.manager.file.manager.core.service.file;


import com.cat.admin.manager.file.manager.shared.dto.file.AddFileRequest;
import com.cat.admin.manager.file.manager.shared.dto.file.FileDTO;
import com.cat.commons.result.core.GenericResult;

import java.util.List;

/**
 * 文件的service操作
 */
public interface FileService {

    /**
     * 添加文件
     * @param request
     * @return
     */
    GenericResult<FileDTO> add(AddFileRequest request);

    /**
     * 删除文件
     * @param id
     * @return
     */
    GenericResult<FileDTO> delete(String id);

    /**
     * 查询多个文件
     * @param ids
     * @return
     */
    GenericResult<List<FileDTO>> all(List<String> ids);
}
