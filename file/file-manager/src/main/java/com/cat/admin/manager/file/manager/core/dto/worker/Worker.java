package com.cat.admin.manager.file.manager.core.dto.worker;

import lombok.Data;

@Data
public class Worker {

    /**
     * 访问的form失效时间
     */
    private long effective;

    /**
     * 工作机的服务名称
     */
    private String name;

    /**
     * 工作机的访问名称
     */
    private String accessUrl;

    /**
     * 文件的物理存储目录
     */
    private String filesPhysicsDir;

    /**
     * 下载的路径前缀
     */
    private String downloadPathPrefix;

    /**
     * 上传的路径前缀
     */
    private String uploadPathPrefix;

}
