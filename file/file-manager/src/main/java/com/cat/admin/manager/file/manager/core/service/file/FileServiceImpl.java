package com.cat.admin.manager.file.manager.core.service.file;

import com.cat.admin.manager.file.manager.core.consts.MessageConsts;
import com.cat.admin.manager.file.manager.data.mapper.FileMapper;
import com.cat.admin.manager.file.manager.domain.entity.File;
import com.cat.admin.manager.file.manager.shared.dto.file.AddFileRequest;
import com.cat.admin.manager.file.manager.shared.dto.file.FileDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.Results;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FileServiceImpl implements FileService {

    @Autowired
    private FileMapper fileMapper;

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE, rollbackFor = Throwable.class)
    public GenericResult<FileDTO> add(AddFileRequest request) {

        File file = transform(request);

        int insert = fileMapper.insert(file);

        if (insert <= 0) {
            throw new RuntimeException("Database exception");
        }

        return Results.successGeneric(transform(file));
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE, rollbackFor = Throwable.class)
    public GenericResult<FileDTO> delete(String id) {

        File file = fileMapper.selectById(id);
        if (file == null) {
            return Results.failedGeneric(MessageConsts.DELETE_NOT_FOUND, HttpStatus.OK);
        }

        int delete = fileMapper.deleteById(id);

        if (delete <= 0) {
            throw new RuntimeException("Database exception");
        }

        return Results.successGeneric(transform(file));
    }

    @Override
    public GenericResult<List<FileDTO>> all(List<String> ids) {

        // 批量查询多个文件
        List<File> files = fileMapper.selectBatchIds(ids);

        return Results.successGeneric(files.stream().map(f -> transform(f)).collect(Collectors.toList()));
    }

    private File transform(AddFileRequest request) {
        File file = new File();
        BeanUtils.copyProperties(request, file);
        return file;
    }

    private FileDTO transform(File file) {
        FileDTO dto = new FileDTO();
        BeanUtils.copyProperties(file, dto);
        return dto;
    }
}
