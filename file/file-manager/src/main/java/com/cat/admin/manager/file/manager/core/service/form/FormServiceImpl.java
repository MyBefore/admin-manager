package com.cat.admin.manager.file.manager.core.service.form;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cat.admin.manager.file.manager.core.consts.MessageConsts;
import com.cat.admin.manager.file.manager.core.dto.form.CurrencyFormDTO;
import com.cat.admin.manager.file.manager.core.dto.worker.Worker;
import com.cat.admin.manager.file.manager.data.mapper.FileMapper;
import com.cat.admin.manager.file.manager.domain.entity.File;
import com.cat.admin.manager.file.manager.shared.dto.file.PhysicsFileDTO;
import com.cat.admin.manager.file.manager.shared.dto.form.DownloadFormDTO;
import com.cat.admin.manager.file.manager.shared.dto.form.FormDTO;
import com.cat.admin.manager.file.manager.shared.dto.form.UploadFormDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.Result;
import com.cat.commons.result.core.Results;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class FormServiceImpl implements FormService {

    /**
     * 表单的失效时间元数据名称
     */
    @Value("${worker.form.effective-meta-name}")
    private String workerEffectiveMetaName;

    /**
     * 上传表单url的前缀元数据名称
     */
    @Value("${worker.form.upload-url-prefix-meta-name}")
    private String workerUploadUrlPrefixMetaName;

    /**
     * 下载表单url的前缀元数据名称
     */
    @Value("${worker.form.download-url-prefix-meta-name}")
    private String workerDownloadUrlPrefixMetaName;

    /**
     * 工作机名称前缀
     */
    @Value("${worker.server-name-prefix}")
    private String workerServerNamePrefix;

    /**
     * 工作机负载的redis键
     */
    @Value("${worker.balanced-redis-key}")
    private String workerBalancedRedisKey;

    /**
     * 工作机访问的url元数据名称
     */
    @Value("${worker.form.access-url-meta-name}")
    private String workerAccessUrlMetaName;

    /**
     * 工作机文件物理存储目录的元数据名称
     */
    @Value("${worker.files-physics-dir-meta-name}")
    private String workerFilesPhysicsDirMetaName;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private FileMapper fileMapper;

    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private LoadBalancerClient loadBalancerClient;

    @Override
    public GenericResult<List<UploadFormDTO>> generateUploadForms(Integer count) throws MalformedURLException {

        List<UploadFormDTO> list = new ArrayList<>(count);

        for (int i = 0; i < count; i++) {
            list.add(generateUpdateForm());
        }

        return Results.successGeneric(list);
    }

    @Override
    public GenericResult<List<DownloadFormDTO>> generateDownloadForms(List<String> ids) throws MalformedURLException {

        if (ids.isEmpty()) {
            return Results.successGeneric(Collections.EMPTY_LIST);
        }

        List<DownloadFormDTO> list = new ArrayList<>(ids.size());

        List<File> files = fileMapper.selectBatchIds(ids);

        for (File file : files) {
            list.add(generateDownloadForm(file));
        }

        return Results.successGeneric(list);
    }

    @Override
    public GenericResult<PhysicsFileDTO> checkForm(String worker, String accessToken, String method) {

        CurrencyFormDTO currencyFormDTO = new CurrencyFormDTO();
        currencyFormDTO.setMethod(method);
        currencyFormDTO.setAccessToken(accessToken);

        PhysicsFileDTO physicsFile = getFormPhysicsFile(stringRedisTemplate.opsForValue(), currencyFormDTO, true);
        if (physicsFile == null || physicsFile.isUsed() || !physicsFile.getWorker().equals(worker)) {
            return Results.failedGeneric(MessageConsts.INVALID_FORM, HttpStatus.OK);
        }

        return Results.successGeneric(physicsFile);
    }

    @Override
    public Result removeForm(String worker, String accessToken, String method) {

        CurrencyFormDTO currencyFormDTO = new CurrencyFormDTO();
        currencyFormDTO.setMethod(method);
        currencyFormDTO.setAccessToken(accessToken);

        return removFormWorker(currencyFormDTO) ? Results.success() : Results.failed(MessageConsts.DELETE_NOT_FOUND, HttpStatus.OK);
    }

    /**
     * 生成上传的form
     * @return
     */
    private UploadFormDTO generateUpdateForm() throws MalformedURLException {
        UploadFormDTO form = new UploadFormDTO();
        form.setMethod(HttpMethod.POST.name());
        ValueOperations<String, String> vo = stringRedisTemplate.opsForValue();
        Worker worker = balancedWorker();
        while (true) {
            form.setEffective(worker.getEffective());
            // 生成一个不存在的url
            do {
                form.setAccessToken(UUID.randomUUID().toString());
                form.setUrl(worker.getAccessUrl() + worker.getUploadPathPrefix() + form.getAccessToken());
            } while (getFormPhysicsFile(vo, form, false) != null);
            setFormWorker(vo, form, new PhysicsFileDTO(worker.getName(), form.getAccessToken(), worker.getAccessUrl()));
            // 判断物理地址是否已被使用
            Integer count = fileMapper.selectCount(new QueryWrapper<File>().eq(File.COLUMN_PHYSICS_PATH, form.getAccessToken()));
            if (count == null || count <= 0) {
                break;
            }
            removFormWorker(form);
        }
        return form;
    }

    /**
     * 生成文件下载的表单
     * @return
     */
    private DownloadFormDTO generateDownloadForm(File file) throws MalformedURLException {
        DownloadFormDTO form = new DownloadFormDTO();
        form.setMethod(HttpMethod.GET.name());
        Worker worker = getWorkerByName(file.getWorker());
        form.setEffective(worker.getEffective());
        form.setId(file.getId());
        form.setFileSize(file.getSize());
        ValueOperations<String, String> vo = stringRedisTemplate.opsForValue();
        // 生成一个不存在的url
        do {
            form.setAccessToken(UUID.randomUUID().toString());
            form.setUrl(worker.getAccessUrl() + worker.getDownloadPathPrefix() + form.getAccessToken() + "/" + file.getName());
        } while (getFormPhysicsFile(vo, form, false) != null);
        setFormWorker(vo, form, new PhysicsFileDTO(file.getId(), worker.getName(), file.getContentType(), file.getPhysicsPath(), file.getSourceWorkerAddress(), file.getSourceWorkerDownloadPrefix(), file.getFileSha1Hash()));
        return form;
    }

    /**
     * 通过form从redis中取出worker
     * @param vo
     * @param form
     * @return
     */
    private PhysicsFileDTO getFormPhysicsFile(ValueOperations<String, String> vo, FormDTO form, boolean use) {
        if (use) {
            String key = form.getAccessToken() + "_" + form.getMethod();
            PhysicsFileDTO physicsFileDTO = JSONObject.parseObject(vo.get(key), PhysicsFileDTO.class);
            if (physicsFileDTO == null || physicsFileDTO.isUsed()) {
                return physicsFileDTO;
            }
            physicsFileDTO.setUsed(true);
            physicsFileDTO = JSONObject.parseObject(vo.getAndSet(key, JSONObject.toJSONString(physicsFileDTO)), PhysicsFileDTO.class);
            return physicsFileDTO;
        } else {
            return JSONObject.parseObject(vo.get(form.getAccessToken() + "_" + form.getMethod()), PhysicsFileDTO.class);
        }
    }

    /**
     * 通过form清除redis中的worker
     * @param form
     */
    private boolean removFormWorker(FormDTO form) {
        return stringRedisTemplate.delete(form.getAccessToken() + "_" + form.getMethod());
    }

    /**
     * 通过form设置worker到redis
     * @param vo
     * @param form
     * @param physicsFile
     */
    private void setFormWorker(ValueOperations<String, String> vo, FormDTO form, PhysicsFileDTO physicsFile) {
        vo.set(form.getAccessToken() + "_" + form.getMethod(), JSONObject.toJSONString(physicsFile), form.getEffective(), TimeUnit.MILLISECONDS);
    }

    /**
     * 获取负载的工作机
     * @return
     */
    private String balancedWorkerName() {

        // 获取worker的service
        List<String> services = discoveryClient.getServices();

        if (services == null || services.isEmpty()) {
            throw new RuntimeException("services is empty");
        }

        List<String> workers = services.stream().filter(s -> s.startsWith(workerServerNamePrefix)).collect(Collectors.toList());
        if (workers.isEmpty()) {
            throw new RuntimeException("workers is empty");
        }

        // 从redis中取出负载的次数
        HashOperations<String, String, String> ho = stringRedisTemplate.opsForHash();
        List<String> counts = ho.multiGet(workerBalancedRedisKey, workers);

        // 将redis中取出的次数合并成map
        Map<String, String> maps = new HashMap<>();
        for (int i = 0; i < workers.size(); i++) {
            String count = counts.get(i);
            maps.put(workers.get(i), count == null ? "0" : count);
        }
        // 找出最小的
        Optional<Map.Entry<String, String>> min = maps.entrySet().stream().reduce((p, l) -> new BigInteger(p.getValue()).compareTo(new BigInteger(l.getValue())) <= 0 ? p : l);
        String balancedWorker = min.get().getKey();

        // 将负载设置到redis中
        ho.putIfAbsent(workerBalancedRedisKey, balancedWorker, "0");
        ho.increment(workerBalancedRedisKey, balancedWorker, 1);

        return balancedWorker;
    }

    /**
     * 获取负载的实例
     * @param serverId
     * @return
     */
    private ServiceInstance getBalancedInstance(String serverId) {
        ServiceInstance choose = loadBalancerClient.choose(serverId);
        if (choose == null) {
            throw new RuntimeException("instances is empty of worker");
        }
        return choose;
    }

    /**
     * 获取工作机访问的url
     * @param instance
     * @return
     * @throws MalformedURLException
     */
    private String getWorkerAccessUrl(ServiceInstance instance) throws MalformedURLException {
        String accessUrl = instance.getMetadata().get(workerAccessUrlMetaName);
        if (StringUtils.isNotBlank(accessUrl)) {
            return accessUrl;
        }
        return instance.getUri().toURL().toString();
    }

    /**
     * 获取负载的worker
     * @return
     */
    private Worker balancedWorker() throws MalformedURLException {
        return getWorkerByName(balancedWorkerName());
    }

    /**
     * 获取文件物理存储位置
     * @return
     */
    private String getWorkerFilesPhysicsDir(ServiceInstance instance) {
        String dir = instance.getMetadata().get(workerFilesPhysicsDirMetaName);
        if (StringUtils.isBlank(dir)) {
            throw new RuntimeException("Physics file dir is blank of worker");
        }
        return dir;
    }

    /**
     * 获取有效的访问时间
     * @param instance
     * @return
     */
    private long getWorkerEffective(ServiceInstance instance) {
        String effective = instance.getMetadata().get(workerEffectiveMetaName);
        if (StringUtils.isBlank(effective)) {
            throw new RuntimeException("Effective access time is blank of worker");
        }
        return Long.valueOf(effective);
    }

    /**
     * 获取下载前缀
     * @param instance
     * @return
     */
    private String getWorkerDownloadUrlPrefix(ServiceInstance instance) {
        String prefix = instance.getMetadata().get(workerDownloadUrlPrefixMetaName);
        if (StringUtils.isBlank(prefix)) {
            prefix = "";
        }
        return prefix;
    }

    /**
     * 获取上传前缀
     * @param instance
     * @return
     */
    private String getWorkerUploadUrlPrefix(ServiceInstance instance) {
        String prefix = instance.getMetadata().get(workerUploadUrlPrefixMetaName);
        if (StringUtils.isBlank(prefix)) {
            prefix = "";
        }
        return prefix;
    }

    /**
     * 通过名称获取worker机
     * @param name
     * @return
     * @throws MalformedURLException
     */
    private Worker getWorkerByName(String name) throws MalformedURLException {
        Worker worker = new Worker();
        // 设置服务名称
        worker.setName(name);
        // 获取工作机的负载实例
        ServiceInstance balancedInstance = getBalancedInstance(worker.getName());
        // 设置访问的url
        worker.setAccessUrl(getWorkerAccessUrl(balancedInstance));
        // 设置工作机的物理目录
        worker.setFilesPhysicsDir(getWorkerFilesPhysicsDir(balancedInstance));
        // 设置有效的访问时间
        worker.setEffective(getWorkerEffective(balancedInstance));
        // 设置访问路径的下载前缀
        worker.setDownloadPathPrefix(getWorkerDownloadUrlPrefix(balancedInstance));
        // 设置访问路径的上传前缀
        worker.setUploadPathPrefix(getWorkerUploadUrlPrefix(balancedInstance));
        return worker;
    }
}
