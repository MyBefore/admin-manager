package com.cat.admin.manager.file.manager.api.controller;

import com.cat.admin.manager.file.manager.core.service.form.FormService;
import com.cat.admin.manager.file.manager.shared.dto.file.PhysicsFileDTO;
import com.cat.admin.manager.file.manager.shared.dto.form.DownloadFormDTO;
import com.cat.admin.manager.file.manager.shared.dto.form.UploadFormDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.net.MalformedURLException;
import java.util.List;

@Api("文件表单接口")
@RestController
@RequestMapping("forms")
@Validated
public class FormController {

    @Autowired
    private FormService formService;

    @ApiOperation("生成文件上传表单")
    @GetMapping("{count}")
    public GenericResult<List<UploadFormDTO>> generateUploadForms(@ApiParam(value = "生成的表单个数", required = true) @Min(1) @Max(10) @NotNull @PathVariable("count") Integer count) throws MalformedURLException {
        return formService.generateUploadForms(count);
    }

    @ApiOperation("生成文件下载表单")
    @GetMapping(params = {"ids"})
    public GenericResult<List<DownloadFormDTO>> generateDownloadForms(@ApiParam(value = "文件的id集合", required = true) @NotNull @RequestParam("ids") List<String> ids) throws MalformedURLException {
        return formService.generateDownloadForms(ids);
    }

    @ApiOperation("检查表单的有效性")
    @GetMapping(value = "checkForm",params = {"worker", "accessToken", "method"})
    public GenericResult<PhysicsFileDTO> checkForm(
            @ApiParam(value = "文件的工作机", required = true) @NotEmpty @RequestParam("worker") String worker,
            @ApiParam(value = "表单的accessToken", required = true) @NotEmpty @RequestParam("accessToken") String accessToken,
            @ApiParam(value = "表单的请求方式", required = true) @NotEmpty @RequestParam("method") String method
    ) {
        return formService.checkForm(worker, accessToken, method);
    }

    @ApiOperation("移除表单")
    @GetMapping(value = "removeForm",params = {"worker", "accessToken", "method"})
    public Result removeForm(
            @ApiParam(value = "文件的工作机", required = true) @NotEmpty @RequestParam("worker") String worker,
            @ApiParam(value = "表单的accessToken", required = true) @NotEmpty @RequestParam("accessToken") String accessToken,
            @ApiParam(value = "表单的请求方式", required = true) @NotEmpty @RequestParam("method") String method
    ) {
        return formService.removeForm(worker, accessToken, method);
    }

}
