package com.cat.admin.manager.file.manager.core.dto.form;

import com.cat.admin.manager.file.manager.shared.dto.form.FormDTO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("公用表单")
public class CurrencyFormDTO extends FormDTO {
}
