package com.cat.admin.manager.file.manager.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigInteger;

@TableName("file")
@Data
public class File {

    public static String COLUMN_PHYSICS_PATH = "physics_path";

    @TableId(value = "id", type = IdType.UUID)
    private String id;

    @TableField("name")
    private String name;

    @TableField("size")
    private BigInteger size;

    @TableField("worker")
    private String worker;

    @TableField("content_type")
    private String contentType;

    @TableField("physics_path")
    private String physicsPath;

    @TableField("source_worker_address")
    private String sourceWorkerAddress;

    @TableField("source_worker_download_prefix")
    private String sourceWorkerDownloadPrefix;

    @TableField("file_sha1_hash")
    private String fileSha1Hash;
}
