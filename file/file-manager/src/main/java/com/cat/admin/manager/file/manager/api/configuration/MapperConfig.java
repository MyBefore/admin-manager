package com.cat.admin.manager.file.manager.api.configuration;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.cat.admin.manager.file.manager.data.mapper")
public class MapperConfig {
}
