package com.cat.admin.manager.file.manager.api.controller;

import com.cat.admin.manager.file.manager.core.service.file.FileService;
import com.cat.admin.manager.file.manager.shared.dto.file.AddFileRequest;
import com.cat.admin.manager.file.manager.shared.dto.file.FileDTO;
import com.cat.commons.result.core.GenericResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Api("文件的接口")
@RestController
@RequestMapping("files")
@Validated
public class FileController {

    @Autowired
    private FileService fileService;

    @ApiOperation("添加文件")
    @PostMapping
    public GenericResult<FileDTO> add(@Valid @RequestBody AddFileRequest request) {
        return fileService.add(request);
    }

    @ApiOperation("删除文件")
    @DeleteMapping("{id}")
    public GenericResult<FileDTO> delete(@ApiParam(value = "文件id", required = true) @NotEmpty @PathVariable("id") String id) {
        return fileService.delete(id);
    }

    @ApiOperation("查询多个文件")
    @GetMapping
    public GenericResult<List<FileDTO>> all(@ApiParam(value = "文件id集合", required = true) @NotNull @RequestParam("ids") List<String> ids) {
        return fileService.all(ids);
    }

}
