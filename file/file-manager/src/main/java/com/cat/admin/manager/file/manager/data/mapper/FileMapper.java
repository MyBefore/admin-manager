package com.cat.admin.manager.file.manager.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cat.admin.manager.file.manager.domain.entity.File;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface FileMapper extends BaseMapper<File> {
}
