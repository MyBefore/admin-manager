package com.cat.admin.manager.file.manager.core.consts;

public class MessageConsts {

    public static final String INVALID_FORM = "无效的表单";

    public static final String DELETE_NOT_FOUND = "删除不存在的项";

}
