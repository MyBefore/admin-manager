package com.cat.admin.manager.file.worker.api.controller;

import com.cat.admin.manager.file.worker.core.service.file.FileService;
import com.cat.admin.manager.file.worker.shared.dto.file.UploadFileDTO;
import com.cat.commons.result.core.GenericResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Api("文件操作")
@RestController
@RequestMapping("files")
@Validated
public class FileController {

    @Autowired
    private FileService fileService;

    @ApiOperation("文件上传的接口")
    @PostMapping("{token}")
    public GenericResult<UploadFileDTO> upload(
            @ApiParam("上传文件的token") @NotEmpty @PathVariable("token") String token,
            @ApiParam("上传的文件对象") @NotNull @RequestParam("file") MultipartFile file
    ) {
        return fileService.upload(token, file);
    }

    @ApiOperation("文件下载的接口")
    @GetMapping("{token}/{fileName}")
    public void download(
            @ApiParam("下载文件的token") @NotEmpty @PathVariable("token") String token,
            @ApiParam("下载的文件名称")  @NotEmpty @PathVariable("fileName") String fileName,
            HttpServletResponse httpServletResponse
    ) {
        fileService.download(token, fileName, httpServletResponse);
    }

}
