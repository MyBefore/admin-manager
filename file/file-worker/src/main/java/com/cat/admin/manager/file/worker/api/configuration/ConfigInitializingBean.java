package com.cat.admin.manager.file.worker.api.configuration;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class ConfigInitializingBean implements InitializingBean {

    @Value("${worker.files-physics-dir}")
    private String workerFilesPhysicsDir;

    @Value("${spring.servlet.multipart.location}")
    private String springServletMultipartLocation;

    @Override
    public void afterPropertiesSet() throws Exception {
        initFileDirs();
    }

    /**
     * 初始化一些文件目录
     */
    private void initFileDirs() {

        File temp = new File(springServletMultipartLocation);
        if (temp.exists() && temp.isFile()) {
            temp.delete();
        }
        if (!temp.exists()) {
            temp.mkdirs();
        }

        // 初始化物理存储文件目录
        File file = new File(workerFilesPhysicsDir);
        if (file.exists() && file.isFile()) {
            file.delete();
        }
        if (!file.exists()) {
            file.mkdirs();
        }
    }
}
