package com.cat.admin.manager.file.worker;

import com.cat.commons.result.spring.annotation.EnableResult;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@EnableResult
public class FileWorkerApplication {
    public static void main(String[] args) {
        SpringApplication.run(FileWorkerApplication.class, args);
    }
}
