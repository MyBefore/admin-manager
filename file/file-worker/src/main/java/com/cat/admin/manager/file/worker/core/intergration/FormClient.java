package com.cat.admin.manager.file.worker.core.intergration;

import com.cat.admin.manager.file.manager.shared.api.FormApi;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

@FeignClient("file-manager")
@Component
public interface FormClient extends FormApi {
}
