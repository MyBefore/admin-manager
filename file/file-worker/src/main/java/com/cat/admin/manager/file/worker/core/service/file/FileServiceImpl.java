package com.cat.admin.manager.file.worker.core.service.file;

import com.cat.admin.manager.file.manager.shared.dto.file.AddFileRequest;
import com.cat.admin.manager.file.manager.shared.dto.file.FileDTO;
import com.cat.admin.manager.file.manager.shared.dto.file.PhysicsFileDTO;
import com.cat.admin.manager.file.manager.shared.dto.form.DownloadFormDTO;
import com.cat.admin.manager.file.worker.core.consts.MessageConsts;
import com.cat.admin.manager.file.worker.core.intergration.FileClient;
import com.cat.admin.manager.file.worker.core.intergration.FormClient;
import com.cat.admin.manager.file.worker.shared.dto.file.UploadFileDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.Results;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import java.io.*;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class FileServiceImpl implements FileService {

    @Autowired
    private FileClient fileClient;

    @Autowired
    private FormClient formClient;

    /**
     * 服务名称
     */
    @Value("${spring.application.name}")
    private String name;

    /**
     * 物理文件存储目录
     */
    @Value("${worker.files-physics-dir}")
    private String filesPhysicsDir;

    /**
     * 表单访问的url
     */
    @Value("${worker.form.access-url}")
    private String formAccessUrl;

    @Value("${worker.form.download-url-prefix}")
    private String downloadPrefix;

    @Override
    public GenericResult<UploadFileDTO> upload(String token, MultipartFile file) {


        // 校验并使用表单
        GenericResult<PhysicsFileDTO> pfr = formClient.checkForm(name, token, HttpMethod.POST.name());
        if (pfr == null) {
            throw new RuntimeException("File Manager Error");
        }
        if (!pfr.getResult()) {
            return Results.failedGeneric(pfr.getMessage(), pfr.getStatus());
        }
        if (!pfr.getData().getSourceWorkerAddress().equals(formAccessUrl)) {
            // 彻底移除表单
            formClient.removeForm(name, token, HttpMethod.POST.name());
            return Results.failedGeneric(MessageConsts.INVALID_FORM, HttpStatus.OK);
        }

        String filePath = filesPhysicsDir + pfr.getData().getPhysicsPath();

        String sha1 = null;
        // 复制文件到指定的位置
        try {
            File f = new File(filePath);
            if (f.exists()) {
//                // 彻底移除表单
//                formClient.removeForm(name, token, HttpMethod.POST.name());
//                return Results.failedGeneric(MessageConsts.FILE_EXISTS, HttpStatus.OK);
                // 删除已存在的文件
                f.delete();
            }
            file.transferTo(f);
            try (InputStream is = new FileInputStream(f)) {
                sha1 = DigestUtils.sha1Hex(is);
            }
        } catch (IOException e) {
            e.printStackTrace();
            // 彻底移除表单
            formClient.removeForm(name, token, HttpMethod.POST.name());
            throw new RuntimeException(e.getMessage());
        }

        // 添加文件
        AddFileRequest add = new AddFileRequest();
        add.setName(file.getOriginalFilename());
        add.setSize(BigInteger.valueOf(file.getSize()));
        add.setPhysicsPath(pfr.getData().getPhysicsPath());
        add.setWorker(name);
        add.setSourceWorkerAddress(formAccessUrl);
        add.setContentType(file.getContentType());
        add.setSourceWorkerDownloadPrefix(downloadPrefix);
        add.setFileSha1Hash(sha1);

        GenericResult<FileDTO> ar = fileClient.add(add);
        if (ar == null) {
            // 彻底移除表单
            formClient.removeForm(name, token, HttpMethod.POST.name());
            throw new RuntimeException("File Manager Error");
        }
        if (!ar.getResult()) {
            // 彻底移除表单
            formClient.removeForm(name, token, HttpMethod.POST.name());
            return Results.failedGeneric(ar.getMessage(), ar.getStatus());
        }

        // 彻底移除表单
        formClient.removeForm(name, token, HttpMethod.POST.name());
        return Results.successGeneric(transform(ar.getData()));
    }

    @Override
    public void download(String token, String fileName, HttpServletResponse httpServletResponse) {

        // 校验并使用表单
        GenericResult<PhysicsFileDTO> pfr = formClient.checkForm(name, token, HttpMethod.GET.name());
        if (pfr == null) {
            log.error("File Manager Error");
            httpServletResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return;
        }
        if (!pfr.getResult()) {
            log.error(pfr.getMessage());
            httpServletResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return;
        }

        // 彻底移除表单
        formClient.removeForm(name, token, HttpMethod.GET.name());

        File file = new File(filesPhysicsDir + pfr.getData().getPhysicsPath());

        if (!file.exists() || !sha1Hex(file).equals(pfr.getData().getFileSha1Hash())) {
            try {
                // 从源主机下载
                downloadFromSourceWorker(pfr.getData(), file, fileName);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                httpServletResponse.setStatus(HttpStatus.NOT_FOUND.value());
                return;
            }
        }

        httpServletResponse.setContentType(pfr.getData().getContentType());
        try (
            FileInputStream fis = new FileInputStream(file)
        ) {
            //校验sha1值
            String sha1Hex = DigestUtils.sha1Hex(fis);
            if (sha1Hex.equals(pfr.getData().getFileSha1Hash())) {
                try (ServletOutputStream os = httpServletResponse.getOutputStream()) {
                    Files.copy(file.toPath(), os);
                    os.flush();
                }
            } else {
                log.error("File corruption");
                httpServletResponse.setStatus(HttpStatus.NOT_FOUND.value());
            }
        } catch (FileNotFoundException e) {
            log.error(e.getMessage(), e);
            httpServletResponse.setStatus(HttpStatus.NOT_FOUND.value());
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            httpServletResponse.setStatus(HttpStatus.NOT_FOUND.value());
        }
    }

    /**
     * 从源主机下载文件
     * @param physicsFileDTO
     * @param file
     * @param fileName
     */
    private void downloadFromSourceWorker(PhysicsFileDTO physicsFileDTO, File file, String fileName) {
        if (file.exists()) {
            file.delete();
        }
        // 先向源主机下载
        // 如果源主机就是当前主机则不存在此文件
        if (physicsFileDTO.getSourceWorkerAddress().equals(formAccessUrl)) {
            throw new RuntimeException("File not exists");
        }
        // 得到下载表单
        GenericResult<List<DownloadFormDTO>> dfsr = formClient.generateDownloadForms(Arrays.asList(physicsFileDTO.getId()));
        if (dfsr == null) {
            throw new RuntimeException("File Manager Error");
        }
        if (!dfsr.getResult()) {
            throw new RuntimeException(dfsr.getMessage());
        }
        if (dfsr.getData().isEmpty()) {
            throw new RuntimeException("Unable to generate form");
        }
        DownloadFormDTO downloadFormDTO = dfsr.getData().get(0);
        @NotEmpty String accessToken = downloadFormDTO.getAccessToken();

        InputStream is = null;
        try {
            URLConnection urlConnection = new URL(
                    physicsFileDTO.getSourceWorkerAddress()
                            + physicsFileDTO.getSourceWorkerDownloadPrefix()
                            + accessToken
                            + "/"
                            + fileName
            ).openConnection();
            if (urlConnection instanceof HttpURLConnection) {

                HttpURLConnection uc = (HttpURLConnection) urlConnection;
                uc.setRequestMethod(downloadFormDTO.getMethod());
                uc.setDoInput(true);
                is = uc.getInputStream();

                Files.copy(is, file.toPath());
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    private UploadFileDTO transform(FileDTO fileDTO) {
        UploadFileDTO uploadFileDTO = new UploadFileDTO();
        BeanUtils.copyProperties(fileDTO, uploadFileDTO);
        return uploadFileDTO;
    }

    private String sha1Hex(File file) {
        try (InputStream is = new FileInputStream(file)) {
            return DigestUtils.sha1Hex(is);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
