package com.cat.admin.manager.file.worker.core.service.file;

import com.cat.admin.manager.file.worker.shared.dto.file.UploadFileDTO;
import com.cat.commons.result.core.GenericResult;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 文件操作
 */
public interface FileService {

    /**
     * 文件上传
     * @param token
     * @param file
     * @return
     */
    GenericResult<UploadFileDTO> upload(String token, MultipartFile file);

    /**
     * 文件下载
     * @param token
     * @param fileName
     * @param httpServletResponse
     */
    void download(String token, String fileName, HttpServletResponse httpServletResponse);
}
