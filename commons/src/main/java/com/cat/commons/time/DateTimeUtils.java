package com.cat.commons.time;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

public class DateTimeUtils {

    /**
     * 获取UTC的当前UNIX时间
     * @return
     */
    public static long getUTCCurrentTimeMillis() {
        return LocalDateTime.ofInstant(Instant.now(), ZoneId.of("Z")).atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli();
    }

    /**
     * 获取某个时区的当前UNIX时间
     * @param zoneId
     * @return
     */
    public static long getCurrentTimeMillis(ZoneId zoneId) {
        return LocalDateTime.ofInstant(Instant.now(), zoneId).atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli();
    }

    /**
     * 获取当前某个时区偏移的UNIX时间
     * @param zoneOffset
     * @return
     */
    public static long getCurrentTimeMillis(ZoneOffset zoneOffset) {
        Instant now = Instant.now();
        return LocalDateTime.ofEpochSecond(now.getEpochSecond(), now.getNano(), zoneOffset).atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli();
    }
}
