package com.cat.commons.result.spring.converter;

import com.cat.commons.result.core.Result;
import com.cat.commons.result.core.Results;
import com.cat.commons.result.spring.ResultConverter;
import org.springframework.http.HttpStatus;

import javax.validation.ConstraintViolationException;

public class ConstraintViolationExceptionConverter implements ResultConverter {
    @Override
    public boolean supports(Object o) {
        return o instanceof ConstraintViolationException;
    }

    @Override
    public Result convert(Object o) {
        ConstraintViolationException cve = (ConstraintViolationException) o;
        return Results.failed(cve.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
