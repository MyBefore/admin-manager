package com.cat.commons.result.spring;

import com.cat.commons.result.core.Result;

/**
 * 结果转换器
 */
public interface ResultConverter {

    /**
     * 判断是否支持转换
     * @param o
     * @return
     */
    boolean supports(Object o);

    /**
     * 进行转换
     * @param o
     * @return
     */
    Result convert(Object o);
}
