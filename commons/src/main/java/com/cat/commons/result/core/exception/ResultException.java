package com.cat.commons.result.core.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ResultException extends RuntimeException {

    private HttpStatus status;

    public ResultException(HttpStatus status) {
        this.status = status;
    }

    public ResultException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

    public ResultException(String message, Throwable cause, HttpStatus status) {
        super(message, cause);
        this.status = status;
    }

    public ResultException(Throwable cause, HttpStatus status) {
        super(cause);
        this.status = status;
    }

    public ResultException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, HttpStatus status) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.status = status;
    }
}
