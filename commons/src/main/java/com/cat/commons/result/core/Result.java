package com.cat.commons.result.core;

import org.springframework.http.HttpStatus;

public class Result {

    private boolean result;
    private String message;
    private HttpStatus status;

    public Result() {
    }

    public Result(boolean result, String message, HttpStatus status) {
        this.result = result;
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }
}
