package com.cat.commons.result.core;

import org.springframework.http.HttpStatus;

import java.util.Collection;

public class PagedResult<T> extends GenericResult<Collection<T>> {
    private long pageIndex;
    private long pageSize;
    private long total;

    public PagedResult() {
    }

    public PagedResult(boolean result, String message, HttpStatus status, Collection<T> data, long pageIndex, long pageSize, long total) {
        super(result, message, status, data);
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
        this.total = total;
    }

    public long getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(long pageIndex) {
        this.pageIndex = pageIndex;
    }

    public long getPageSize() {
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }
}
