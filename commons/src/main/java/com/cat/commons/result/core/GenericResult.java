package com.cat.commons.result.core;

import org.springframework.http.HttpStatus;

public class GenericResult<T> extends Result {
    private T data;

    public GenericResult() {
    }

    public GenericResult(boolean result, String message, HttpStatus status, T data) {
        super(result, message, status);
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
