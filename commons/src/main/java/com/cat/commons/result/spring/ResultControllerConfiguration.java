package com.cat.commons.result.spring;

import com.cat.commons.result.spring.converter.ConstraintViolationExceptionConverter;
import com.cat.commons.result.spring.converter.ResultExceptionConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class ResultControllerConfiguration {

    /**
     * 创建返回结果注册器
     * @return
     */
    @Bean
    public ResultConverterRegistor resultConverterRegistor() {
        return new ResultConverterRegistor();
    }

    /**
     * 参数校验的转换器
     * @return
     */
    @Bean
    public ConstraintViolationExceptionConverter constraintViolationExceptionConverter(ResultConverterRegistor resultConverterRegistor) {
        ConstraintViolationExceptionConverter constraintViolationExceptionConverter = new ConstraintViolationExceptionConverter();
        resultConverterRegistor.registration(constraintViolationExceptionConverter);
        return constraintViolationExceptionConverter;
    }

    /**
     * 特殊的返回异常转换
     * @return
     */
    @Bean
    public ResultExceptionConverter resultExceptionConverter(ResultConverterRegistor resultConverterRegistor) {
        ResultExceptionConverter resultExceptionConverter = new ResultExceptionConverter();
        resultConverterRegistor.registration(resultExceptionConverter);
        return resultExceptionConverter;
    }

}
