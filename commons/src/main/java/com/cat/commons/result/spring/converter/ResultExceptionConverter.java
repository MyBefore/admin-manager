package com.cat.commons.result.spring.converter;

import com.cat.commons.result.core.Result;
import com.cat.commons.result.core.Results;
import com.cat.commons.result.spring.ResultConverter;
import com.cat.commons.result.core.exception.ResultException;

public class ResultExceptionConverter implements ResultConverter {

    @Override
    public boolean supports(Object o) {
        return o instanceof ResultException;
    }

    @Override
    public Result convert(Object o) {
        ResultException re = (ResultException) o;
        return Results.failed(re.getMessage(), re.getStatus());
    }
}
