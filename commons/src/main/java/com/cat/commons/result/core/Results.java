package com.cat.commons.result.core;

import org.springframework.http.HttpStatus;

import java.util.Collection;

/**
 * 返回结果的通用工具类
 */
public class Results {

    public static Result success() {
        return new Result(true, null, HttpStatus.OK);
    }

    public static Result failed(String message, HttpStatus status) {
        return new Result(false, message, status);
    }

    public static <T> GenericResult<T> successGeneric(T data) {
        return new GenericResult<T>(true, null, HttpStatus.OK, data);
    }

    public static <T> GenericResult<T> failedGeneric(String message, HttpStatus status) {
        return new GenericResult<T>(false, message, status, null);
    }

    public static <T> PagedResult<T> successPaged(Collection<T> data, long pageIndex, long pageSize, long total) {
        return new PagedResult<T>(true, null, HttpStatus.OK, data, pageIndex, pageSize, total);
    }

    public static <T> PagedResult<T> failedPaged(String message, HttpStatus status) {
        return new PagedResult<T>(false, message, status, null, 0, 0, 0);
    }
    
}
