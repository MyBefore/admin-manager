package com.cat.commons.result.spring.annotation;

import com.cat.commons.result.spring.ResultControllerConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 开启Result返回结果处理
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(ResultControllerConfiguration.class)
public @interface EnableResult {
}
