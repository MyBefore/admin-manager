package com.cat.commons.result.spring;

import com.cat.commons.result.core.Result;
import com.cat.commons.result.core.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.List;

/**
 * Controller的返回结果处理
 */
@ControllerAdvice
public class ResultControllerAdvice implements ResponseBodyAdvice<Object> {

    @Autowired(required = true)
    private ResultConverterRegistor resultConverterRegistor;

    /**
     * 统一的异常拦截
     * @param e
     * @return
     */
    @ExceptionHandler({Exception.class})
    @ResponseBody
    public Result exceptionHandler(Exception e) {
        try {
            ResultConverter supportConverter = getSupportConverter(e);
            if (supportConverter != null) {
                Result r = supportConverter.convert(e);
                return r;
            }
            e.printStackTrace();
            return Results.failed(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception ex) {
            ex.printStackTrace();
            return Results.failed(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * 过滤所有的rest接口
     * @param methodParameter
     * @param aClass
     * @return
     */
    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
        Class<?> controllerClass = methodParameter.getContainingClass();
        return controllerClass.isAnnotationPresent(RestController.class)
                || controllerClass.isAnnotationPresent(ResponseBody.class)
                || methodParameter.getMethodAnnotation(ResponseBody.class) != null;
    }

    @Override
    public Object beforeBodyWrite(Object o, MethodParameter methodParameter, MediaType mediaType, Class<? extends HttpMessageConverter<?>> aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {

        try {
            // 判断是否是Result的返回类型
            if (o instanceof Result) {
                serverHttpResponse.setStatusCode(((Result)o).getStatus());
                return o;
            }
            ResultConverter supportConverter = getSupportConverter(o);
            if (supportConverter != null) {
                Result r = supportConverter.convert(o);
                serverHttpResponse.setStatusCode(r.getStatus());
                return r;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Results.failed(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        // 没有适合的转化
        return o;
    }

    private ResultConverter getSupportConverter(Object o) {
        // 通过注册的转换器进行转换
        List<ResultConverter> converters = resultConverterRegistor.getConverters();
        for (ResultConverter converter : converters) {
            if (converter.supports(o)) {
                return converter;
            }
        }
        return null;
    }
}
