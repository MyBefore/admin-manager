package com.cat.commons.result.spring;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 转换注册器
 */
public class ResultConverterRegistor {

    private List<ResultConverter> converters = new CopyOnWriteArrayList<>();

    /**
     * 注册转换器
     * @param resultConverter
     */
    public boolean registration(ResultConverter resultConverter) {
        return converters.add(resultConverter);
    }

    /**
     * 移除注册器
     * @param resultConverter
     */
    public boolean unRegistration(ResultConverter resultConverter) {
        return converters.remove(resultConverter);
    }

    /**
     * 获取转换器列表
     * @return
     */
    public List<ResultConverter> getConverters() {
        return converters;
    }
}
