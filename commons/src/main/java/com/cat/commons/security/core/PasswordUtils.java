package com.cat.commons.security.core;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.codec.Hex;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class PasswordUtils {

    private static SecureRandom random = new SecureRandom();

    /**
     * 密码加密
     * @param password
     * @return
     */
    public static String encode(String password) {
        try {
            return "{bcrypt}" + BCrypt.hashpw(String.valueOf(Hex.encode(MessageDigest.getInstance("SHA-256").digest(password.getBytes(Charset.forName("utf8"))))), gensalt());
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * 获取盐
     * @return
     */
    private static String gensalt() {
        int round = 10;
        int i = random.nextInt(32);
        round = i >= 4 ? i : round;
        return BCrypt.gensalt(round, random);
    }

}
