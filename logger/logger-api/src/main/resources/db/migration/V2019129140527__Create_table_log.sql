-- 日志记录表
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `opreator` varchar(255) NOT NULL COMMENT '操作人',
  `server` varchar(255) NOT NULL COMMENT '服务名称',
  `function` varchar(255) NOT NULL COMMENT '操作的功能',
  `method` varchar(255) NOT NULL COMMENT '操作的方法',
  `params` longtext NOT NULL COMMENT '参数',
  `exception` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '异常',
  `returns` longtext COMMENT '返回',
  `start_time` bigint(20) NOT NULL COMMENT '操作开始时间',
  `end_time` bigint(20) NOT NULL COMMENT '操作结束时间',
  `created_time` bigint(20) NOT NULL COMMENT '日志创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='日志表';
