package com.cat.admin.manager.logger.core.intergration;

import com.cat.admin.manager.logger.core.service.log.LogService;
import com.cat.admin.manager.logger.shared.dto.log.AddLogRequest;
import com.cat.admin.manager.logger.shared.kafka.KafkaUtils;
import com.cat.commons.result.core.Result;
import com.cat.commons.result.core.exception.ResultException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaLogListener {

    @Autowired
    private LogService logService;

    @KafkaListener(topics = {KafkaUtils.TOPIC})
    public void recive(AddLogRequest request) {
        Result result = logService.add(request);
        if (!result.getResult()) {
            throw new ResultException(result.getMessage(), result.getStatus());
        }
    }

}
