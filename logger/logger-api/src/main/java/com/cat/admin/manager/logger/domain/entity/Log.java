package com.cat.admin.manager.logger.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("log")
public class Log {

    public static final String COLUMN_OPREATOR = "opreator";
    public static final String COLUMN_SERVER = "server";
    public static final String COLUMN_FUNCTION = "function";
    public static final String COLUMN_METHOD = "method";
    public static final String COLUMN_PARAMS = "params";
    public static final String COLUMN_EXCEPTION = "exception";
    public static final String COLUMN_RETURNS = "returns";

    /**
     * 操作人员
     */
    private String opreator;

    /**
     * 服务名称
     */
    @TableField("`server`")
    private String server;

    /**
     * 操作功能
     */
    @TableField("`function`")
    private String function;

    /**
     * 操作的具体类.方法
     */
    private String method;

    /**
     * 操作的参数
     */
    private String params;

    /**
     * 操作的异常
     */
    private String exception;

    /**
     * 操作的返回结果
     */
    @TableField("`returns`")
    private String returns;

    @TableField("start_time")
    private Long startTime;

    @TableField("end_time")
    private Long endTime;

    @TableField("created_time")
    private Long createdTime;

}
