package com.cat.admin.manager.logger.core.service.log;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cat.admin.manager.logger.data.mapper.LogMapper;
import com.cat.admin.manager.logger.domain.entity.Log;
import com.cat.admin.manager.logger.shared.dto.log.AddLogRequest;
import com.cat.admin.manager.logger.shared.dto.log.LogDTO;
import com.cat.commons.result.core.PagedResult;
import com.cat.commons.result.core.Result;
import com.cat.commons.result.core.Results;
import com.cat.commons.time.DateTimeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LogServiceImpl implements LogService {

    @Autowired
    private LogMapper logMapper;

    @Override
    public Result add(AddLogRequest request) {
        logMapper.insert(transform(request));
        return Results.success();
    }

    @Override
    public PagedResult<LogDTO> page(Integer pageIndex, Integer pageSize, String opreator, String server, String function, Boolean exception, Long minCreatedTime, Long maxCreatedTime, String keywords) {

        QueryWrapper<Log> query = new QueryWrapper<>();

        if (StringUtils.isNotBlank(opreator)) {
            query.like(Log.COLUMN_OPREATOR, opreator);
        }
        if (StringUtils.isNotBlank(server)) {
            query.like(Log.COLUMN_SERVER, server);
        }
        if (StringUtils.isNotBlank(function)) {
            query.like(Log.COLUMN_FUNCTION, function);
        }
        if (exception != null) {
            if (exception) {
                query.isNotNull(Log.COLUMN_EXCEPTION);
            } else {
                query.isNull(Log.COLUMN_EXCEPTION);
            }
        }
        if (StringUtils.isNotBlank(keywords)) {
            query.and(q ->
                    q.like(Log.COLUMN_EXCEPTION, keywords)
                    .or()
                    .like(Log.COLUMN_METHOD, keywords)
                    .or()
                    .like(Log.COLUMN_PARAMS, keywords)
                    .or()
                    .like(Log.COLUMN_RETURNS, keywords)
            );
        }

        IPage<Log> page = logMapper.selectPage(new Page<>(pageIndex, pageSize), query);

        return Results.successPaged(transforms(page.getRecords()), page.getCurrent(), page.getSize(), page.getTotal());
    }

    private Log transform(AddLogRequest request) {
        Log log = new Log();
        BeanUtils.copyProperties(request, log);
        log.setCreatedTime(DateTimeUtils.getUTCCurrentTimeMillis());
        return log;
    }

    private LogDTO transform(Log log) {
        LogDTO dto = new LogDTO();
        BeanUtils.copyProperties(log, dto);
        return dto;
    }

    private List<LogDTO> transforms(List<Log> logs) {
        return logs.stream().map(l -> transform(l)).collect(Collectors.toList());
    }
}
