package com.cat.admin.manager.logger.core.configuration;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.cat.admin.manager.logger.data.mapper")
public class MapperConfig {
}
