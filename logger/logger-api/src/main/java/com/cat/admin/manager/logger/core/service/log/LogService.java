package com.cat.admin.manager.logger.core.service.log;

import com.cat.admin.manager.logger.shared.dto.log.AddLogRequest;
import com.cat.admin.manager.logger.shared.dto.log.LogDTO;
import com.cat.commons.result.core.PagedResult;
import com.cat.commons.result.core.Result;

public interface LogService {


    /**
     * 添加日志
     * @param request
     * @return
     */
    Result add(AddLogRequest request);

    /**
     * 分页查询日志
     * @param pageIndex 页码
     * @param pageSize 条数
     * @param opreator 操作人
     * @param server 服务
     * @param function 功能
     * @param exception 是否异常
     * @param minCreatedTime 最小记录时间
     * @param maxCreatedTime 最大记录时间
     * @param keywords 关键字
     * @return
     */
    PagedResult<LogDTO> page(
            Integer pageIndex,
            Integer pageSize,
            String opreator,
            String server,
            String function,
            Boolean exception,
            Long minCreatedTime,
            Long maxCreatedTime,
            String keywords
    );

}
