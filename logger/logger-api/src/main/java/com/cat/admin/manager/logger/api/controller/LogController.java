package com.cat.admin.manager.logger.api.controller;

import com.cat.admin.manager.logger.core.service.log.LogService;
import com.cat.admin.manager.logger.shared.dto.log.AddLogRequest;
import com.cat.admin.manager.logger.shared.dto.log.LogDTO;
import com.cat.commons.result.core.PagedResult;
import com.cat.commons.result.core.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Api("日志操作接口")
@RestController
@RequestMapping("logs")
@Validated
public class LogController {

    @Autowired
    private LogService logService;

    @ApiOperation("添加日志")
    @PostMapping
    public Result add(@Valid @RequestBody AddLogRequest request) {
        return logService.add(request);
    }

    @ApiOperation("分页查询日志")
    @GetMapping(params = {"pageIndex", "pageSize"})
    public PagedResult<LogDTO> page(
            @ApiParam(value = "页码", required = true) @NotNull @RequestParam("pageIndex") Integer pageIndex,
            @ApiParam(value = "条数", required = true) @NotNull @RequestParam("pageSize") Integer pageSize,
            @ApiParam(value = "操作人员") @RequestParam(value = "opreator", required = false, defaultValue = "") String opreator,
            @ApiParam(value = "服务名称") @RequestParam(value = "server", required = false, defaultValue = "") String server,
            @ApiParam(value = "操作功能") @RequestParam(value = "function", required = false, defaultValue = "") String function,
            @ApiParam(value = "是否异常") @RequestParam(value = "exception", required = false, defaultValue = "") Boolean exception,
            @ApiParam(value = "最小记录时间") @RequestParam(value = "minCreatedTime", required = false, defaultValue = "") Long minCreatedTime,
            @ApiParam(value = "最大记录时间") @RequestParam(value = "maxCreatedTime", required = false, defaultValue = "") Long maxCreatedTime,
            @ApiParam(value = "关键字") @RequestParam(value = "keywords", required = false, defaultValue = "") String keywords
    ) {
        return logService.page(
                pageIndex,
                pageSize,
                opreator,
                server,
                function,
                exception,
                minCreatedTime,
                maxCreatedTime,
                keywords
        );
    }

}
