package com.cat.admin.manager.logger.client.core.service;

import com.cat.admin.manager.logger.shared.dto.log.AddLogRequest;

/**
 * kafka方式的日志服务
 */
public interface LogService {

    /**
     * 添加日志
     * @param request
     */
    void add(AddLogRequest request);

}
