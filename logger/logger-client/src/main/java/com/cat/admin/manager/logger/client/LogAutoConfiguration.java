package com.cat.admin.manager.logger.client;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.kafka.annotation.EnableKafka;

@Configuration
@ComponentScan
@EnableKafka
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class LogAutoConfiguration {
}
