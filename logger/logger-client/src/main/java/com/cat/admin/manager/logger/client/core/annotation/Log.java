package com.cat.admin.manager.logger.client.core.annotation;

import com.cat.admin.manager.logger.client.core.aop.LogAop;

import java.lang.annotation.*;

/**
 * 标识日志的注解
 * @see LogAop
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {

    /**
     * 操作功能
     * @return
     */
    String function();

}
