package com.cat.admin.manager.logger.client.core.converter;

/**
 * 功能转换
 */
public interface FunctionConverter {

    /**
     * 转换操作
     * @param function
     * @return
     */
    String converter(String function);

}
