package com.cat.admin.manager.logger.client;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 开启日志记录
 * @see LogAutoConfiguration
 */
@Import(LogAutoConfiguration.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnableLogger {
}
