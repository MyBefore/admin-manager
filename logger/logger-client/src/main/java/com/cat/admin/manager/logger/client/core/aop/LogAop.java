package com.cat.admin.manager.logger.client.core.aop;

import com.alibaba.fastjson.JSONObject;
import com.cat.admin.manager.authenticator.client.core.consts.MessageConsts;
import com.cat.admin.manager.authenticator.client.core.util.Security;
import com.cat.admin.manager.logger.client.core.annotation.Log;
import com.cat.admin.manager.logger.client.core.converter.FunctionConverter;
import com.cat.admin.manager.logger.client.core.service.LogService;
import com.cat.admin.manager.logger.shared.dto.log.AddLogRequest;
import com.cat.commons.result.core.exception.ResultException;
import com.cat.commons.time.DateTimeUtils;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Aspect
@Component
public class LogAop {

    @Value("${spring.application.name}")
    private String server;

    @Autowired
    private LogService logService;

    @Autowired
    private FunctionConverter functionConverter;

    private static final LocalVariableTableParameterNameDiscoverer PARAMETER_NAME_DISCOVERER = new LocalVariableTableParameterNameDiscoverer();

    @Order
    @Around("@annotation(com.cat.admin.manager.logger.client.core.annotation.Log)")
    public Object auth(ProceedingJoinPoint joinPoint) throws Throwable {
        AddLogRequest request = new AddLogRequest();
        // 准备日志
        prepareLog(request, getTargetMethod(joinPoint), joinPoint.getArgs());
        try {
            Object proceed = joinPoint.proceed(joinPoint.getArgs());
            // 设置日志结果
            resultLog(request, proceed);
            return proceed;
        } catch (Throwable throwable) {
            // 设置日志异常
            exceptionLog(request, throwable);
            throw throwable;
        } finally {
            // 保存日志
            saveLog(request);
        }
    }

    /**
     * 准备设置日志
     * @param request
     * @param method
     * @param args
     */
    private void prepareLog(AddLogRequest request, Method method, Object[] args) {
        // 设置功能
        request.setFunction(unknownFilter(convertFunction(getLog(method).function())));
        // 设置具体操作方法
        request.setMethod(unknownFilter(method.toString()));
        // 设置操作人
        request.setOpreator(unknownFilter(getOpreator()));
        // 设置服务
        request.setServer(unknownFilter(server));
        // 设置参数
        request.setParams(unknownFilter(serializeParams(method, args)));
        // 设置开始时间
        request.setStartTime(DateTimeUtils.getUTCCurrentTimeMillis());
    }

    /**
     * 设置结果日志
     * @param request
     * @param result
     */
    private void resultLog(AddLogRequest request, Object result) {
        // 设置结束时间
        request.setEndTime(DateTimeUtils.getUTCCurrentTimeMillis());
        // 设置返回结果
        request.setReturns(unknownFilter(serializeResults(result)));
    }

    /**
     * 设置异常日志
     * @param request
     * @param throwable
     */
    private void exceptionLog(AddLogRequest request, Throwable throwable) {
        // 设置结束时间
        request.setEndTime(DateTimeUtils.getUTCCurrentTimeMillis());
        // 设置异常结果
        request.setException(unknownFilter(serializeException(throwable)));
    }

    /**
     * 保存日志
     * @param request
     */
    private void saveLog(AddLogRequest request) {
        logService.add(request);
    }

    /**
     * 通过目标方法获取日志
     * @param method
     * @return
     */
    private Log getLog(Method method) {
        return method.getAnnotation(Log.class);
    }

    /**
     * 获取目标方法
     * @param joinPoint
     * @return
     * @throws NoSuchMethodException
     */
    private static Method getTargetMethod(ProceedingJoinPoint joinPoint) throws NoSuchMethodException {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        return joinPoint.getTarget().getClass().getMethod(signature.getName(), signature.getParameterTypes());
    }

    /**
     * 获取操作人
     * @return
     */
    private String getOpreator() {
        User user = Security.getUser();
        if (user == null) {
            throw new ResultException(MessageConsts.NOT_ACCESS_AUTHORITY, HttpStatus.OK);
        }
        return user.getUsername();
    }

    /**
     * 序列化参数
     * @param method
     * @param args
     * @return
     */
    private String serializeParams(Method method, Object[] args) {
        String[] parameterNames = PARAMETER_NAME_DISCOVERER.getParameterNames(method);
        JSONObject json = new JSONObject();
        for (int i = 0, length = parameterNames.length < args.length ? parameterNames.length : args.length; i < length; i++) {
            json.put(parameterNames[i], args[i]);
        }
        return json.toJSONString();
    }

    private String serializeResults(Object results) {
        return JSONObject.toJSONString(results);
    }

    private String serializeException(Throwable throwable) {
        return throwable.toString();
    }

    private String convertFunction(String function) {
        return functionConverter.converter(function);
    }

    /**
     * 过滤无效字符串
     * @param str
     * @return
     */
    private String unknownFilter(String str) {
        if (StringUtils.isBlank(str)) {
            return LogUtils.UNKNOWN;
        }
        return str;
    }
}
