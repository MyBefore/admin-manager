package com.cat.admin.manager.logger.client.core.service.kafka;

import com.cat.admin.manager.logger.client.core.service.LogService;
import com.cat.admin.manager.logger.shared.dto.log.AddLogRequest;
import com.cat.admin.manager.logger.shared.kafka.KafkaUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaLogServiceImpl implements LogService {

    @Autowired
    private KafkaTemplate<String, AddLogRequest> kafkaTemplate;

    @Override
    public void add(AddLogRequest request) {
        kafkaTemplate.send(KafkaUtils.TOPIC, request);
    }
}
