package com.cat.admin.manager.logger.client.core.configuration;

import com.cat.admin.manager.logger.client.core.converter.FunctionConverter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnMissingBean(FunctionConverter.class)
public class FunctionConverterConfig {

    @Bean
    public FunctionConverter functionConverter() {
        return new DefaultFunctionConverter();
    }

    public static class DefaultFunctionConverter implements FunctionConverter {

        @Override
        public String converter(String function) {
            return function;
        }
    }

}
