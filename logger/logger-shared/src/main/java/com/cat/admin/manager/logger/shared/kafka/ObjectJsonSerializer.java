package com.cat.admin.manager.logger.shared.kafka;

import com.alibaba.fastjson.JSONObject;
import org.apache.kafka.common.serialization.Serializer;

import java.nio.charset.Charset;
import java.util.Map;

public class ObjectJsonSerializer implements Serializer {
    @Override
    public void configure(Map configs, boolean isKey) {}

    @Override
    public byte[] serialize(String topic, Object data) {
        JSONObject json = new JSONObject();
        json.put(SerializerUtils.CLASS, data.getClass());
        json.put(SerializerUtils.DATA, data);
        return JSONObject.toJSONString(json).getBytes(Charset.forName(SerializerUtils.ENCODING));
    }

    @Override
    public void close() {}
}
