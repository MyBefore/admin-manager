package com.cat.admin.manager.logger.shared.dto.log;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel("日志描述")
public class LogDTO extends AddLogRequest {

    @ApiModelProperty(value = "日志记录时间", required = true)
    @NotNull
    private Long createdTime;

}
