package com.cat.admin.manager.logger.shared.kafka;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.kafka.common.serialization.Deserializer;

import java.nio.charset.Charset;
import java.util.Map;

public class ObjectJsonDeserializer implements Deserializer {

    @Override
    public void configure(Map configs, boolean isKey) {}

    @Override
    public Object deserialize(String topic, byte[] data) {
        try {
            JSONObject json = JSONObject.parseObject(new String(data, Charset.forName(SerializerUtils.ENCODING)));
            return parse(getClass(json), getData(json));
        } catch (ClassNotFoundException e) {
            throw new UnsupportedOperationException("Message is unsupported.", e);
        }
    }

    private Object parse(Class<?> c, JSON d) {
        return d.toJavaObject(c);
    }

    private Class<?> getClass(JSONObject json) throws ClassNotFoundException {
        return Class.forName(json.getString(SerializerUtils.CLASS));
    }

    private JSON getData(JSONObject json) {
        return (JSON) json.get(SerializerUtils.DATA);
    }

    @Override
    public void close() {}
}
