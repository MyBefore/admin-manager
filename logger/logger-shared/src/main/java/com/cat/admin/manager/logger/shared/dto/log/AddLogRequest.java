package com.cat.admin.manager.logger.shared.dto.log;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ApiModel("添加日志请求")
public class AddLogRequest {

    @ApiModelProperty(value = "操作人员", required = true)
    @NotBlank
    private String opreator;

    @ApiModelProperty(value = "服务名称", required = true)
    @NotBlank
    private String server;

    @ApiModelProperty(value = "操作功能", required = true)
    @NotBlank
    private String function;

    @ApiModelProperty(value = "操作的具体类.方法", required = true)
    @NotBlank
    private String method;

    @ApiModelProperty(value = "操作的参数", required = true)
    @NotBlank
    private String params;

    @ApiModelProperty(value = "操作的异常")
    private String exception;

    @ApiModelProperty(value = "操作的返回结果")
    private String returns;

    @ApiModelProperty(value = "操作的开始时间", required = true)
    @NotNull
    private Long startTime;

    @ApiModelProperty(value = "操作的结束时间", required = true)
    @NotNull
    private Long endTime;
}
