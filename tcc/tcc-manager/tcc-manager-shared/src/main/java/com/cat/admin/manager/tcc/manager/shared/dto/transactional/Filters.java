package com.cat.admin.manager.tcc.manager.shared.dto.transactional;

import com.alibaba.fastjson.JSONObject;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Filters {

    private List<Filter> filters;

    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }
}
