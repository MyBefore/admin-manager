package com.cat.admin.manager.tcc.manager.shared.dto.participator;

import lombok.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 任务状态
 */
public enum TaskStatus {

    /**
     * 准备
     */
    READY,

    /**
     * 任务中
     */
    RUNNING,

    /**
     * 任务成功
     */
    SUCCESS,

    /**
     * 回滚中
     */
    ROLLBACKING,

    /**
     * 任务失败
     */
    FAILED,

    /**
     * 回滚完成
     */
    ROLLBACKED;

    /**
     * 合并关系映射
     */
    public static final Map<DoubleValue<TaskStatus>, TaskStatus> MERGE_MAP = new HashMap<>();
    static {
        // 相同的状态合并成相同的状态
        MERGE_MAP.put(new DoubleValue<>(READY, READY), READY);
        MERGE_MAP.put(new DoubleValue<>(RUNNING, RUNNING), RUNNING);
        MERGE_MAP.put(new DoubleValue<>(SUCCESS, SUCCESS), SUCCESS);
        MERGE_MAP.put(new DoubleValue<>(ROLLBACKING, ROLLBACKING), ROLLBACKING);
        MERGE_MAP.put(new DoubleValue<>(FAILED, FAILED), FAILED);
        MERGE_MAP.put(new DoubleValue<>(ROLLBACKED, ROLLBACKED), ROLLBACKED);

        MERGE_MAP.put(new DoubleValue<>(READY, RUNNING), RUNNING);
        MERGE_MAP.put(new DoubleValue<>(READY, SUCCESS), RUNNING);
        MERGE_MAP.put(new DoubleValue<>(READY, ROLLBACKING), ROLLBACKING);
        MERGE_MAP.put(new DoubleValue<>(READY, FAILED), FAILED);
        MERGE_MAP.put(new DoubleValue<>(READY, ROLLBACKED), FAILED);

        MERGE_MAP.put(new DoubleValue<>(RUNNING, SUCCESS), RUNNING);
        MERGE_MAP.put(new DoubleValue<>(RUNNING, ROLLBACKING), ROLLBACKING);
        MERGE_MAP.put(new DoubleValue<>(RUNNING, FAILED), FAILED);
        MERGE_MAP.put(new DoubleValue<>(RUNNING, ROLLBACKED), FAILED);

        MERGE_MAP.put(new DoubleValue<>(SUCCESS, ROLLBACKING), FAILED);
        MERGE_MAP.put(new DoubleValue<>(SUCCESS, FAILED), FAILED);
        MERGE_MAP.put(new DoubleValue<>(SUCCESS, ROLLBACKED), FAILED);

        MERGE_MAP.put(new DoubleValue<>(ROLLBACKING, FAILED), FAILED);
        MERGE_MAP.put(new DoubleValue<>(ROLLBACKING, ROLLBACKED), FAILED);

        MERGE_MAP.put(new DoubleValue<>(FAILED, ROLLBACKED), FAILED);
    }

    /**
     * 合并任务状态 同时两种状态存在时 应当是什么状态
     * @param t1
     * @param t2
     * @return
     */
    public static TaskStatus merge(TaskStatus t1, TaskStatus t2) {

        if (t1 == null) {
            return t2;
        }

        if (t2 == null) {
            return t1;
        }

        DoubleValue<TaskStatus> taskStatusDoubleValue = new DoubleValue<>(t1, t2);
        TaskStatus taskStatus = MERGE_MAP.get(taskStatusDoubleValue);
        if (taskStatus == null) {
            taskStatusDoubleValue.a = t2;
            taskStatusDoubleValue.b = t1;
            taskStatus = MERGE_MAP.get(taskStatusDoubleValue);
        }

        return taskStatus;

    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class DoubleValue<T> {

        private T a;

        private T b;
    }
}
