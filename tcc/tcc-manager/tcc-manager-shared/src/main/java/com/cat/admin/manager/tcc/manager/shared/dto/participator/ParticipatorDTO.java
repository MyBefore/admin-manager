package com.cat.admin.manager.tcc.manager.shared.dto.participator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@ApiModel("参与者详情")
@Data
public class ParticipatorDTO extends EditParticipatorRequest {

    @ApiModelProperty(value = "参与者id", required = true)
    @NotEmpty
    private String id;

    @ApiModelProperty(value = "上次活动的时间", required = true)
    @NotNull
    private long lastActiveTime;
}
