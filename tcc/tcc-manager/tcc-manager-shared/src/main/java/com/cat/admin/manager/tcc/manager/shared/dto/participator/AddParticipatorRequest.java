package com.cat.admin.manager.tcc.manager.shared.dto.participator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@ApiModel("添加事务参与者的请求")
@Data
public class AddParticipatorRequest {

    @ApiModelProperty(value = "服务的ID", required = true)
    @NotEmpty
    private String serverId;

    @ApiModelProperty(value = "取消的方法，包.类.方法", required = true)
    @NotEmpty
    private String cancelMethod;

    @ApiModelProperty(value = "参与者任务状态", required = true)
    @NotNull
    private TaskStatus taskStatus;

}
