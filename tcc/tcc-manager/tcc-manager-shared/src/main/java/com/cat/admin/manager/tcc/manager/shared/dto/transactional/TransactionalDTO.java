package com.cat.admin.manager.tcc.manager.shared.dto.transactional;

import com.cat.admin.manager.tcc.manager.shared.dto.participator.ParticipatorDTO;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.TaskStatus;
import com.cat.commons.time.DateTimeUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@ApiModel("事务详情")
@Data
public class TransactionalDTO {

    @ApiModelProperty(value = "事务id", required = true)
    @NotEmpty
    private String id;

    @ApiModelProperty(value = "事务参与者", required = true)
    @NotNull
    private List<ParticipatorDTO> participators = new ArrayList<>();

    @ApiModelProperty(value = "事务创建者id", required = true)
    @NotEmpty
    private String creatorId;

    @ApiModelProperty(value = "服务器当前时间,用于判读是否失效", required = true)
    @NotEmpty
    private long currentTimeMillis;

    @ApiModelProperty(value = "事务状态", required = true)
    @NotNull
    private TaskStatus taskStatus;

    @ApiModelProperty(value = "上次活动的时间", required = true)
    @NotNull
    private long lastActiveTime;
}
