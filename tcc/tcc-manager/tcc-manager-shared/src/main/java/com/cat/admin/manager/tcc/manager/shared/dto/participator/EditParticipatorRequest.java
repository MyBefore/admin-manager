package com.cat.admin.manager.tcc.manager.shared.dto.participator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("编辑事务参与者的请求")
@Data
public class EditParticipatorRequest extends AddParticipatorRequest {

    @ApiModelProperty("取消的参数")
    private String cancelParams;

}
