package com.cat.admin.manager.tcc.manager.shared.dto.transactional;

import com.cat.admin.manager.tcc.manager.shared.dto.participator.AddParticipatorRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@ApiModel("添加事务请求")
@Data
public class AddTransactionalRequest {

    @ApiModelProperty(value = "事务创建者，将作为参与者", required = true)
    @NotNull
    private AddParticipatorRequest creator;

}
