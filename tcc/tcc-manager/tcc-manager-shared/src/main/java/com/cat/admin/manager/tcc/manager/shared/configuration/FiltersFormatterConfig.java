package com.cat.admin.manager.tcc.manager.shared.configuration;

import com.alibaba.fastjson.JSONObject;
import com.cat.admin.manager.tcc.manager.shared.dto.transactional.Filters;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.Formatter;
import org.springframework.format.FormatterRegistrar;
import org.springframework.format.support.FormattingConversionService;

import java.text.ParseException;
import java.util.Locale;

@Configuration
public class FiltersFormatterConfig {

    @Bean
    public FiltersConverter filtersConverter(FormattingConversionService formattingConversionService) {
        FiltersConverter filtersConverter = new FiltersConverter();
        formattingConversionService.addFormatterForFieldType(Filters.class, filtersConverter);
        return filtersConverter;
    }

    /**
     * 用于字符串转换为Filters的Formatter
     */
    public static class FiltersConverter implements Formatter<Filters> {
        @Override
        public Filters parse(String text, Locale locale) throws ParseException {
            return JSONObject.parseObject(text, Filters.class);
        }

        @Override
        public String print(Filters object, Locale locale) {
            return JSONObject.toJSONString(object);
        }
    }

}
