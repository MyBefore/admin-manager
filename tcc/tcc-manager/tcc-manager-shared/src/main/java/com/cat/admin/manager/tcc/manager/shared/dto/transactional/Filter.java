package com.cat.admin.manager.tcc.manager.shared.dto.transactional;

import com.cat.admin.manager.tcc.manager.shared.dto.participator.TaskStatus;
import lombok.Data;

/**
 * 为or过滤
 */
@Data
public class Filter {

    /**
     * 事务状态 null 则不过滤
     */
    private TaskStatus status;

    /**
     * 小于失效的活动的时间 null 则不过滤 lastActiveTime < (current - ltInactiveTime)
     */
    private Long ltInactiveTime;
}
