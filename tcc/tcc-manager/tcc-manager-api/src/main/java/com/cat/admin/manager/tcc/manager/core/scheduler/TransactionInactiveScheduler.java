package com.cat.admin.manager.tcc.manager.core.scheduler;

import com.cat.admin.manager.tcc.manager.core.service.transactional.TransactionalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Timer;
import java.util.TimerTask;

@Component
@Slf4j
public class TransactionInactiveScheduler implements InitializingBean {

    @Value("${tcc.transaction.inactive-time}")
    private long inactiveTime = 10000;

    @Value("${tcc.transaction.inactive-handler-period}")
    private long inactiveHandlerPeriod = 1000L;

    @Autowired
    private TransactionalService transactionalService;

    public void transactionInactiveHandler() {
        log.debug("Delete inactive transactions : {}", transactionalService.deleteInactiveTransactions(inactiveTime).getData());
    }

    @Override
    public void afterPropertiesSet() {
        new Timer("transactionInactiveHandler").schedule(new TimerTask() {
            @Override
            public void run() {
                transactionInactiveHandler();
            }
        }, 0, inactiveHandlerPeriod);
    }
}
