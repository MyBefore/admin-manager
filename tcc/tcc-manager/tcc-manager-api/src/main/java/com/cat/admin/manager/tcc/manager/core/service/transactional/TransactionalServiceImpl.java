package com.cat.admin.manager.tcc.manager.core.service.transactional;

import com.cat.admin.manager.tcc.manager.core.consts.MessageConsts;
import com.cat.admin.manager.tcc.manager.dao.transactional.TransactionalDao;
import com.cat.admin.manager.tcc.manager.domain.participator.Participator;
import com.cat.admin.manager.tcc.manager.domain.transactional.Transactional;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.AddParticipatorRequest;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.EditParticipatorRequest;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.ParticipatorDTO;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.TaskStatus;
import com.cat.admin.manager.tcc.manager.shared.dto.transactional.AddTransactionalRequest;
import com.cat.admin.manager.tcc.manager.shared.dto.transactional.Filter;
import com.cat.admin.manager.tcc.manager.shared.dto.transactional.Filters;
import com.cat.admin.manager.tcc.manager.shared.dto.transactional.TransactionalDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.Result;
import com.cat.commons.result.core.Results;
import com.cat.commons.time.DateTimeUtils;
import com.mongodb.client.result.DeleteResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TransactionalServiceImpl implements TransactionalService {

    @Autowired
    private TransactionalDao transactionalDao;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public GenericResult<TransactionalDTO> createTransactional(AddTransactionalRequest request) {

        long currentTimeMillis = DateTimeUtils.getUTCCurrentTimeMillis();

        Transactional t = transactionalDao.insert(transform(request));

        if (t == null) {
            return Results.failedGeneric(MessageConsts.CREATE_TRANSACTION_ERROR, HttpStatus.OK);
        }

        return Results.successGeneric(transform(t, currentTimeMillis));
    }

    @Override
    public GenericResult<ParticipatorDTO> addParticipator(String id, AddParticipatorRequest request) {

        Transactional t;
        Participator p;

        do {
            t = transactionalDao.findById(id).orElse(null);

            if (t == null) {
                return Results.failedGeneric(MessageConsts.TRANSACTION_IS_NOT_EXISTS, HttpStatus.OK);
            }

            List<Participator> participators = t.getParticipators();
            p = transform(request, generateUniqueParticipatorId(participators));
            participators.add(p);

            // 实现数据原子性操作
            t = mongoTemplate.findAndModify(
                    new Query(
                            Criteria
                                    .where(Transactional.FIELD_ID)
                                    .is(t.getId())
                                    .and(Transactional.FIELD_VERSION)
                                    .is(t.getVersion())
                    ),
                    new Update()
                            .set(Transactional.FIELD_PARTICIPATORS, t.getParticipators())
                            .set(Transactional.FIELD_LAST_ACTIVE_TIME, getTransactionLastActiveTime(t.getParticipators()))
                            .set(Transactional.FIELD_TASK_STATUS, getTransactionTaskStatus(t.getParticipators()))
                            .set(Transactional.FIELD_VERSION, t.getVersion() + 1L),
                    Transactional.class
            );
        } while (t == null);

        return Results.successGeneric(transform(p));
    }

    @Override
    public GenericResult<ParticipatorDTO> submitParticipator(String id, String pid, Long lockId, EditParticipatorRequest request, Long maxKeepActiveTime) {

        Transactional t;
        Participator p;

        do {
            t = transactionalDao.findById(id).orElse(null);

            if (t == null) {
                return Results.failedGeneric(MessageConsts.TRANSACTION_IS_NOT_EXISTS, HttpStatus.OK);
            }

            List<Participator> collect = t.getParticipators().stream().filter(px -> px.getId().equals(pid)).collect(Collectors.toList());
            if (collect.isEmpty()) {
                return Results.failedGeneric(MessageConsts.PARTICIPATOR_IS_NOT_EXISTS, HttpStatus.OK);
            }

            p = collect.get(0);

            // 验证锁是否正确
            if (!lockId.equals(p.getLockId())) {
                return Results.failedGeneric(MessageConsts.PARTICIPATOR_IS_ACTIVE_LOCK_ERROR, HttpStatus.OK);
            }

            // 验证活动时间是否超时 RUNNING->SUCESS RUNNING->FAILED ROLLBACKING->ROLLBACKED
            if (
                    ((TaskStatus.RUNNING.equals(p.getTaskStatus()) && TaskStatus.SUCCESS.equals(request.getTaskStatus()))
                            || (TaskStatus.RUNNING.equals(p.getTaskStatus()) && TaskStatus.FAILED.equals(request.getTaskStatus()))
                            || (TaskStatus.ROLLBACKING.equals(p.getTaskStatus()) && TaskStatus.ROLLBACKED.equals(request.getTaskStatus())))
                            && (DateTimeUtils.getUTCCurrentTimeMillis() - p.getLastActiveTime()) >= maxKeepActiveTime) {
                return Results.failedGeneric(MessageConsts.PARTICIPATOR_IS_INACTIVE, HttpStatus.OK);
            }

            p = transform(p, request);

            // 实现数据原子性操作
            t = mongoTemplate.findAndModify(
                    new Query(
                            Criteria
                                    .where(Transactional.FIELD_ID)
                                    .is(t.getId())
                                    .and(Transactional.FIELD_VERSION)
                                    .is(t.getVersion())
                    ),
                    new Update()
                            .set(Transactional.FIELD_PARTICIPATORS, t.getParticipators())
                            .set(Transactional.FIELD_LAST_ACTIVE_TIME, getTransactionLastActiveTime(t.getParticipators()))
                            .set(Transactional.FIELD_TASK_STATUS, getTransactionTaskStatus(t.getParticipators()))
                            .set(Transactional.FIELD_VERSION, t.getVersion() + 1L),
                    Transactional.class
            );
        } while (t == null);

        return Results.successGeneric(transform(p));
    }

    @Override
    public GenericResult<TransactionalDTO> getTransactional(String id) {

        long currentTimeMillis = DateTimeUtils.getUTCCurrentTimeMillis();

        Transactional t = transactionalDao.findById(id).orElse(null);

        if (t == null) {
            return Results.failedGeneric(MessageConsts.TRANSACTION_IS_NOT_EXISTS, HttpStatus.OK);
        }

        return Results.successGeneric(transform(t, currentTimeMillis));
    }

    @Override
    public GenericResult<List<TransactionalDTO>> getTransactionalsByServerIdAndFilters(String serverId, Filters filters) {
        long currentTimeMillis = DateTimeUtils.getUTCCurrentTimeMillis();

        Criteria criteria = Criteria
                .where(Transactional.FIELD_PARTICIPATORS)
                .elemMatch(
                        Criteria
                                .where(Participator.FILED_SERVER_ID)
                                .is(serverId)
                                // 查询自己未回滚
                                .orOperator(
                                        Criteria.where(Participator.FIELD_TASK_STATUS).is(TaskStatus.SUCCESS),
                                        Criteria.where(Participator.FIELD_TASK_STATUS).is(TaskStatus.FAILED),
                                        Criteria.where(Participator.FIELD_TASK_STATUS).is(TaskStatus.READY),
                                        Criteria.where(Participator.FIELD_TASK_STATUS).is(TaskStatus.RUNNING),
                                        Criteria.where(Participator.FIELD_TASK_STATUS).is(TaskStatus.ROLLBACKING)
                                )
                );

        List<Filter> fs = filters.getFilters();
        if (fs != null) {
            List<Criteria> cs = filtersToCriterias(fs);
            if (!cs.isEmpty()) {
                criteria.orOperator(cs.toArray(new Criteria[cs.size()]));
            }
        }

        return Results.successGeneric(transformsT(mongoTemplate.find(new Query(criteria), Transactional.class), currentTimeMillis));
    }

    @Override
    public Result updateParticipatorActive(String id, String pid, Long lockId, Long maxKeepActiveTime) {

        Transactional t;
        Participator p;

        do {
            t = transactionalDao.findById(id).orElse(null);

            if (t == null) {
                return Results.failed(MessageConsts.TRANSACTION_IS_NOT_EXISTS, HttpStatus.OK);
            }

            List<Participator> collect = t.getParticipators().stream().filter(px -> px.getId().equals(pid)).collect(Collectors.toList());
            if (collect.isEmpty()) {
                return Results.failed(MessageConsts.PARTICIPATOR_IS_NOT_EXISTS, HttpStatus.OK);
            }
            p = collect.get(0);

            // 验证锁是否正确
            if (!lockId.equals(p.getLockId())) {
                return Results.failed(MessageConsts.PARTICIPATOR_IS_ACTIVE_LOCK_ERROR, HttpStatus.OK);
            }

            // 验证活动时间是否超时
            if ((DateTimeUtils.getUTCCurrentTimeMillis() - p.getLastActiveTime()) >= maxKeepActiveTime) {
                return Results.failed(MessageConsts.PARTICIPATOR_IS_INACTIVE, HttpStatus.OK);
            }

            // 设置新的活动时间
            p.setLastActiveTime(DateTimeUtils.getUTCCurrentTimeMillis());

            // 实现数据原子性操作
            t = mongoTemplate.findAndModify(
                    new Query(
                            Criteria
                                    .where(Transactional.FIELD_ID)
                                    .is(t.getId())
                                    .and(Transactional.FIELD_VERSION)
                                    .is(t.getVersion())
                                    .and(Transactional.FIELD_PARTICIPATORS)
                                    .elemMatch(
                                            Criteria
                                                    .where(Participator.FIELD_ID)
                                                    .is(p.getId())
                                                    .and(Participator.FIELD_LAST_ACTIVE_TIME)
                                                    .gt(DateTimeUtils.getUTCCurrentTimeMillis() - maxKeepActiveTime)
                                    )
                    ),
                    new Update()
                            .set(Transactional.FIELD_PARTICIPATORS, t.getParticipators())
                            .set(Transactional.FIELD_LAST_ACTIVE_TIME, getTransactionLastActiveTime(t.getParticipators()))
                            .set(Transactional.FIELD_TASK_STATUS, getTransactionTaskStatus(t.getParticipators()))
                            .set(Transactional.FIELD_VERSION, t.getVersion() + 1L),
                    Transactional.class
            );
        } while (t == null);

        return Results.success();
    }

    @Override
    public GenericResult<Long> tryLockParticipatorActive(String id, String pid, TaskStatus status, Long inactiveTime, Long maxKeepActiveTime) {

        Transactional t;
        Participator p;

        Long lockId;

        do {
            t = transactionalDao.findById(id).orElse(null);

            if (t == null) {
                return Results.failedGeneric(MessageConsts.TRANSACTION_IS_NOT_EXISTS, HttpStatus.OK);
            }

            List<Participator> collect = t.getParticipators().stream().filter(px -> px.getId().equals(pid)).collect(Collectors.toList());
            if (collect.isEmpty()) {
                return Results.failedGeneric(MessageConsts.PARTICIPATOR_IS_NOT_EXISTS, HttpStatus.OK);
            }
            p = collect.get(0);

            // 验证准备超时
            if (TaskStatus.RUNNING.equals(status) && TaskStatus.READY.equals(p.getTaskStatus()) && (DateTimeUtils.getUTCCurrentTimeMillis() - p.getLastActiveTime() >= maxKeepActiveTime.longValue())) {
                return Results.failedGeneric(MessageConsts.PARTICIPATOR_IS_READY_TIMEOUT, HttpStatus.OK);
            }

            // 判断是否是未活动状态
            if ((TaskStatus.ROLLBACKING.equals(p.getTaskStatus()) || TaskStatus.RUNNING.equals(p.getTaskStatus())) && (DateTimeUtils.getUTCCurrentTimeMillis() - p.getLastActiveTime() <= inactiveTime.longValue())) {
                return Results.failedGeneric(MessageConsts.PARTICIPATOR_IS_ACTIVE, HttpStatus.OK);
            }

            // 设置新的状态
            p.setTaskStatus(status);
            // 设置新的活动时间
            p.setLastActiveTime(DateTimeUtils.getUTCCurrentTimeMillis());

            // 锁的id
            lockId = t.getVersion() + 1L;

            p.setLockId(lockId);

            // 实现数据原子性操作
            t = mongoTemplate.findAndModify(
                    new Query(
                            Criteria
                                    .where(Transactional.FIELD_ID)
                                    .is(t.getId())
                                    .and(Transactional.FIELD_VERSION)
                                    .is(t.getVersion())
                    ),
                    new Update()
                            .set(Transactional.FIELD_PARTICIPATORS, t.getParticipators())
                            .set(Transactional.FIELD_LAST_ACTIVE_TIME, getTransactionLastActiveTime(t.getParticipators()))
                            .set(Transactional.FIELD_TASK_STATUS, getTransactionTaskStatus(t.getParticipators()))
                            .set(Transactional.FIELD_VERSION, lockId),
                    Transactional.class
            );
        } while (t == null);

        return Results.successGeneric(lockId);
    }

    @Override
    public GenericResult<Long> deleteInactiveTransactions(Long inactiveTime) {
        DeleteResult remove = mongoTemplate.remove(new Query(
                Criteria
                        .where(Transactional.FIELD_LAST_ACTIVE_TIME)
                        .lt(DateTimeUtils.getUTCCurrentTimeMillis() - inactiveTime)
                        .orOperator(
                                Criteria.where(Transactional.FIELD_TASK_STATUS).is(TaskStatus.SUCCESS),
                                Criteria.where(Transactional.FIELD_TASK_STATUS).is(TaskStatus.ROLLBACKED)
                        )
        ), Transactional.class);
        return Results.successGeneric(remove.getDeletedCount());
    }

    private Participator transform(AddParticipatorRequest request, String id) {
        Participator participator = new Participator();
        BeanUtils.copyProperties(request, participator);
        participator.setId(id);
        return participator;
    }

    private Participator transform(Participator participator, EditParticipatorRequest request) {
        BeanUtils.copyProperties(request, participator);
        return participator;
    }

    private Transactional transform(AddTransactionalRequest request) {
        Transactional transactional = new Transactional();
        BeanUtils.copyProperties(request, transactional);
        Participator creator = transform(request.getCreator(), generateUniqueParticipatorId(Collections.EMPTY_LIST));
        transactional.setCreatorId(creator.getId());
        transactional.getParticipators().add(creator);
        // 设置事务的状态
        transactional.setTaskStatus(getTransactionTaskStatus(transactional.getParticipators()));
        transactional.setLastActiveTime(getTransactionLastActiveTime(transactional.getParticipators()));
        return transactional;
    }

    private TransactionalDTO transform(Transactional transactional, long currentTimeMillis) {
        TransactionalDTO transactionalDTO = new TransactionalDTO();
        BeanUtils.copyProperties(transactional, transactionalDTO);
        transactionalDTO.setParticipators(transformsP(transactional.getParticipators()));
        transactionalDTO.setCurrentTimeMillis(currentTimeMillis);
        return transactionalDTO;
    }

    private ParticipatorDTO transform(Participator participator) {
        ParticipatorDTO participatorDTO = new ParticipatorDTO();
        BeanUtils.copyProperties(participator, participatorDTO);
        return participatorDTO;
    }

    private List<ParticipatorDTO> transformsP(List<Participator> participators) {
        return participators.stream().map(p -> transform(p)).collect(Collectors.toList());
    }

    private List<TransactionalDTO> transformsT(List<Transactional> transactionals, long currentTimeMillis) {
        return transactionals.stream().map(t -> transform(t, currentTimeMillis)).collect(Collectors.toList());
    }

    /**
     * 生成唯一的参与者id
     * @param exists 已存在的参与者
     * @return
     */
    private String generateUniqueParticipatorId(List<Participator> exists) {
        String id;
        do {
            id = UUID.randomUUID().toString();
        } while (existsId(exists, id));
        return id;
    }

    /**
     * 判断是否已有存在参与者的id
     * @param exists 已存在的参与者
     * @param id 尝试的id
     * @return
     */
    private boolean existsId(List<Participator> exists, String id) {
        return exists.stream().anyMatch(e -> e.getId().equals(id));
    }

    /**
     * 通过参与者获取当前事务的状态
     * @param participators 事务的参与者
     * @return
     */
    private TaskStatus getTransactionTaskStatus(List<Participator> participators) {
        return participators.stream().map(p -> p.getTaskStatus()).reduce(TaskStatus::merge).get();
    }

    /**
     * 通过参与者获取事务最后活动的时间
     * @param participators 事务的参与者
     * @return
     */
    private long getTransactionLastActiveTime(List<Participator> participators) {
        return participators.stream().map(p -> p.getLastActiveTime()).reduce(Math::max).get();
    }

    /**
     * 过滤转查询
     * @param filter
     * @return
     */
    private Criteria filterToCriteria(Filter filter) {

        Criteria task = null;

        if (filter.getStatus() != null) {
            task = Criteria.where(Transactional.FIELD_TASK_STATUS).is(filter.getStatus());
        }

        if (task != null && filter.getLtInactiveTime() != null) {
            task.and(Transactional.FIELD_LAST_ACTIVE_TIME).lt(DateTimeUtils.getUTCCurrentTimeMillis() - filter.getLtInactiveTime());
        }

        return task;
    }

    /**
     * 批量过滤转查询
     * @param filters
     * @return
     */
    private List<Criteria> filtersToCriterias(List<Filter> filters) {

        List<Criteria> criterias = new ArrayList<>();

        for (Filter filter : filters) {
            criterias.add(filterToCriteria(filter));
        }

        return criterias;
    }

}
