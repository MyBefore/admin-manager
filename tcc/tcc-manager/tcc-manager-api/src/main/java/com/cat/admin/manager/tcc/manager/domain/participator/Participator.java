package com.cat.admin.manager.tcc.manager.domain.participator;

import com.cat.admin.manager.tcc.manager.shared.dto.participator.TaskStatus;
import com.cat.commons.time.DateTimeUtils;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * 事务参与者
 */
@Data
public class Participator {

    public static final String FIELD_ID = "id";
    public static final String FIELD_LAST_ACTIVE_TIME = "lastActiveTime";
    public static final String FIELD_TASK_STATUS = "taskStatus";
    public static final String FILED_SERVER_ID = "serverId";

    /**
     * 参与者id
     */
    @Id
    private String id;

    /**
     * 取消的参数
     */
    @Field("cancel_params")
    private String cancelParams;

    /**
     * 服务标识
     */
    @Field("server_id")
    private String serverId;

    /**
     * 取消的方法
     */
    @Field("cancel_method")
    private String cancelMethod;

    /**
     * 参与者执行状态
     */
    @Field("task_status")
    private TaskStatus taskStatus = TaskStatus.READY;

    /**
     * 上次活动的时间
     */
    @Field("last_active_time")
    private long lastActiveTime = DateTimeUtils.getUTCCurrentTimeMillis();

    /**
     * 锁的id
     */
    @Field("lock_id")
    private Long lockId;
}
