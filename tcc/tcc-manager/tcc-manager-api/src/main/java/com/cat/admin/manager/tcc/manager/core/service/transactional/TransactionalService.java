package com.cat.admin.manager.tcc.manager.core.service.transactional;


import com.cat.admin.manager.tcc.manager.shared.dto.participator.AddParticipatorRequest;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.EditParticipatorRequest;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.ParticipatorDTO;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.TaskStatus;
import com.cat.admin.manager.tcc.manager.shared.dto.transactional.AddTransactionalRequest;
import com.cat.admin.manager.tcc.manager.shared.dto.transactional.Filters;
import com.cat.admin.manager.tcc.manager.shared.dto.transactional.TransactionalDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.Result;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public interface TransactionalService {

    /**
     * 创建一个新事务
     * @param request
     * @return
     */
    GenericResult<TransactionalDTO> createTransactional(AddTransactionalRequest request);

    /**
     * 添加事务参与者
     * @param id 事务id
     * @param request
     * @return
     */
    GenericResult<ParticipatorDTO> addParticipator(String id, AddParticipatorRequest request);

    /**
     * 提交事务参与者
     * @param id 事务id
     * @param pid 参与者id
     * @param lockId 锁的id
     * @param request
     * @param maxKeepActiveTime 未活动的最大时长
     * @return
     */
    GenericResult<ParticipatorDTO> submitParticipator(String id, String pid, Long lockId, EditParticipatorRequest request, Long maxKeepActiveTime);

    /**
     * 查询事务
     * @param id 事务id
     * @return
     */
    GenericResult<TransactionalDTO> getTransactional(String id);

    /**
     * 根据服务查询事务
     * @param serverId 服务id
     * @return
     */
    GenericResult<List<TransactionalDTO>> getTransactionalsByServerIdAndFilters(String serverId, Filters filters);

    /**
     * 更新参与者id
     * @param id 事务id
     * @param pid 参与者id
     * @param lockId 锁的id
     * @param maxKeepActiveTime 未活动的最大时长
     * @return
     */
    Result updateParticipatorActive(String id, String pid, Long lockId, Long maxKeepActiveTime);

    /**
     * 获取状态锁
     * @param id 事务id
     * @param pid 参与者id
     * @param status 变更为的状态
     * @param inactiveTime 超时的时长
     * @param maxKeepActiveTime 未活动的最大时长
     * @return 返回活动的锁id
     */
    GenericResult<Long> tryLockParticipatorActive(String id, String pid, TaskStatus status, Long inactiveTime, Long maxKeepActiveTime);

    /**
     * 移除未活动的事务
     * @param inactiveTime
     */
    GenericResult<Long> deleteInactiveTransactions(Long inactiveTime);
}
