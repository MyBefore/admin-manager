package com.cat.admin.manager.tcc.manager.api.controller;

import com.cat.admin.manager.tcc.manager.core.service.transactional.TransactionalService;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.AddParticipatorRequest;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.EditParticipatorRequest;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.ParticipatorDTO;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.TaskStatus;
import com.cat.admin.manager.tcc.manager.shared.dto.transactional.AddTransactionalRequest;
import com.cat.admin.manager.tcc.manager.shared.dto.transactional.Filters;
import com.cat.admin.manager.tcc.manager.shared.dto.transactional.TransactionalDTO;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Api("事务处理的接口")
@RestController
@RequestMapping("transactionals")
@Validated
public class TransactionalController {

    @Autowired
    private TransactionalService transactionalService;

    @ApiOperation("创建一个新事务")
    @PostMapping
    public GenericResult<TransactionalDTO> createTransactional(
            @RequestBody @Validated AddTransactionalRequest request
            ) {
        return transactionalService.createTransactional(request);
    }

    @ApiOperation("添加事务参与者")
    @PostMapping("{id}/participators")
    public GenericResult<ParticipatorDTO> addParticipator(
            @ApiParam(value = "事务id", required = true) @NotEmpty @PathVariable("id") String id,
            @RequestBody @Valid AddParticipatorRequest request
    ) {
        return transactionalService.addParticipator(id, request);
    }

    @ApiOperation("提交事务参与者")
    @PutMapping(value = "{id}/participators/{pid}", params = {"maxKeepActiveTime"}, consumes = {"application/json"})
    public GenericResult<ParticipatorDTO> submitParticipator(
            @ApiParam(value = "事务id", required = true) @NotEmpty @PathVariable("id") String id,
            @ApiParam(value = "参与者id", required = true) @NotEmpty @PathVariable("pid") String pid,
            @ApiParam(value = "活动的锁id", required = true) @NotNull @RequestParam("lockId") Long lockId,
            @RequestBody @Valid EditParticipatorRequest request,
            @ApiParam(value = "未活动的最大时长", required = true) @NotNull @RequestParam("maxKeepActiveTime") Long maxKeepActiveTime
    ) {
        return transactionalService.submitParticipator(id, pid, lockId, request, maxKeepActiveTime);
    }

    @ApiOperation("查询事务")
    @GetMapping("{id}")
    public GenericResult<TransactionalDTO> getTransactional(
            @ApiParam(value = "事务id", required = true) @NotEmpty @PathVariable("id") String id
    ) {
        return transactionalService.getTransactional(id);
    }

    @ApiOperation("根据服务id和过滤查询事务")
    @GetMapping(params = {"serverId"})
    public GenericResult<List<TransactionalDTO>> getTransactionalsByServerIdAndFilters(
            @ApiParam(value = "服务id", required = true) @NotEmpty @RequestParam("serverId") String serverId,
            @ApiParam(value = "事务过滤") @RequestParam("filters") Filters filters
    ) {
        return transactionalService.getTransactionalsByServerIdAndFilters(serverId, filters);
    }

    @ApiOperation("更新参与者活动状态")
    @PutMapping(value = "{id}/participators/{pid}", params = {"maxKeepActiveTime"})
    public Result updateParticipatorActive(
            @ApiParam(value = "事务id", required = true) @NotEmpty @PathVariable("id") String id,
            @ApiParam(value = "参与者id", required = true) @NotEmpty @PathVariable("pid") String pid,
            @ApiParam(value = "活动的锁id", required = true) @NotNull @RequestParam("lockId") Long lockId,
            @ApiParam(value = "未活动的最大时长", required = true) @NotNull @RequestParam("maxKeepActiveTime") Long maxKeepActiveTime
    ) {
        return transactionalService.updateParticipatorActive(id, pid, lockId, maxKeepActiveTime);
    }

    @ApiOperation("获取状态锁")
    @GetMapping(value = "{id}/participators/{pid}", params = {"status", "inactiveTime"})
    public GenericResult<Long> tryLockParticipatorActive(
            @ApiParam(value = "事务id", required = true) @NotEmpty @PathVariable("id") String id,
            @ApiParam(value = "参与者id", required = true) @NotEmpty @PathVariable("pid") String pid,
            @ApiParam(value = "锁住的状态", required = true) @NotNull @RequestParam("status") TaskStatus status,
            @ApiParam(value = "未活动的最大时长", required = true) @NotNull @RequestParam("inactiveTime") Long inactiveTime,
            @ApiParam(value = "未活动的最大时长", required = true) @NotNull @RequestParam("maxKeepActiveTime") Long maxKeepActiveTime
            ) {
        return transactionalService.tryLockParticipatorActive(id, pid, status, inactiveTime, maxKeepActiveTime);
    }

}
