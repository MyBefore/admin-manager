package com.cat.admin.manager.tcc.manager.domain.transactional;

import com.cat.admin.manager.tcc.manager.domain.participator.Participator;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.TaskStatus;
import com.cat.commons.time.DateTimeUtils;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.ArrayList;
import java.util.List;

/**
 * 事务
 */
@Document(collection = "transactional")
@Data
public class Transactional {

    public static final String FIELD_ID = "id";
    public static final String FIELD_PARTICIPATORS = "participators";
    public static final String FIELD_VERSION = "version";
    public static final String FIELD_LAST_ACTIVE_TIME = "lastActiveTime";
    public static final String FIELD_TASK_STATUS = "taskStatus";

    /**
     * 事务的id
     */
    @Id
    private String id;

    /**
     * 参与者集合
     */
    private List<Participator> participators = new ArrayList<>();

    /**
     * 创建者id 是参与者中的某一个id 也是事务的创建者
     */
    @Field("creator_id")
    private String creatorId;

    /**
     * 用于乐观锁使用
     */
    @Field("version")
    private long version = 0L;

    /**
     * 事务状态
     */
    @Field("task_status")
    private TaskStatus taskStatus = TaskStatus.READY;

    /**
     * 上次活动的时间
     */
    @Field("last_active_time")
    private long lastActiveTime = DateTimeUtils.getUTCCurrentTimeMillis();

}
