package com.cat.admin.manager.tcc.manager.core.consts;

public class MessageConsts {

    public static final String PARTICIPATOR_IS_NOT_EXISTS = "参与者不存在";
    public static final String PARTICIPATOR_IS_INACTIVE = "参与者已停止活动";
    public static final String PARTICIPATOR_IS_READY_TIMEOUT = "参与者准备超时";
    public static final String PARTICIPATOR_IS_ACTIVE = "参与者是激活状态";
    public static final String PARTICIPATOR_IS_ACTIVE_LOCK_ERROR = "参与者活动的锁不正确";
    public static final String CREATE_TRANSACTION_ERROR = "创建事务错误";
    public static final String TRANSACTION_IS_NOT_EXISTS = "事务不存在";

}
