package com.cat.admin.manager.tcc.manager;

import com.cat.commons.result.spring.annotation.EnableResult;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 用于SpringCloud的分布式事务TCC方式处理的事务协调器
 */
@SpringBootApplication
@EnableEurekaClient
@EnableResult
public class TCCManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TCCManagerApplication.class, args);
    }

}
