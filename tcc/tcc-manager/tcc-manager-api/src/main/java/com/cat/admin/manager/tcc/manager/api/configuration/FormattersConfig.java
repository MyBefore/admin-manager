package com.cat.admin.manager.tcc.manager.api.configuration;

import com.cat.admin.manager.tcc.manager.shared.configuration.FiltersFormatterConfig;
import org.springframework.context.annotation.Import;

@Import(FiltersFormatterConfig.class)
public class FormattersConfig {
}
