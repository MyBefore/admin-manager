package com.cat.admin.manager.tcc.manager.dao.transactional;

import com.cat.admin.manager.tcc.manager.domain.transactional.Transactional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 事务的数据操作层
 */
@Repository
public interface TransactionalDao extends MongoRepository<Transactional, String> {
}
