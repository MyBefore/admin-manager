package com.cat.admin.manager.tcc.client.core;

import com.cat.admin.manager.tcc.client.core.configuration.FormattersConfig;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableFeignClients
@EnableAsync
@ComponentScan
@Configuration
@Import(FormattersConfig.class)
public class TccAutoConfiguration {
}
