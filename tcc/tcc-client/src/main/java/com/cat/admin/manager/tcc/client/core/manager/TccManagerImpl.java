package com.cat.admin.manager.tcc.client.core.manager;

import com.cat.admin.manager.tcc.client.core.domain.TccContext;
import com.cat.admin.manager.tcc.client.core.intergration.TransactionalClient;
import com.cat.admin.manager.tcc.client.core.manager.exception.CannotJoinTransactionException;
import com.cat.admin.manager.tcc.client.core.manager.exception.CannotSubmitTransactionException;
import com.cat.admin.manager.tcc.client.core.scheduler.ParticipatorActiveLock;
import com.cat.admin.manager.tcc.client.core.scheduler.lock.Activer;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.AddParticipatorRequest;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.EditParticipatorRequest;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.ParticipatorDTO;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.TaskStatus;
import com.cat.admin.manager.tcc.manager.shared.dto.transactional.AddTransactionalRequest;
import com.cat.admin.manager.tcc.manager.shared.dto.transactional.TransactionalDTO;
import com.cat.commons.result.core.GenericResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.CannotCreateTransactionException;

@Component
public class TccManagerImpl implements TccManager {

    /**
     * 创建事务后默认的状态
     */
    public static final TaskStatus CREATE_DEFAULT_TASK_STATUS = TaskStatus.READY;

    @Autowired
    private TransactionalClient transactionalClient;

    @Autowired
    private ParticipatorActiveLock participatorActiveLock;

    @Autowired
    private TccContext tccContext;

    /**
     * 创建事务
     * @param serverId 服务id
     * @param cancelMethod 取消方法  com.cat.Test.cancel
     * @return
     */
    @Override
    public TransactionalDTO create(String serverId, String cancelMethod) {

        GenericResult<TransactionalDTO> transactional = transactionalClient.createTransactional(createAddTransactionalRequest(serverId, cancelMethod));

        if (!transactional.getResult() || transactional.getData() == null) {
            throw new CannotCreateTransactionException(transactional.getMessage());
        }

        return transactional.getData();
    }

    /**
     * 获取已存在的事务
     * @param transactionId 事务id
     * @return
     */
    @Override
    public TransactionalDTO getTransaction(String transactionId) {

        GenericResult<TransactionalDTO> transactional = transactionalClient.getTransactional(transactionId);

        if (!transactional.getResult() || transactional.getData() == null) {
            throw new CannotCreateTransactionException(transactional.getMessage());
        }

        return transactional.getData();
    }

    private AddTransactionalRequest createAddTransactionalRequest(String serverId, String cancelMethod) {
        AddTransactionalRequest atr = new AddTransactionalRequest();
        atr.setCreator(createAddParticipatorRequest(serverId, cancelMethod));
        return atr;
    }

    private AddParticipatorRequest createAddParticipatorRequest(String serverId, String cancelMethod) {
        AddParticipatorRequest apr = new AddParticipatorRequest();
        apr.setCancelMethod(cancelMethod);
        apr.setServerId(serverId);
        apr.setTaskStatus(CREATE_DEFAULT_TASK_STATUS);
        return apr;
    }

    /**
     * 加入事务
     * @param transactionId 要加入的事务id
     * @param serverId 服务id
     * @param cancelMethod 取消方法  com.cat.Test.cancel
     * @return
     */
    @Override
    public ParticipatorDTO joinTo(String transactionId, String serverId, String cancelMethod) {

        GenericResult<ParticipatorDTO> participator = transactionalClient.addParticipator(transactionId, createAddParticipatorRequest(serverId, cancelMethod));

        if (!participator.getResult() || participator.getData() == null) {
            throw new CannotJoinTransactionException(participator.getMessage());
        }

        return participator.getData();
    }

    /**
     * 锁定事务
     * @param transactionId 事务id
     * @param participatorId 参与者id
     * @return
     */
    @Override
    public Activer lockRunning(String transactionId, String participatorId) {
        return participatorActiveLock.tryLock(transactionId, participatorId, TaskStatus.RUNNING);
    }

    /**
     * 提交事务状态
     * @param activer 活动者
     * @param epr 提交的状态
     * @return
     */
    @Override
    public ParticipatorDTO submit(Activer activer, EditParticipatorRequest epr) {

        GenericResult<ParticipatorDTO> participator = transactionalClient.submitParticipator(activer.getTransactionId(), activer.getParticipatorId(), activer.getLockId(), epr, tccContext.getMaxKeepActiveTime());

        if (!participator.getResult() || participator.getData() == null) {
            throw new CannotSubmitTransactionException(participator.getMessage());
        }

        return participator.getData();
    }

    /**
     * 释放锁
     * @param activer
     */
    @Override
    public void releaseLock(Activer activer) {
        participatorActiveLock.unLock(activer);
    }

}
