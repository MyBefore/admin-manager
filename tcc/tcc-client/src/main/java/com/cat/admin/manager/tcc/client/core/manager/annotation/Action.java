package com.cat.admin.manager.tcc.client.core.manager.annotation;

public enum Action {

    /**
     * 忽略事务
     */
    IGNORE,

    /**
     * 创建新事务
     */
    NEW,

    /**
     * 抛出异常
     */
    THROW
}
