package com.cat.admin.manager.tcc.client.core.manager.aop;

import com.cat.admin.manager.tcc.client.core.manager.TccLocal;
import com.cat.admin.manager.tcc.client.core.manager.annotation.Tcc;
import com.cat.admin.manager.tcc.client.core.manager.exception.NotFoundExistsTransactionException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

@Component
@Aspect
@Slf4j
public class TccAop {

    @Autowired
    private com.cat.admin.manager.tcc.client.core.manager.TccLocal tcm;

    @Around(value = "@annotation(com.cat.admin.manager.tcc.client.core.manager.annotation.Tcc)")
    @Order
    public Object handle(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

        // 获取参数
        Method targetMethod = getTargetMethod(proceedingJoinPoint);
        Tcc tcc = targetMethod.getAnnotation(Tcc.class);

        Object proceed;

        boolean ignoreTx = false;

        try {
            if (tcc.join().enable()) {
                try {
                    if (!ignoreTx) {
                        tcm.setJoinTransactionId(getTransactionId());
                        tcm.join(tcc.cancelMethod());
                    }
                } catch (NotFoundExistsTransactionException e) {
                    switch (tcc.join().notExistsAction()) {
                        case IGNORE: {
                            ignoreTx = true;
                        }break;
                        case NEW: {
                            tcm.create(tcc.cancelMethod());
                        }break;
                        case THROW: {
                            throw e;
                        }
                    }
                }
            } else {
                if (!ignoreTx) {
                    tcm.create(tcc.cancelMethod());
                }
            }
            // 执行操作方法
            proceed = proceedingJoinPoint.proceed(proceedingJoinPoint.getArgs());
            tcm.getActiver().checkError();
            if (!ignoreTx) {
                tcm.commit();
            }
        } catch (Exception e) {
            if (!ignoreTx) {
                try {
                    tcm.rollback();
                } catch (NotFoundExistsTransactionException ex) {
                    log.error(ex.getMessage(), ex);
                }
            }
            throw e;
        }

        return proceed;

    }

    private String getTransactionId() {

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        String transactionId = request.getHeader(TccLocal.TCC_TRANSACTIONAL_ID_HEADER_NAME);

        if (transactionId == null)  {
            throw new NotFoundExistsTransactionException("Transaction id is null");
        }

        return transactionId;
    }

    private Method getTargetMethod(ProceedingJoinPoint proceedingJoinPoint) throws NoSuchMethodException {
        MethodSignature signature = (MethodSignature) proceedingJoinPoint.getSignature();
        return proceedingJoinPoint.getTarget().getClass().getMethod(signature.getName(), signature.getParameterTypes());
    }

}
