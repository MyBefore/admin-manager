package com.cat.admin.manager.tcc.client.core.domain;

import lombok.Data;

@Data
public class TccContext {

    /**
     * 回滚任务的线程默认个数
     */
    private int rollbackTaskPoolCount = 5;

    /**
     * 中断存活的定时间隔 默认为50毫秒
     */
    private long interruptAliveActivePeriod = 50L;

    /**
     * 回滚任务活动定时间隔 默认为1000毫秒
     */
    private long rollbackAliveActivePeriod = 1000L;

    /**
     * 保持活动定时间隔 默认为1000毫秒
     */
    private long keepAliveActivePeriod = 1000L;

    /**
     * 保持活动的最长间隔时间
     */
    private long maxKeepActiveTime = 8000L;

    /**
     * 保持活动的请求间隔时间
     */
    private long keepActiveTime = 5000L;

    /**
     * 处理非活动的最长时间
     */
    private long killActiveTime = 10000L;

    /**
     * 中断超时的时间
     */
    private long interruptTimeout = 500L;

    /**
     * 获取未活动的间隔时长
     * @return
     */
    public long getInactiveTime() {
        return maxKeepActiveTime + killActiveTime;
    }
}
