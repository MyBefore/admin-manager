package com.cat.admin.manager.tcc.client.core;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 开启tcc事务
 */
@Import(TccAutoConfiguration.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnableTcc {
}
