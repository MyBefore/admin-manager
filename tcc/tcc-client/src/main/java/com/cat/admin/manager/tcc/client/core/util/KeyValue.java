package com.cat.admin.manager.tcc.client.core.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KeyValue<T, P> {

    private T key;
    private P value;

}
