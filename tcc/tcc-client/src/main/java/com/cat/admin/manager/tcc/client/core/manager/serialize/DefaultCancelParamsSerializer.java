package com.cat.admin.manager.tcc.client.core.manager.serialize;

import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class DefaultCancelParamsSerializer implements CancelParamsSerializer {

    @Override
    public String serialize(Object params) {
        return JSONObject.toJSONString(params);
    }
}
