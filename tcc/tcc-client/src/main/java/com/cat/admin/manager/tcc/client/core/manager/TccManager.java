package com.cat.admin.manager.tcc.client.core.manager;

import com.cat.admin.manager.tcc.client.core.scheduler.lock.Activer;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.EditParticipatorRequest;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.ParticipatorDTO;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.TaskStatus;
import com.cat.admin.manager.tcc.manager.shared.dto.transactional.TransactionalDTO;

/**
 * tcc管理器
 */
public interface TccManager {

    /**
     * 创建事务
     * @param serverId 服务id
     * @param cancelMethod 取消方法  com.cat.Test.cancel
     * @return
     */
    TransactionalDTO create(String serverId, String cancelMethod);

    /**
     * 获取已存在的事务
     * @param transactionId 事务id
     * @return
     */
    TransactionalDTO getTransaction(String transactionId);

    /**
     * 加入事务
     * @param transactionId 要加入的事务id
     * @param serverId 服务id
     * @param cancelMethod 取消方法  com.cat.Test.cancel
     * @return
     */
    ParticipatorDTO joinTo(String transactionId, String serverId, String cancelMethod);

    /**
     * 锁定事务
     * @param transactionId 事务id
     * @param participatorId 参与者id
     * @return
     */
    Activer lockRunning(String transactionId, String participatorId);

    /**
     * 提交事务状态
     * @param activer 活动者
     * @param epr 提交的状态
     * @return
     */
    ParticipatorDTO submit(Activer activer, EditParticipatorRequest epr);

    /**
     * 释放锁
     * @param activer
     */
    void releaseLock(Activer activer);
}
