package com.cat.admin.manager.tcc.client.core.manager.annotation;

import java.lang.annotation.*;

/**
 * 对方法开启事务的注解
 * {@link com.cat.admin.manager.tcc.client.core.manager.aop.TccAop}
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Tcc {
    /**
     * 加入事务的事务id的方法参数名称 未设置则新创建一个事务 将获取此参数的值作为加入的事务的id
     * @return
     */
    Join join() default @Join(enable = false);

    /**
     * 取消方法 包.类.方法 例：com.cat.test.Test.cancel
     * @return
     */
    String cancelMethod();
}
