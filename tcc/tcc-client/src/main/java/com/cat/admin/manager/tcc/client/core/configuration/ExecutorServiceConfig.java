package com.cat.admin.manager.tcc.client.core.configuration;

import com.cat.admin.manager.tcc.client.core.domain.TccContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class ExecutorServiceConfig {

    @Autowired
    private TccContext tccContext;

    @Bean(name = "rollbackTaskPool")
    public ExecutorService rollbackTaskPool() {
        return Executors.newFixedThreadPool(tccContext.getRollbackTaskPoolCount());
    }

}
