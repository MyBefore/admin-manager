package com.cat.admin.manager.tcc.client.core.scheduler.rollback;

import com.cat.admin.manager.tcc.client.core.domain.TccContext;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.TaskStatus;
import com.cat.admin.manager.tcc.manager.shared.dto.transactional.Filter;
import com.cat.admin.manager.tcc.manager.shared.dto.transactional.Filters;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class ExceptionFilters extends Filters implements InitializingBean {

    @Autowired
    private TccContext tccContext;

    private List<Filter> getExceptionFilters() {
        return Arrays.asList(
                readyTimeoutFilter(),
                failedFilter(),
                runningTimeoutFilter(),
                rollbackingTimeoutFilter()
        );
    }

    /**
     * 准备超时的事务
     * @return
     */
    private Filter readyTimeoutFilter() {
        Filter filter = new Filter();
        filter.setStatus(TaskStatus.READY);
        filter.setLtInactiveTime(tccContext.getInactiveTime());
        return filter;
    }

    /**
     * 失败的事务
     * @return
     */
    private Filter failedFilter() {
        Filter filter = new Filter();
        filter.setStatus(TaskStatus.FAILED);
        filter.setLtInactiveTime(0L);
        return filter;
    }

    /**
     * 运行超时的事务
     * @return
     */
    private Filter runningTimeoutFilter() {
        Filter filter = new Filter();
        filter.setStatus(TaskStatus.RUNNING);
        filter.setLtInactiveTime(tccContext.getInactiveTime());
        return filter;
    }

    /**
     * 回滚超时的事务
     * @return
     */
    private Filter rollbackingTimeoutFilter() {
        Filter filter = new Filter();
        filter.setStatus(TaskStatus.ROLLBACKING);
        filter.setLtInactiveTime(tccContext.getInactiveTime());
        return filter;
    }

    @Override
    public void afterPropertiesSet() {
        setFilters(getExceptionFilters());
    }
}
