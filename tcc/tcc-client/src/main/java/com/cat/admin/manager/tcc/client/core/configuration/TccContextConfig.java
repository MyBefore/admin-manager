package com.cat.admin.manager.tcc.client.core.configuration;

import com.cat.admin.manager.tcc.client.core.domain.TccContext;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TccContextConfig {

    @Bean
    @ConfigurationProperties("tcc.context")
    public TccContext tccContext() {
        return new TccContext();
    }

}
