package com.cat.admin.manager.tcc.client.core.scheduler;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cat.admin.manager.tcc.client.core.domain.TccContext;
import com.cat.admin.manager.tcc.client.core.intergration.TransactionalClient;
import com.cat.admin.manager.tcc.client.core.manager.TccManager;
import com.cat.admin.manager.tcc.client.core.scheduler.lock.Activer;
import com.cat.admin.manager.tcc.client.core.scheduler.rollback.ExceptionFilters;
import com.cat.admin.manager.tcc.client.core.scheduler.rollback.Rollbacker;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.ParticipatorDTO;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.TaskStatus;
import com.cat.admin.manager.tcc.manager.shared.dto.transactional.TransactionalDTO;
import com.cat.commons.result.core.GenericResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class RollbackScheduler implements InitializingBean {

    @Autowired
    private TccContext tccContext;

    @Autowired
    private TransactionalClient transactionalClient;

    @Autowired
    private ParticipatorActiveLock participatorActiveLock;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private TccManager tccManager;

    @Autowired
    private ExceptionFilters exceptionFilters;

    @Value("${spring.application.name}")
    private String serverId;

    public void rollbackAliveActive() {
        try {
            List<TransactionalDTO> transactionals = getTransactionals();
            log.debug("GET transactionals: {}", JSONObject.toJSONString(transactionals));
            transactionals.forEach(this::tryRollback);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void tryRollback(TransactionalDTO transactional) {
        List<ParticipatorDTO> ps = transactional
                .getParticipators()
                .stream()
                // 过滤非本服务的参与者
                .filter(p -> serverId.equals(p.getServerId()))
                // 过滤已回滚的参与者
                .filter(p -> !TaskStatus.ROLLBACKED.equals(p.getTaskStatus()))
                // 过滤正在被锁定的参与者
                .filter(p -> ((TaskStatus.READY.equals(p.getTaskStatus())
                        || TaskStatus.RUNNING.equals(p.getTaskStatus())
                        || TaskStatus.ROLLBACKING.equals(p.getTaskStatus()))
                        && ((transactional.getCurrentTimeMillis() - p.getLastActiveTime()) > tccContext.getInactiveTime()))
                        || TaskStatus.FAILED.equals(p.getTaskStatus()) || TaskStatus.SUCCESS.equals(p.getTaskStatus())
                )
                .collect(Collectors.toList());
        ps.forEach(p -> {
            // 尝试对事务获取回滚锁
            log.debug("rollback: transactionId: {}; participatorId: {}", transactional.getId(), p.getId());
            Activer activer = null;
            try {
                activer = participatorActiveLock.tryLock(transactional.getId(), p.getId(), TaskStatus.ROLLBACKING);
                if (activer != null) {
                    Rollbacker rollbacker = parseCancelMethod(p.getCancelMethod());
                    // 开始回滚
                    rollbackTask(p, activer, rollbacker, parseCancelParams(rollbacker.getMethod(), p.getCancelParams()));
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                try {
                    participatorActiveLock.unLock(activer);
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                }
            }

        });
    }

    @Async("rollbackTaskPool")
    public void rollbackTask(ParticipatorDTO participator, Activer activer, Rollbacker rollbacker, Object[] args) throws InvocationTargetException, IllegalAccessException {

        try {

            activer.setActiveTaskThread(Thread.currentThread());

            if (!Thread.interrupted()) {

                if (args == null) {
                    rollbacker.getMethod().invoke(applicationContext.getBean(rollbacker.getClazz()));
                } else {
                    rollbacker.getMethod().invoke(applicationContext.getBean(rollbacker.getClazz()), args);
                }

                // 回滚成功 设置事务为已回滚状态
                participator.setTaskStatus(TaskStatus.ROLLBACKED);
                tccManager.submit(activer, participator);

            }

        } finally {
            try {
                participatorActiveLock.unLock(activer);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }

    }

    private Object[] parseCancelParams(Method method, String cancelParams) {

        Parameter[] parameters = method.getParameters();

        if (parameters.length <= 0) {
            return null;
        }

        if (parameters.length == 1) {
            return new Object[]{JSONObject.parseObject(cancelParams, parameters[0].getType())};
        }

        if (cancelParams == null) {
            cancelParams = "[]";
        }

        Object[] args = new Object[parameters.length];

        Object parse = JSONObject.parse(cancelParams);
        if (parse instanceof JSONArray) {
            JSONArray objects = (JSONArray) parse;
            int size = objects.size();
            for (int i = 0, len = size > parameters.length ? parameters.length : size; i < len; i++) {
                args[i] = objects.getObject(i, parameters[i].getType());
            }
        } else  if (parse instanceof JSONObject) {
            JSONObject object = (JSONObject) parse;
            for (int i = 0; i < parameters.length; i++) {
                args[i] = object.getObject(parameters[i].getName(), parameters[i].getType());
            }
        } else {
            throw new IllegalArgumentException("Params object be cannot parse");
        }

        return args;
    }

    /**
     * 解析取消方法
     * @param cancelMethod
     * @return
     */
    private Rollbacker parseCancelMethod(String cancelMethod) throws ClassNotFoundException, NoSuchMethodException {

        int lastP = cancelMethod.lastIndexOf('.');

        if (StringUtils.isBlank(cancelMethod) || lastP == -1) {
            return null;
        }

        Class<?> clazz = Class.forName(cancelMethod.substring(0, lastP));
        Optional<Method> first = Arrays.stream(clazz.getMethods()).filter(m -> m.getName().equals(cancelMethod.substring(lastP + 1))).findFirst();

        return new Rollbacker(clazz, first.orElseThrow(() -> new NoSuchMethodException()));
    }

    /**
     * 获取含有超时的准备事务
     * @param transactionals
     * @return
     */
    private List<TransactionalDTO> getReadyTimeoutTransactionals(List<TransactionalDTO> transactionals) {
        return transactionals
                .stream()
                .filter(t -> t
                        .getParticipators()
                        .stream()
                        .anyMatch(p -> TaskStatus.READY.equals(p.getTaskStatus()) && (t.getCurrentTimeMillis() - p.getLastActiveTime()) > tccContext.getInactiveTime()))
                .collect(Collectors.toList());
    }

    /**
     * 获取含有回滚的事务
     * @param transactionals
     * @return
     */
    private List<TransactionalDTO> getRollbackedTransactionals(List<TransactionalDTO> transactionals) {
        return transactionals
                .stream()
                .filter(t -> t
                        .getParticipators()
                        .stream()
                        .anyMatch(p -> TaskStatus.ROLLBACKED.equals(p.getTaskStatus())))
                .collect(Collectors.toList());
    }

    /**
     * 获取失败的事务
     * @param transactionals
     * @return
     */
    private List<TransactionalDTO> getFailedTransactionals(List<TransactionalDTO> transactionals) {
        return transactionals
                .stream()
                .filter(t -> t
                        .getParticipators()
                        .stream()
                        .anyMatch(p -> TaskStatus.FAILED.equals(p.getTaskStatus())))
                .collect(Collectors.toList());
    }

    /**
     * 获取运行中但已断开活动的事务
     * @param transactionals
     * @return
     */
    private List<TransactionalDTO> getRunningAndNotActiveTransactionals(List<TransactionalDTO> transactionals) {
        return transactionals
                .stream()
                .filter(t -> t
                        .getParticipators()
                        .stream()
                        .anyMatch(p ->
                                TaskStatus.RUNNING.equals(p.getTaskStatus()) && ((t.getCurrentTimeMillis() - p.getLastActiveTime()) <= tccContext.getInactiveTime())))
                .collect(Collectors.toList());
    }

    /**
     * 获取回滚中但已断开活动的事务
     * @param transactionals
     * @return
     */
    private List<TransactionalDTO> getRollbackingAndNotActiveTransactionals(List<TransactionalDTO> transactionals) {
        return transactionals
                .stream()
                .filter(t -> t
                        .getParticipators()
                        .stream()
                        .anyMatch(p ->
                                TaskStatus.ROLLBACKING.equals(p.getTaskStatus()) && ((t.getCurrentTimeMillis() - p.getLastActiveTime()) <= tccContext.getInactiveTime())))
                .collect(Collectors.toList());
    }

    /**
     * 获取事务集合
     * @return
     */
    private List<TransactionalDTO> getTransactionals() {
        GenericResult<List<TransactionalDTO>> result = transactionalClient.getTransactionalsByServerIdAndFilters(serverId, exceptionFilters);
        if (result.getResult()) {
            return result.getData();
        } else {
            log.error(result.getMessage());
        }
        return Collections.EMPTY_LIST;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        new Timer("rollbackAliveActive", true).schedule(new TimerTask() {
            @Override
            public void run() {
                rollbackAliveActive();
            }
        }, 0, tccContext.getRollbackAliveActivePeriod());
    }
}
