package com.cat.admin.manager.tcc.client.core.scheduler;

import com.cat.admin.manager.tcc.client.core.scheduler.lock.Activer;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.TaskStatus;

/**
 * 参与者活动锁
 */
public interface ParticipatorActiveLock {

    /**
     * 尝试锁定
     * @param transactionId 事务id
     * @param participatorId 参与者id
     * @param status 状态
     * @return
     */
    Activer tryLock(String transactionId, String participatorId, TaskStatus status);

    /**
     * 释放锁
     * @param activer 活动者
     */
    void unLock(Activer activer);

    /**
     * 定时中断存活的线程
     */
    void interruptAliveActive();

    /**
     * 定时存活的活动
     */
    void keepAliveActive();

    /**
     * 异步执行存活的活动
     * @param a 活动者
     */
    void handlerActive(Activer a);

}
