package com.cat.admin.manager.tcc.client.core.manager;

import com.cat.admin.manager.tcc.client.core.manager.exception.ExistsTransactionException;
import com.cat.admin.manager.tcc.client.core.manager.exception.NotFoundExistsTransactionException;
import com.cat.admin.manager.tcc.client.core.manager.serialize.CancelParamsSerializer;
import com.cat.admin.manager.tcc.client.core.scheduler.lock.Activer;
import com.cat.admin.manager.tcc.client.core.util.KeyValue;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.ParticipatorDTO;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.TaskStatus;
import com.cat.admin.manager.tcc.manager.shared.dto.transactional.TransactionalDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TccLocal {


    public static final String TCC_TRANSACTIONAL_ID_HEADER_NAME = "TCC_TRANSACTIONAL_ID";

    /**
     * 存放当前线程的事务
     */
    private ThreadLocal<KeyValue<TransactionalDTO, KeyValue<ParticipatorDTO, Activer>>> local = new ThreadLocal<>();

    /**
     * 当前事务id
     */
    private ThreadLocal<KeyValue<String, Object>> txp = new ThreadLocal<>();

    @Autowired
    private TccManager tccManager;

    @Value("${spring.application.name}")
    private String serverId;

    @Autowired
    private CancelParamsSerializer cancelParamsSerializer;

    /**
     * 创建新事务
     * @param cancelMethod
     */
    public void create(String cancelMethod) {
        begin(cancelMethod, false);
    }

    /**
     * 加入事务
     * @param cancelMethod
     */
    public void join(String cancelMethod) {
        begin(cancelMethod, true);
    }

    /**
     * 开始事务
     * @param cancelMethod 事务的取消方法
     * @param join 是否需要检测加入事务
     */
    public void begin(String cancelMethod, boolean join) {

        if (local.get() != null) {
            throw new ExistsTransactionException("Transaction already exists");
        }

        String transactionId = Optional.ofNullable(this.txp.get()).map(KeyValue::getKey).orElse(null);

        TransactionalDTO transactional;
        String participatorId;

        // 判断是否需要加入到某一个事务
        if (join) {
            if (StringUtils.isBlank(transactionId)) {
                throw new NotFoundExistsTransactionException("Transaction does not exist");
            }
            // 加入事务
            ParticipatorDTO participator = tccManager.joinTo(transactionId, serverId, cancelMethod);
            participatorId = participator.getId();
            // 获取事务
            transactional = tccManager.getTransaction(transactionId);
        } else {
            transactional = tccManager.create(serverId, cancelMethod);
            participatorId = transactional.getCreatorId();
        }

        Activer activer = null;
        try {
            activer = tccManager.lockRunning(transactional.getId(), participatorId);
            activer.setActiveTaskThread(Thread.currentThread());
            local.set(
                    new KeyValue<>(
                            transactional,
                            new KeyValue<>(
                                    transactional
                                            .getParticipators()
                                            .stream()
                                            .filter(p -> p.getId().equals(participatorId))
                                            .findFirst()
                                            .get(),
                                    activer
                            )
                    )
            );
        } catch (Exception e) {
            tccManager.releaseLock(activer);
            local.remove();
            this.txp.remove();
            throw e;
        }
    }

    /**
     * 提交事务
     * @param cancelParams
     */
    public void commit(Object cancelParams) {

        if (local.get() == null) {
            throw new NotFoundExistsTransactionException("Transaction does not exist");
        }

        try {
            KeyValue<TransactionalDTO, KeyValue<ParticipatorDTO, Activer>> tkv = local.get();
            ParticipatorDTO participator = tkv.getValue().getKey();
            participator.setCancelParams(cancelParamsSerializer.serialize(cancelParams));
            participator.setTaskStatus(TaskStatus.SUCCESS);
            tkv.getValue().setKey(tccManager.submit(tkv.getValue().getValue(), participator));
        } finally {
            tccManager.releaseLock(local.get().getValue().getValue());
            local.remove();
            txp.remove();
        }
    }

    /**
     * 提交事务 从设置的取消参数中取值
     */
    public void commit() {
        commit(Optional.ofNullable(txp.get()).map(KeyValue::getValue).orElse(null));
    }

    /**
     * 回滚事务
     * @param cancelParams
     */
    public void rollback(Object cancelParams) {

        if (local.get() == null) {
            throw new NotFoundExistsTransactionException("Transaction does not exist");
        }

        try {
            KeyValue<TransactionalDTO, KeyValue<ParticipatorDTO, Activer>> tkv = local.get();
            ParticipatorDTO participator = tkv.getValue().getKey();
            participator.setCancelParams(cancelParamsSerializer.serialize(cancelParams));
            participator.setTaskStatus(TaskStatus.FAILED);
            tkv.getValue().setKey(tccManager.submit(tkv.getValue().getValue(), participator));
        } finally {
            tccManager.releaseLock(local.get().getValue().getValue());
            local.remove();
            txp.remove();
        }
    }

    /**
     * 回滚事务 从设置的取消参数中取值
     */
    public void rollback() {
        rollback(Optional.ofNullable(txp.get()).map(KeyValue::getValue).orElse(null));
    }

    /**
     * 设置加入到事务id
     * @param transactionId
     */
    public void setJoinTransactionId(String transactionId) {

        if (local.get() != null) {
            throw new ExistsTransactionException("Transaction already exists");
        }

        if (this.txp.get() == null) {
            this.txp.set(new KeyValue<>());
        }

        this.txp.get().setKey(transactionId);
    }

    /**
     * 获取当前事务id
     * @return
     */
    public String getTransactionId() {

        if (local.get() == null) {
            throw new NotFoundExistsTransactionException("Transaction does not exist");
        }

        return local.get().getKey().getId();
    }

    /**
     * 获取当前参与者id
     * @return
     */
    public String geParticipatorId() {

        if (local.get() == null) {
            throw new NotFoundExistsTransactionException("Transaction does not exist");
        }

        return local.get().getValue().getKey().getId();
    }

    /**
     * 设置取消参数
     * @param cancelParams
     */
    public void setCancelParams(Object cancelParams) {

        if (this.txp.get() == null) {
            this.txp.set(new KeyValue<>());
        }

        this.txp.get().setValue(cancelParams);
    }

    /**
     * 获取当前活动者
     * @return
     */
    public Activer getActiver() {
        return Optional.ofNullable(local.get()).map(KeyValue::getValue).map(KeyValue::getValue).orElse(null);
    }


}
