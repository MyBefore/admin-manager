package com.cat.admin.manager.tcc.client.core.manager.exception;

import org.springframework.transaction.TransactionException;

public class ExistsTransactionException extends TransactionException {
    public ExistsTransactionException(String msg) {
        super(msg);
    }

    public ExistsTransactionException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
