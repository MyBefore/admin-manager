package com.cat.admin.manager.tcc.client.core.manager.annotation;

/**
 * 加入事务的对象
 */
public @interface Join {

    /**
     * 是否启用
     * @return
     */
    boolean enable() default true;

    /**
     * 事务不存在的操作
     * @return
     */
    Action notExistsAction() default Action.IGNORE;
}
