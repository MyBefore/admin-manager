package com.cat.admin.manager.tcc.client.core.scheduler.lock;

import lombok.Data;

@Data
public class Activer {

    public Activer(String transactionId, String participatorId, Long lockId, long lastActiveTime) {
        this.transactionId = transactionId;
        this.participatorId = participatorId;
        this.lockId = lockId;
        this.lastActiveTime = lastActiveTime;
    }

    /**
     * 事务id
     */
    private String transactionId;

    /**
     * 参与者id
     */
    private String participatorId;

    /**
     * 锁的id
     */
    private Long lockId;

    /**
     * 上次活动的时间
     */
    private long lastActiveTime;

    /**
     * 活动任务的执行线程
     */
    private Thread activeTaskThread;

    /**
     * 出错时存放的异常
     */
    private Exception error;

    /**
     * 出错时的时间
     */
    private long errorTime;

    /**
     * 校验错误状态
     * @throws Exception
     */
    public void checkError() throws Exception {
        if (error != null) {
            throw error;
        }
    }
}
