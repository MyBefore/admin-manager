package com.cat.admin.manager.tcc.client.core.configuration;

import com.cat.admin.manager.tcc.client.core.manager.TccLocal;
import feign.Feign;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TccFeignConfig implements InitializingBean {

    @Autowired
    private Feign.Builder builder;

    @Autowired
    private TccLocal tccLocal;

    @Override
    public void afterPropertiesSet() {
        builder.requestInterceptor(new TransactionalRequestInterceptor(tccLocal));
    }

    public static class TransactionalRequestInterceptor implements RequestInterceptor {

        private TccLocal tccLocal;

        public TransactionalRequestInterceptor(TccLocal tccLocal) {
            this.tccLocal = tccLocal;
        }

        @Override
        public void apply(RequestTemplate template) {

            String tccTransaction = null;
            try {
                tccTransaction = tccLocal.getTransactionId();
            } catch (Exception e) {}

            if (tccTransaction != null) {
                template.header(TccLocal.TCC_TRANSACTIONAL_ID_HEADER_NAME, tccTransaction);
            }
        }
    }

}
