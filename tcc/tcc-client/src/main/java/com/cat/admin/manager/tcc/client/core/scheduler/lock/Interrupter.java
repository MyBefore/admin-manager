package com.cat.admin.manager.tcc.client.core.scheduler.lock;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Interrupter {

    private Activer activer;

    private long interruptTime;
}
