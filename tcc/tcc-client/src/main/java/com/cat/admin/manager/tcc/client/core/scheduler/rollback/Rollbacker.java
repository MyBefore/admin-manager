package com.cat.admin.manager.tcc.client.core.scheduler.rollback;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.lang.reflect.Method;

@Data
@AllArgsConstructor
public class Rollbacker {

    private Class<?> clazz;

    private Method method;

}
