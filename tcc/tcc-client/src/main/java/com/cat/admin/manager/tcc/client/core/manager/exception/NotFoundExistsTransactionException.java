package com.cat.admin.manager.tcc.client.core.manager.exception;

import org.springframework.transaction.TransactionException;

public class NotFoundExistsTransactionException extends TransactionException {
    public NotFoundExistsTransactionException(String msg) {
        super(msg);
    }

    public NotFoundExistsTransactionException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
