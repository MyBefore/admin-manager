package com.cat.admin.manager.tcc.client.core.intergration;

import com.cat.admin.manager.tcc.manager.shared.api.TransactionalApi;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

@FeignClient("tcc-manager")
@Component
public interface TransactionalClient extends TransactionalApi {
}
