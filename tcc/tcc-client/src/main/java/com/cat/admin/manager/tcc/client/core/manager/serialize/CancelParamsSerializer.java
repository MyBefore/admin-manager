package com.cat.admin.manager.tcc.client.core.manager.serialize;

/**
 * 取消参数的序列化
 */
public interface CancelParamsSerializer {

    /**
     * 序列化操作
     * @param params
     * @return
     */
    String serialize(Object params);
}
