package com.cat.admin.manager.tcc.client.core.manager.exception;

import org.springframework.transaction.TransactionException;

public class CannotJoinTransactionException extends TransactionException {
    public CannotJoinTransactionException(String msg) {
        super(msg);
    }

    public CannotJoinTransactionException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
