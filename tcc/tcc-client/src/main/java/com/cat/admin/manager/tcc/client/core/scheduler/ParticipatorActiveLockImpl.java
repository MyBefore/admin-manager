package com.cat.admin.manager.tcc.client.core.scheduler;

import com.cat.admin.manager.tcc.client.core.domain.TccContext;
import com.cat.admin.manager.tcc.client.core.intergration.TransactionalClient;
import com.cat.admin.manager.tcc.client.core.manager.exception.CannotLockTransactionException;
import com.cat.admin.manager.tcc.client.core.scheduler.lock.Activer;
import com.cat.admin.manager.tcc.client.core.scheduler.lock.Interrupter;
import com.cat.admin.manager.tcc.manager.shared.dto.participator.TaskStatus;
import com.cat.commons.result.core.GenericResult;
import com.cat.commons.result.core.Result;
import com.cat.commons.time.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

@Component
@Slf4j
public class ParticipatorActiveLockImpl implements ParticipatorActiveLock, InitializingBean {

    /**
     * 正在活动的队列
     */
    private List<Activer> activers = new CopyOnWriteArrayList<>();


    /**
     * 中断队列
     */
    private List<Interrupter> interrupts = new CopyOnWriteArrayList<>();
    @Autowired
    private TccContext tccContext;

    @Autowired
    private TransactionalClient transactionalClient;

    @Override
    public void interruptAliveActive() {
        interrupts.forEach(i -> {
            Thread rollbackThread = i.getActiver().getActiveTaskThread();
            if (rollbackThread != null) {
                if (rollbackThread.isAlive()) {
                    rollbackThread.interrupt();
                }
                interrupts.remove(rollbackThread);
            } else {
                if (DateTimeUtils.getUTCCurrentTimeMillis() - i.getInterruptTime() >= tccContext.getInterruptTimeout()) {
                    interrupts.remove(rollbackThread);
                }
            }
        });
    }

    @Override
    public void keepAliveActive() {
        log.debug("Activers count: {}", this.activers.size());
        // 将 activers 上次存活时间到目前 间隔 tccContext.keepActiveTime 时间的活动者执行活动
        this.activers.forEach(a -> {
            if ((DateTimeUtils.getUTCCurrentTimeMillis() - a.getLastActiveTime()) >= (tccContext.getKeepActiveTime() - 1)) {
                handlerActive(a);
            }
        });
    }

    public void handlerActive(Activer a) {
        long errorTime = DateTimeUtils.getUTCCurrentTimeMillis();
        try {
            Result result = transactionalClient.updateParticipatorActive(a.getTransactionId(), a.getParticipatorId(), a.getLockId(), tccContext.getMaxKeepActiveTime());
            if (!result.getResult()) {
                a.setErrorTime(errorTime);
                a.setError(new IllegalAccessException(result.getMessage()));
                // 中断活动任务线程
                interrupts.add(new Interrupter(a, DateTimeUtils.getUTCCurrentTimeMillis()));
            }
        } catch (Exception e) {
            a.setErrorTime(errorTime);
            a.setError(e);
            // 中断活动任务线程
            interrupts.add(new Interrupter(a, DateTimeUtils.getUTCCurrentTimeMillis()));
        }
    }

    /**
     * 尝试锁定
     * @param transactionId 事务id
     * @param participatorId 参与者id
     * @param status 状态
     * @return
     */
    @Override
    public Activer tryLock(String transactionId, String participatorId, TaskStatus status) {

        long lastActiveTime = DateTimeUtils.getUTCCurrentTimeMillis();

        GenericResult<Long> result = transactionalClient.tryLockParticipatorActive(transactionId, participatorId, status, tccContext.getInactiveTime(), tccContext.getMaxKeepActiveTime());

        if (!result.getResult()) {
            throw new CannotLockTransactionException(result.getMessage());
        }

        Activer activer = new Activer(transactionId, participatorId, result.getData(), lastActiveTime);

        if (activers.add(activer)) {
            return activer;
        } else {
            return null;
        }
    }

    /**
     * 释放锁
     * @param activer 活动者
     */
    @Override
    public void unLock(Activer activer) {
        if (activer != null) {
            activers.remove(activer);
        }
    }

    @Override
    public void afterPropertiesSet() {

        // 定时调度中断轮询
        new Timer("interruptAliveActive", true).schedule(new TimerTask() {
            @Override
            public void run() {
                interruptAliveActive();
            }
        }, 0, tccContext.getInterruptAliveActivePeriod());

        // 定时存活轮询
        new Timer("keepAliveActive", true).schedule(new TimerTask() {
            @Override
            public void run() {
                keepAliveActive();
            }
        }, 0, tccContext.getKeepAliveActivePeriod());

    }
}
