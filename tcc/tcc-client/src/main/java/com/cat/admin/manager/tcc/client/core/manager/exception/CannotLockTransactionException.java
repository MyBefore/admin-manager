package com.cat.admin.manager.tcc.client.core.manager.exception;

import org.springframework.transaction.TransactionException;

public class CannotLockTransactionException extends TransactionException {
    public CannotLockTransactionException(String msg) {
        super(msg);
    }

    public CannotLockTransactionException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
