package com.cat.admin.manager.tcc.client.core.manager.exception;

import org.springframework.transaction.TransactionException;

public class CannotSubmitTransactionException extends TransactionException {
    public CannotSubmitTransactionException(String msg) {
        super(msg);
    }

    public CannotSubmitTransactionException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
